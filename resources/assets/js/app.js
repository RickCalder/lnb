
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
// require('./slugify');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));

const app = new Vue({
    el: '#app'
});

//Debounce

var debounce = function(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

  //enable Bootstrap popovers
  $('[data-toggle="popover"]').popover().on("click",function(e){
    e.preventDefault();
  }); 

//Function to keep containers equal height.
var equalheight = function(container){
  var currentTallest = 0,
      currentRowStart = 0,
      rowDivs = new Array(),
      $el,
      topPosition = 0;
  $(container).each(function() {
    $el = $(this)
    $($el).height("auto");
    topPosition = $el.position().top
  if (currentRowStart != topPosition) {
    for (var currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
      rowDivs[currentDiv].height(currentTallest)
    }
    rowDivs.length = 0; // empty the array
    currentRowStart = topPosition;
    currentTallest = $el.height()
    rowDivs.push($el)
  } else {
    rowDivs.push($el)
    currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest)
  }
  for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
    rowDivs[currentDiv].height(currentTallest)
  }
 });
}

$(window).on('load', function(){
  if($(window).width() > 1023) {
    equalheight(".article-teaser-inner");
  }
  if($(window).width() > 767) {
    equalheight(".front-col .section")
  }
});

$(document).ready(function() {
  var loc = window.location.href;
  if( loc.indexOf('articles') > -1 ) {
    var el = $('.share-side-container').offset().top;

    $(window).on("scroll", function() {
      
      var currentPosition = $(document).scrollTop();
      if( currentPosition >= el) {
        $('.share-side-container').addClass('sidebar-stuck');
      } else if( currentPosition <= el){
        $('.share-side-container').removeClass('sidebar-stuck')
      }

    });
  }


  $(".form-edit-add").submit(function(e) {
    e.preventDefault();
    var str = $("#slug").val();
    console.log(str)
    str.replace(/[$*_+~.()'"!\-:@]/g,''); 
    console.log(str)
  })
})

//Header opacity

$(document).on("scroll", debounce( function() {
  windowTop = $(window).scrollTop();
  if( windowTop > 200 ) {
    $(".navbar.transparent-nav.navbar-default").addClass("opaque");
  } else {
    $(".navbar.transparent-nav.navbar-default").removeClass("opaque");
  }
}, 50));

// Contact Form
$('.contact-submit').on('click', function(e) {
  e.preventDefault();
  $('.alert-success').slideDown();
  $('.alert-danger').slideDown();
})

$('.next').on('click', function(e) {
  if(startElement===(totalGames-(visibleGames-1) )) return;
  e.preventDefault();
  var scroll = $(elements[startElement]).outerWidth() + 10
  $('.games-container').animate({
    left: '-=' + scroll
  }, 300)
  startElement += 1;
})

$('.prev').on('click', function(e) {
  e.preventDefault();
  if(startElement===0) return;
  startElement -= 1;
  var scroll = $(elements[startElement]).outerWidth() + 10

  $('.games-container').animate({
    left: '+=' + scroll
  }, 300)

})

if($('.games-container').length) {
  var position = $(".games-container").position()
  var totalGames = $('.game-item').length
  var containerWidth = $('.current-games').outerWidth();
  var visibleGames = (containerWidth / 200).toFixed(0)
  var elements = $.makeArray( $('.game-item') )
  var pos = $('.game-date-today').position()
  var startElement
  $.each(elements, function(i) {
    if( $.inArray('game-date-today', this.classList ) > -1 ) {
      startElement = i
    }
  })
  var startpos = pos.left;
  position.left = startpos * -1
  $('.games-container').animate({
    left: '-=' + startpos
  }, 0)

}
