$(document).ready(function () {
  var standings = $('#standingsTable').DataTable({
    'paging': false,
    'responsive': true,
    'info': false
  });
  $("#league-div").show();

  $("#standings-select").on("change", function() {
    var whichType = $(this).find(":selected").data("stype")
    var whichOne = $(this).val()
    console.log(whichOne)
    if( whichType == "division") {
      $(".standings-type").html(whichOne + " " + whichType)
      var div = "#" + whichOne.toLowerCase() + "-div";
      $(".vs-div").hide()
      $(div).slideDown();
      standings
        .columns([11, 10])
        .search("")
        .column(10)
        .search(whichOne)
        .draw();
    }
    if( whichType == "conference") {
      $(".standings-type").html(whichOne + " " + whichType)
      var div = "#" + whichOne.toLowerCase() + "-div";
      $(".vs-div").hide()
      $(div).slideDown();
      standings
        .columns([10, 11])
        .search("")
        .column(11)
        .search(whichOne)
        .order([0, "desc"])
        .draw();
    }
    if( whichType == "league") {
      $(".standings-type").html(whichOne)
      var div = "#" + whichOne.toLowerCase() + "-div";
      $(".vs-div").hide()
      $(div).slideDown();
      standings
        .columns([10, 11])
        .search("")
        .draw();
    }
    console.log(whichType)
  })
});