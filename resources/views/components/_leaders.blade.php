@php
  foreach( $standings as $index => $team ){
    if($team->abbr === 'TOR') {
      if( substr($index+1, -1) == 1 ) {
        $atlantic = $index+1 . 'st';
      } else if( substr($index+1, -1) == 2 ) {
        $atlantic = $index+1 . 'nd';
      } else if( substr($index+1, -1) == 3 ) {
        $atlantic = $index+1 . 'rd';
      } else {
        $atlantic = $index+1 . 'th';  
      }
    }
  }
  foreach( $fullStandings as $index => $team ){
    if($team->abbr === 'TOR') {
      if( substr($index+1, -1) == 1 ) {
        $league = $index+1 . 'st';
      } else if( substr($index+1, -1) == 2 ) {
        $league = $index+1 . 'nd';
      } else if( substr($index+1, -1) == 3 ) {
        $league = $index+1 . 'rd';
      } else {
        $league = $index+1 . 'th';  
      }
    }
  }
  foreach( $fullStandings as $index => $team ){
    if($team->abbr === 'TOR') {
      $leafs = $team;
    }
  }
  foreach( $eastStandings as $index => $team ){
    if($team->abbr === 'TOR') {
      if( substr($index+1, -1) == 1 ) {
        $eastern = $index+1 . 'st';
      } else if( substr($index+1, -1) == 2 ) {
        $eastern = $index+1 . 'nd';
      } else if( substr($index+1, -1) == 3 ) {
        $eastern = $index+1 . 'rd';
      } else {
        $eastern = $index+1 . 'th';  
      }
    }
  }
@endphp

 <div class="page-container"> 
  <h2>Season at a Glance</h2>
  <div class="row">
    <div class="col-md-12">
      <h3 class="reverse center">Standings</h3>
    </div>
    <div class="col-md-4">
      <h4 class="reverse">Atlantic: {{ $atlantic }}</h4>
    </div>
    <div class="col-md-4">
      <h4 class="reverse">Eastern: {{ $eastern }}</h4>
    </div>
    <div class="col-md-4">
      <h4 class="reverse">League: {{ $league }}</h4>
    </div>
    <div class="col-md-12 center">
      <p style="margin-top: 1rem"><a href="{{ route('standings') }}" class="btn btn-primary">Full Standings</a></p>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-md-12">
      <h3 class="reverse center">Record</h3>
    </div>
    <div class="col-md-3">
      <h4 class="reverse">Wins: {{ $leafs->wins }}</h4>
    </div>
    <div class="col-md-3">
      <h4 class="reverse">Losses: {{ $leafs->losses }}</h4>
    </div>
    <div class="col-md-3">
      <h4 class="reverse">OT: {{ $leafs->ot }}</h4>
    </div>
    <div class="col-md-3">
      <h4 class="reverse">Points: {{ $leafs->points }}</h4>
    </div>
    <div class="col-md-12 center">
      <p style="margin-top: 1rem"><a href="{{ route('schedule') }}" class="btn btn-primary">Full Schedule</a></p>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col-md-12">
      <h3 class="reverse center">Team Leaders</h3>
    </div>
    <div class="col-md-6">
      <h4 class="reverse">Goals - {{ $leaders['goals'][0]->goals }}</h4>
      <?php $player = $leaders['goals'][0] ;?>
      @include('components/_leader-single')
    </div>
    <div class="col-md-6">
      <h4 class="reverse">Assists - {{ $leaders['assists'][0]->assists }}</h4>
      <?php $player = $leaders['assists'][0] ;?>
      @include('components/_leader-single')
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <h4 class="reverse">Points - {{ $leaders['points'][0]->points }}</h4>
      <?php $player = $leaders['points'][0] ;?>
      @include('components/_leader-single')
    </div>
    <div class="col-md-6">
      <h4 class="reverse">Penalty Minutes - {{ $leaders['pim'][0]->pim }}</h4>
      <?php $player = $leaders['pim'][0] ;?>
      @include('components/_leader-single')
    </div>
    <div class="col-md-12 center">
      <p style="margin-top: 1rem"><a href="{{ route('stats') }}" class="btn btn-primary">All Statistics</a></p>
    </div>
  </div>
</div>