@php
  switch($type){
    case 'Goals':
      $stat = $player->goals;
      break;
    case 'Assists':
      $stat = $player->assists;
      break;
    case 'Points':
      $stat = $player->points;
      break;
    case 'PIM':
      $stat = $player->pim;
      break;
  }
@endphp

<a href="{{ route('player', $player->nhl_slug) }}" title="Link to {{ $player->first_name }} {{ $player->last_name }}">
  <div class="leader-row">
    <div class="leader-avatar">
      <img 
        src="https://nhl.bamcontent.com/images/headshots/current/60x60/{{ $player->league_id }}.jpg"
        class="small-headshot sidebar-headshot "
        alt="Image of {{ $player->first_name }} {{ $player->last_name}}" 
        onerror="this.src = 'https://nhl.bamcontent.com/images/headshots/current/60x60/skater.jpg'">
    </div>
    <div class="leader-player">
      <p class="type">{{ $type }}</p>
      <p class="player-name">{{ $player->first_name }} {{ $player->last_name }}</p>
      <p>#{{ $player->jersey_number }} {{ $player->position }}</p>
    </div>
    <div class="player-stat">
      <p class="stat">{{ $stat }}</p>
      <p>{{ $type }}</p>
    </div>
  </div>
</a>
<div class="clearfix"></div>