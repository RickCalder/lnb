 
<div class="article-teaser-container">
  <h2>Recent Articles</h2>
  <div class="row">
    @foreach( $posts as $post )
     <a href="/articles/{{ $post->slug }}" class="article-teaser-link">
      <div class="article-teaser col-md-4">
        <div class="article-teaser-inner">
          <div class="header" style="background-image:url(storage/{{ $post->image }})">
            <h3>{{ $post->title }}</h3>
          </div>
          <p class="author">{{ $post->user->name }}</p>
          <p class="excerpt">
            {{ $post->excerpt }}
          </p>
          <p class="center read-more"><span class="btn btn-primary">Read More</span></p>
        </div>
      </div>
    </a>
    @endforeach
  </div>
</div>