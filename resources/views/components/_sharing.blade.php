<div class="sharing">
  <p class="facebook-share share-link">
    <a href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(URL::to('/articles/' )) }}%2F{!! urlencode($page->slug) !!}&t={{$page->title}}"
         onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=400,width=800');return false;"
         target="_blank" title="Share on Facebook">
      <i class="fa fa-facebook"></i><span class="hidden-xs"> Share on Facebook</span>
    </a>
  </p>
  <p class="twitter-share share-link">
    <a href="https://twitter.com/share?url={{ urlencode(URL::to('/articles/' )) }}%2F{{$page->slug}}&text={{ urlencode($page->title) }}"
         onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
         target="_blank" title="Share on Twitter">
      <i class="fa fa-twitter"></i><span class="hidden-xs"> Share on Twitter</span>
    </a>
  </p>
</div>