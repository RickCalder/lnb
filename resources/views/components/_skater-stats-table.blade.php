<table id="skatersTable" class="player-page-table">
  <thead>
    <tr>
      <th>GP</th>
      <th>G</th>
      <th>A</th>
      <th>P</th>
      <th>+/-</th>
      <th>PIM</th>
      <th>GWG</th>
      <th>OTG</th>
      <th>S</th>
      <th>S%</th>
      <th>FO%</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>{{ $player->games }}</td>
      <td>{{ $player->goals }}</td>
      <td>{{ $player->assists }}</td>
      <td>{{ $player->points }}</td>
      <td>{{ $player->plus_minus }}</td>
      <td>{{ $player->pim }}</td>
      <td>{{ $player->gwg }}</td>
      <td>{{ $player->otg }}</td>
      <td>{{ $player->shots }}</td>
      <td>{{ $player->shot_pct }}</td>
      <td>{{ $player->face_off_pct }}</td>
    </tr>
  </tbody>
</table>

<div class="col-sm-12" id="player-stats-small">
  <p>Games Played: {{ $player->games }}</p>
  <p>Goals: {{ $player->goals }}</p>
  <p>Assists: {{ $player->assists }}</p>
  <p>Points: {{ $player->points }}</p>
  <p>Plus / Minus: {{ $player->plus_minus }}</p>
  <p>Penalty Minutes: {{ $player->pim }}</p>
  <p>Game Winning Goals: {{ $player->gwg }}</p>
  <p>Overtime Goals: {{ $player->otg }}</p>
  <p>Shots: {{ $player->shots }}</p>
  <p>Shooting Percentage: {{ $player->shot_pct }}%</p>
  <p>Face Off Percentage: {{ $player->face_off_pct }}%</p>
</div>