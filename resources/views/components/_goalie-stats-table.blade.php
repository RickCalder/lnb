<table id="goaliesTable" class="player-page-table">
<thead>
  <tr>
    <th>GP</th>
    <th>GS</th>
    <th>W</th>
    <th>L</th>
    <th>T</th>
    <th>OT</th>
    <th>SA</th>
    <th>GA</th>
    <th>GAA</th>
    <th>S</th>
    <th>Sv%</th>
    <th>SO</th>
    <th>MIN</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>{{ $player->games }}</td>
    <td>{{ $player->games_started }}</td>
    <td>{{ $player->wins }}</td>
    <td>{{ $player->losses }}</td>
    <td>{{ $player->ties }}</td>
    <td>{{ $player->ot }}</td>
    <td>{{ $player->shots_against }}</td>
    <td>{{ $player->goals_against }}</td>
    <td>{{ $player->goals_against_average }}</td>
    <td>{{ $player->saves }}</td>
    <td>{{ $player->save_pct }}</td>
    <td>{{ $player->shutouts }}</td>
    <td>{{ $player->toi }}</td>
  </tr>
</tbody>
</table>


<div class="col-sm-12" id="player-stats-small">
  <p>Games Played: {{ $player->games }}</p>
  <p>Games Started: {{ $player->games_started }}</p>
  <p>Wins: {{ $player->wins }}</p>
  <p>Losses: {{ $player->losses }}</p>
  <p>Ties: {{ $player->ties }}</p>
  <p>OT Losses: {{ $player->ot }}</p>
  <p>Shots Against: {{ $player->shots_against }}</p>
  <p>Goals Against: {{ $player->goals_against }}</p>
  <p>Goals Against Average: {{ $player->goals_against_average }}</p>
  <p>Saves: {{ $player->saves }}</p>
  <p>Save Percentage: {{ $player->save_pct }}</p>
  <p>Shutouts: {{ $player->shutouts }}</p>
  <p>Minutes: {{ $player->toi }}</p>
</div>