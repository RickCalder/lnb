
@php
  $first_column = ceil(count($players)/3);
  $second_column = $first_column - 1;
  $third_column = $first_column + $second_column;
@endphp

@for($x=0; $x<=2; $x++)

@php
  switch($x) {
    case 0:
      $start = 0;
      $to = $first_column - 1;
      break;
    case 1:
      $start = $second_column +1 ;
      $to = $third_column - 1;
      break;
    case 2:
      $start = $third_column;
      $to = count($players) -1;
      break;
  }

@endphp

  <div class="col-md-4">
    @for( $i=$start; $i <= $to; $i++ )
      <div class="social-player clearfix">
        <div class="avatar">
          <img 
            src="https://nhl.bamcontent.com/images/headshots/current/60x60/{{ $players[$i]->league_id }}.jpg"
            class="small-headshot sidebar-headshot "
            alt="Image of {{ $players[$i]->first_name }} {{ $players[$i]->last_name}}" 
            onerror="this.src = 'https://nhl.bamcontent.com/images/headshots/current/60x60/skater.jpg'">
        </div>
        <div class="player">
          <p class="player-name">{{ $players[$i]->first_name }} {{ $players[$i]->last_name }}</p>
          @if(  $players[$i]->twitter != null )
          <p>
            <a href="https://twitter.com/{{ $players[$i]->twitter }}" target="_blank" class="twitter-sm">
              <i class="fa fa-twitter"></i> {{ $players[$i]->twitter }}
            </a>
          </p>
          @endif
          @if(  $players[$i]->facebook !==null )
          <p>
            <a href="https://facebook.com/{{ $players[$i]->facebook }}" target="_blank" class="facebook-sm">
              <i class="fa fa-facebook"></i> {{ $players[$i]->facebook }}
            </a>
          </p>
          @endif
        </div>
      </div>
    @endfor
  </div>
@endfor