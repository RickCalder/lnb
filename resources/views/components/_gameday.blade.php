@php 
$opponent = isset($todaysOpponent[0]) ? $todaysOpponent[0] : $nextOpponent[0];
$opponentLogo = strtolower(str_replace("é","e",str_replace(".","",str_replace(" ", "_", $opponent->name)))) . '.svg';
// echo env('TEAM_ID');die;
  foreach ($standings as $item) {
    if($item->team_id == env('TEAM_ID')) {
      $team = $item;
      break;
    }
  }

  $where = $games['next']->awayId == env('TEAM_ID') ? 'Away Game' : 'Home Game';
@endphp

<div class="col-lg-12 gameday-div">
  @if(isset($todaysOpponent[0]))
    <h2>Game Day &mdash; {{ date('M j, g:ia', strtotime($games['next']->date)) }} &mdash; {{ $where }}</h2>
  @else
    <h2>Off Day &mdash; Next Game: {{ date('M j, g:ia', strtotime($games['next']->date)) }} &mdash; {{ $where }}</h2>
  @endif
  <div class="row">
    <div class="col-lg-4">
        <table class="todays-game">
          <thead>
            <tr>
              <th>
                <img height="30" src="/img/logos/{{ $opponentLogo }}" alt="{{ $opponent->name }} logo"/>
                <strong>{{$opponent->name}}</strong>
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Games Played:</td><td class="right">{{ $opponent->gp }}</td>
            </tr>
            <tr>
              <td>Wins:</td><td class="right">{{ $opponent->wins }}</td>
            </tr>
            <tr>
              <td>Losses:</td><td class="right">{{ $opponent->losses }}</td>
            </tr>
            <tr>
              <td>OT:</td><td class="right">{{ $opponent->ot }}</td>
            </tr>
            <tr>
              <td>Points:</td><td class="right">{{ $opponent->points }}</td>
            </tr>
            <tr>
              <td>Goals For:</td><td class="right">{{ $opponent->gf }}</td>
            </tr>
            <tr>
              <td>Goals Against:</td><td class="right">{{ $opponent->ga }}</td>
            </tr>
          </tbody>
        </table>
    </div>
    <div class="col-lg-4">
      <table class="todays-game">
          <thead>
            <tr>
              <th>
                <img height="30"  src="/img/logos/toronto_maple_leafs.svg" alt="Toronto Maple Leafs logo" />
                <strong>Toronto Maple Leafs</strong>
              </th>
            </tr>
          </thead>
        <tbody>
          <tr>
            <td>Games Played:</td><td class="right">{{ $team->gp }}</td>
          </tr>
          <tr>
            <td>Wins:</td><td class="right">{{ $team->wins }}</td>
          </tr>
          <tr>
            <td>Losses:</td><td class="right">{{ $team->losses }}</td>
          </tr>
          <tr>
            <td>OT:</td><td class="right">{{ $team->ot }}</td>
          </tr>
          <tr>
            <td>Points:</td><td class="right">{{ $team->points }}</td>
          </tr>
          <tr>
            <td>Goals For:</td><td class="right">{{ $team->gf }}</td>
          </tr>
          <tr>
            <td>Goals Against:</td><td class="right">{{ $team->ga }}</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-lg-4">
      <table class="todays-game">
          <thead>
            <tr>
              <th>
                <strong>Difference</strong>
              </th>
            </tr>
          </thead>
        <tbody>
          <tr>
            <td>Games Played:</td><td class="right">{{ $team->gp - $opponent->gp }}</td>
          </tr>
          <tr>
            <td>Wins:</td><td class="right">{{ $team->wins - $opponent->wins }}</td>
          </tr>
          <tr>
            <td>Losses:</td><td class="right">{{ $team->losses - $opponent->losses }}</td>
          </tr>
          <tr>
            <td>OT:</td><td class="right">{{ $team->ot - $opponent->ot }}</td>
          </tr>
          <tr>
            <td>Points:</td><td class="right">{{ $team->points - $opponent->points }}</td>
          </tr>
          <tr>
            <td>Goals For:</td><td class="right">{{ $team->gf - $opponent->gf }}</td>
          </tr>
          <tr>
            <td>Goals Against:</td><td class="right">{{ $team->ga - $opponent->ga }}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  @if( !empty($head2head) )
  <hr>
  <h3 class="h4 reverse center">Head to Head</h3>
  <div class="row" style="text-align: center">
  @foreach($head2head as $game)
  @php
    $classHome = $game->home_score > $game->away_score ? 'winner' : 'loser';
    $classAway = $game->home_score < $game->away_score ? 'winner' : 'loser';
    if( $game->home_score === $game->away_score ) {
      $classHome = 'winner';
      $classAway = 'winner';
    }
    $period = ($game->final_period == '3rd')  && ($game->game_state == 'Final') ? 'Regulation' : $game->final_period;
  @endphp
  <div class="col-md-2" style="float:none; display:inline-block; text-align: left">
    <table>
      <tbody>
        <tr>
          <td colspan="2" class="center">{{ date( 'M j, g:ia', strtotime($game->date) ) }}</td>
        </tr>
        <tr class=" {{ $classAway }}">
          <td><img height="25" src="/img/logos/{{ $game->away_logo }}">&nbsp;{{ $game->away }}</td>
          <td class="right">{{ $game->away_score }}</td>
        </tr>
        <tr class=" {{ $classHome }}">
          <td><img height="25" src="/img/logos/{{ $game->home_logo }}">&nbsp;{{ $game->home }}</td>
          <td class="right">{{ $game->home_score }}</td>
        </tr>
        @if( $game->final_period !== 'Upcoming' )
        <tr><td colspan="2" class="center">{{ $period }}</td></tr>
        @endif
      </tbody>
    </table>
  </div>
    @endforeach
  </div>
  @endif
  <hr>
  <h3 class="h4 reverse center pb-0 mb-0">Around the League</h3>
  <div class="current-games">
    <div class="prev">
      <p><i class="fa fa-chevron-left"></i></p>
    </div>
    <div class="games-container">
      @if(isset($todaysGames['yesterday'][0]))
      <div class="game-date game-item"><p>{{ date( 'M j', strtotime($todaysGames['yesterday'][0]->date) ) }}</p></div>
      @foreach($todaysGames['yesterday'] as $game)
      @php
        $classHome = $game->home_score > $game->away_score ? 'winner' : 'loser';
        $classAway = $game->home_score < $game->away_score ? 'winner' : 'loser';
        if( $game->home_score === $game->away_score ) {
          $classHome = 'winner';
          $classAway = 'winner';
        }
        $period = ($game->final_period == '3rd')  && ($game->game_state == 'Final') ? 'Regulation' : $game->final_period;

      @endphp
      <div class="current-game game-item">
        <table>
          <tbody>
            <tr>
              <td colspan="2" class="center">{{ date( 'g:ia', strtotime($game->date) ) }}</td>
            </tr>
            <tr class="{{ $classAway }}">
              <td><img height="25" src="/img/logos/{{ $game->away_logo }}">&nbsp;{{ $game->away }}</td>
              <td class="right">{{ $game->away_score }}</td>
            </tr>
            <tr class=" {{ $classHome }}">
              <td><img height="25" src="/img/logos/{{ $game->home_logo }}">&nbsp;{{ $game->home }}</td>
              <td class="right">{{ $game->home_score }}</td>
            </tr>
            @if( $game->final_period !== 'Upcoming' )
            <tr><td colspan="2" class="center">{{ $period }}</td></tr>
            @endif
          </tbody>
        </table>
      </div>
      @endforeach
      @endif
      @if(isset($todaysGames['today'][0]))
      <div class="game-date game-date-today game-item"><p>{{ date( 'M j', strtotime($todaysGames['today'][0]->date) ) }}</p></div>
      @foreach($todaysGames['today'] as $game)
      @php
        $classHome = ($game->home_score > $game->away_score && $game->game_state == 'Final') ? 'winner' : 'loser';
        $classAway = $game->home_score < $game->away_score  && $game->game_state == 'Final'? 'winner' : 'loser';
        if( $game->home_score === $game->away_score || $game->game_state !=='Final' ) {
          $classHome = 'winner';
          $classAway = 'winner';
        }
        $period = (($game->final_period == '3rd')  && ($game->game_state == 'Final')) ? 'Final' : $game->final_period;

      @endphp
      <div class="current-game game-item">
        <table>
          <tbody>
            <tr>
              <td colspan="2" class="center">{{ date( 'g:ia', strtotime($game->date) ) }}</td>
            </tr>
            <tr class="{{ $classAway }}">
              <td><img height="25" src="/img/logos/{{ $game->away_logo }}">&nbsp;{{ $game->away }}</td>
              <td class="right">{{ $game->away_score }}</td>
            </tr>
            <tr class="{{ $classHome }}">
              <td><img height="25" src="/img/logos/{{ $game->home_logo }}">&nbsp;{{ $game->home }}</td>
              <td class="right">{{ $game->home_score }}</td>
            </tr>
            @if( $game->final_period !== 'Upcoming' )
            <tr><td colspan="2" class="center">{{ $period }}</td></tr>
            @endif
          </tbody>
        </table>
      </div>
      @endforeach
      @endif
      @if(isset($todaysGames['tomorrow'][0]))
      <div class="game-date game-item"><p>{{ date( 'M j', strtotime($todaysGames['tomorrow'][0]->date) ) }}</p></div>
      @foreach($todaysGames['tomorrow'] as $game)
      <div class="current-game game-item">
        <table>
          <tbody>
            <tr>
              <td colspan="2" class="center">{{ date( 'g:ia', strtotime($game->date) ) }}</td>
            </tr>
            <tr>
              <td><img height="25" src="/img/logos/{{ $game->away_logo }}">&nbsp;{{ $game->away }}</td>
              <td class="right">{{ $game->away_score }}</td>
            </tr>
            <tr>
              <td><img height="25" src="/img/logos/{{ $game->home_logo }}">&nbsp;{{ $game->home }}</td>
              <td class="right">{{ $game->home_score }}</td>
            </tr>
            @if( $game->final_period !== 'Upcoming' )
            <tr><td colspan="2" class="center">{{ $period }}</td></tr>
            @endif
          </tbody>
        </table>
      </div>
      @endforeach
      @endif
    </div>
    <div class="next">
      <p><i class="fa fa-chevron-right"></i></p>
    </div>
  </div>
</div>