  <div class="row">
    <div class="col-md-4">
      <h3>Even Strength</h3>
      <table>
        <tbody>
          <tr>
            <td><strong>Time on Ice</strong></td>
            <td class="right">{{ $player->even_toi }}</td>
          </tr>
          <tr>
            <td><strong>Goals</strong></td>
            <td class="right">{{ $even_goals }}</td>
          </tr>
          <tr>
            <td><strong>Assists</strong></td>
            <td class="right">{{ $even_assists }}</td>
          </tr>
          <tr>
            <td><strong>Points</strong></td>
            <td class="right">{{ $even_points }}</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-md-4">
      <h3>Power Play</h3>
      <table>
        <tbody>
          <tr>
            <td><strong>Time on Ice</strong></td>
            <td class="right">{{ $player->pp_toi }}</td>
          </tr>
          <tr>
            <td><strong>Goals</strong></td>
            <td class="right">{{ $pp_goals }}</td>
          </tr>
          <tr>
            <td><strong>Assists</strong></td>
            <td class="right">{{ $pp_assists }}</td>
          </tr>
          <tr>
            <td><strong>Points</strong></td>
            <td class="right">{{ $pp_points }}</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-md-4">
      <h3>Shorthanded</h3>
      <table>
        <tbody>
          <tr>
            <td><strong>Time on Ice</strong></td>
            <td class="right">{{ $player->sh_toi }}</td>
          </tr>
          <tr>
            <td><strong>Goals</strong></td>
            <td class="right">{{ $sh_goals }}</td>
          </tr>
          <tr>
            <td><strong>Assists</strong></td>
            <td class="right">{{ $sh_assists }}</td>
          </tr>
          <tr>
            <td><strong>Points</strong></td>
            <td class="right">{{ $sh_points }}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>