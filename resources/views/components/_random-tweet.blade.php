<div class="col-md-12">
  <h2>Random Leaf Tweet - {{$randomTweets[0]['user']['name']}}</h2>
  
  <div class="random-tweet">
    <p>{!! Twitter::linkify($randomTweets[0]['text']) !!}</p>
    <p class="tweet-time">
      <a href="https://twitter.com/intent/tweet?in_reply_to={{ $randomTweets[0]['id'] }}" target="_blank">
        <i class="fa fa-reply" aria-hidden="true"></i>
      </a>
      <a href="https://twitter.com/intent/retweet?tweet_id={{ $randomTweets[0]['id'] }}" target="_blank">
        <i class="fa fa-re$randomTweets[0]" aria-hidden="true"></i>
      </a>
      <a href="https://twitter.com/intent/like?tweet_id={{ $randomTweets[0]['id'] }}" target="_blank">
        <i class="fa fa-heart" aria-hidden="true"></i>
      </a>
      {{ Twitter::ago($randomTweets[0]['created_at']) }}
    </p>
  </div>
</div>