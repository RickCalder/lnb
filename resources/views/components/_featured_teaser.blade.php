 
<div class="featured-teaser-container">

  <div class="row">
     <a href="/articles/{{ $featured->slug }}" class="featured-teaser-link">
      <div class="featured-teaser col-md-12">
        <div class="header" style="background-image:url(storage/{{ $featured->image }})">
          <div class="screen"></div>
          <div class="featured-inner">
            <h2>{{ $featured->title }}</h2>
            <p class="excerpt hidden-xs">
              {{ $featured->excerpt }}... Read More >>
            </p>
          </div>
        </div>
      </div>
    </a>
  </div>
</div>