  <div class="row">
    <div class="col-md-4">
      <h3>Even Strength</h3>
      <table>
        <tbody>
          <tr>
            <td><strong>Shots</strong></td>
            <td class="right">{{ $player->even_shots }}</td>
          </tr>
          <tr>
            <td><strong>Saves</strong></td>
            <td class="right">{{ $player->even_saves }}</td>
          </tr>
          <tr>
            <td><strong>Save Percentage</strong></td>
            <td class="right">{{ number_format( ($player->even_save_pct /100), 3, '.', ',' ) }}</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-md-4">
      <h3>Power Play</h3>
      <table>
        <tbody>
          <tr>
            <td><strong>Shots</strong></td>
            <td class="right">{{ $player->pp_shots }}</td>
          </tr>
          <tr>
            <td><strong>Saves</strong></td>
            <td class="right">{{ $player->pp_saves }}</td>
          </tr>
          <tr>
            <td><strong>Save Percentage</strong></td>
            <td class="right">{{ number_format( ($player->pp_save_pct /100), 3, '.', ',' ) }}</td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-md-4">
      <h3>Shorthanded</h3>
      <table>
        <tbody>
          <tr>
            <td><strong>Shots</strong></td>
            <td class="right">{{ $player->sh_shots }}</td>
          </tr>
          <tr>
            <td><strong>Saves</strong></td>
            <td class="right">{{ $player->sh_saves }}</td>
          </tr>
          <tr>
            <td><strong>Save Percentage</strong></td>
            <td class="right">{{ number_format( ($player->sh_save_pct /100), 3, '.', ',' ) }}</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>