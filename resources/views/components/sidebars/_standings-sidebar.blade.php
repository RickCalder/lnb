@php
  foreach( $standings as $index => $team ){
    if($team->abbr === 'TOR') {
      if( substr($index+1, -1) == 1 ) {
        $atlantic = $index+1 . 'st';
      } else if( substr($index+1, -1) == 2 ) {
        $atlantic = $index+1 . 'nd';
      } else if( substr($index+1, -1) == 3 ) {
        $atlantic = $index+1 . 'rd';
      } else {
        $atlantic = $index+1 . 'th';  
      }
    }
  }
  foreach( $fullStandings as $index => $team ){
    if($team->abbr === 'TOR') {
      if( substr($index+1, -1) == 1 ) {
        $league = $index+1 . 'st';
      } else if( substr($index+1, -1) == 2 ) {
        $league = $index+1 . 'nd';
      } else if( substr($index+1, -1) == 3 ) {
        $league = $index+1 . 'rd';
      } else {
        $league = $index+1 . 'th';  
      }
    }
  }
  foreach( $fullStandings as $index => $team ){
    if($team->abbr === 'TOR') {
      $leafs = $team;
    }
  }
  foreach( $eastStandings as $index => $team ){
    if($team->abbr === 'TOR') {
      if( substr($index+1, -1) == 1 ) {
        $eastern = $index+1 . 'st';
      } else if( substr($index+1, -1) == 2 ) {
        $eastern = $index+1 . 'nd';
      } else if( substr($index+1, -1) == 3 ) {
        $eastern = $index+1 . 'rd';
      } else {
        $eastern = $index+1 . 'th';  
      }
    }
  }
@endphp

<div class="sidebar-component">
  <div class="section">
    <h2>Atlantic Standings</h2>
    <ol>
      @foreach($standings as $team)
        <li>{{$team->name}} <span style="float:right;margin-right: 15px;">{{$team->points}}</span></li>
      @endforeach
    </ol>
    <h2>Overall Standings</h2>
    <div class="standings-div">
      <p>Atlantic: {{ $atlantic }}</p>
      <p>Eastern: {{ $eastern }}</p>
      <p>League: {{ $league }}</p>
    </div>
    <p class="center button-container">
      <a href="{{ route('standings') }}" class="btn btn-primary">Full Standings</a>
    </p>
  </div>
</div>