<div class="sidebar-component col-md-12">
  <div class="section">
    <h2>Social Media</h2>
    <p>Join the conversation on your favourite social media platform!</p>
    <p class="center">
      <a href="{{ setting('site.twitter_url') }}" target="_blank" Title="twitter" class="sm twitter-sm">
        <i class="fa fa-twitter"></i>
      </a>
      <a href="{{ setting('site.fb_page') }}" target="_blank" Title="twitter" class="sm facebook-sm">
        <i class="fa fa-facebook"></i>
      </a>
      <a href="{{ setting('site.fb_group') }}" target="_blank" Title="twitter" class="sm facebook-sm">
        <i class="fa fa-users"></i>
      </a>
      <a href="{{ setting('site.instagram_url') }}" target="_blank" Title="twitter" class="sm instagram-sm">
        <i class="fa fa-instagram"></i>
      </a>
    </p>
  </div>
</div>