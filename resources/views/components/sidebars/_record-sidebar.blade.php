@php
// echo env('TEAM_ID');die;
  foreach ($standings as $item) {
    if($item->team_id == env('TEAM_ID')) {
      $team = $item;
      break;
    }
  }
@endphp

<div class="sidebar-component">
  <div class="section record-div">
    <h2>Record</h2>
      <p>Wins <span style="float:right">{{ $team->wins }}</span></p>
      <p>Losses <span style="float:right">{{ $team->losses }}</span></p>
      <p>OT <span style="float:right">{{ $team->ot }}</span></p>
      <p>Points <span style="float:right">{{ $team->points }}</span></p>
      <p>Goals For <span style="float:right">{{ $team->gf }}</span></p>
      <p>Goals Against <span style="float:right">{{ $team->ga }}</span></p>
      <p>Goal Differential<span style="float:right">{{ $team->gf - $team->ga }}</span></p>
      <p class="center button-container"><a href="{{ route('standings') }}" class="btn btn-primary">Full Standings</a></p>
  </div>
</div>