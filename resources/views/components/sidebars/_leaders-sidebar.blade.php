<div class="sidebar-component">
  <div class="section">
    <h2>Team Leaders</h2>
      <?php $player = $leaders['goals'][0]; $type='Goals'; ?>
      @include('components/_leader-single')
      <?php $player = $leaders['assists'][0]; $type='Assists'; ?>
      @include('components/_leader-single')
      <?php $player = $leaders['points'][0]; $type='Points'; ?>
      @include('components/_leader-single')
      <?php $player = $leaders['pim'][0]; $type='PIM'; ?>
      @include('components/_leader-single')
      

      <p class="center button-container"><a href="{{ route('stats') }}" class="btn btn-primary">All Statistics</a></p>
  </div>
</div>