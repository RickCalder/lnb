<?php 
  $win = strpos($games['last']->class, 'win') > -1 ? 'YES! <i class="fa fa-smile-o"></i>' : 'No. <i class="fa fa-frown-o"></i>';
  $period = ($games['last']->final_period == '3rd')  && ($games['last']->game_state == 'Final') ? 'Regulation' : $games['last']->final_period;
?>
<div class="sidebar-component">
  <div class="section">
    <h2>Did the Leafs Win?</h2>
    <p class="center">
      <strong>{!! $win !!}</strong><br><br>
      <img src="/img/logos/{{ $games['last']->home_logo }}" alt="{{ $games['last']->home_name }} logo" class="sidebar-schedule-logo"/>
       - <strong>{{ $games['last']->home_score }}</strong>&nbsp;&nbsp;vs&nbsp;&nbsp;
      <img src="/img/logos/{{ $games['last']->away_logo }}" alt="{{ $games['last']->away_name }} logo" class="sidebar-schedule-logo"/>
       - <strong>{{ $games['last']->away_score }}</strong><br><strong>{{ $period }}</strong>
    </p>
    <hr>
        @if( isset($games['next']))
    <p class="center">
      <strong>Next game:</strong><br>
      {{ $games['next']->date }}<br><br>
      <img src="/img/logos/{{ $games['next']->home_logo }}" alt="{{ $games['next']->home_name }} logo" class="sidebar-schedule-logo"/>
       vs 
      <img src="/img/logos/{{ $games['next']->away_logo }}" alt="{{ $games['next']->away_name }} logo" class="sidebar-schedule-logo"/>
    </p>
    @endif
    <p class="center button-container">
      <a href="{{ route('schedule') }}" class="btn btn-primary">Full Schedule</a>
    </p>
  </div>
</div>