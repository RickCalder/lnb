@if(! empty($feeds) )
  <div class="sidebar-component col-md-12">
    <div class="section">
      <h2>News From the Web</h2>
      <ul class="small">
        @foreach( $feeds as $feed )
          <li>
            <a href="{{ $feed['link'] }}" target="_blank">
              {{ $feed['title'] }}
            </a>
            <span> &mdash; {{ $feed['date'] }}</span>
          </li>
        @endforeach
      </ul>
    </div>
  </div>
@endif