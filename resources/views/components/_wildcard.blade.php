
<h2 class="reverse">{{ $data['conference'] }}</h2>
<table class="wildcard-table">
  <thead>
    <tr>
      <th>{{ $data['div1Title'] }}</th>
      <th>GP</th>
      <th>W</th>
      <th>L</th>
      <th>OT</th>
      <th>PTS</th>
      <th>ROW</th>
      <th>GF</th>
      <th>GA</th>
      <th>DIFF</th>
      <th>STREAK</th>
      <th class="never">Division</th>
      <th class="never">Conference</th>
    </tr>
  </thead>
  <tbody>
  @foreach($data['div1'] as $team)
  @php
    $logo = strtolower(str_replace("é","e",str_replace(".","",str_replace(" ", "_", $team->name)))) . '.svg';
  @endphp
    <tr>
      <td>
        <img width="30" height="30" src="/img/logos/{{ $logo }}" alt="{{ $team->name }} logo" class="sidebar-schedule-logo"/>
        <span class="hidden-xs">{{ $team->name }}</span>
        <span style="font-size: 0.75rem" class="team-toggle"><i class="fa fa-chevron-down"></i></span>
      </td>
      <td>{{ $team->gp }}</td>
      <td>{{ $team->wins }}</td>
      <td>{{ $team->losses }}</td>
      <td>{{ $team->ot }}</td>
      <td>{{ $team->points }}</td>
      <td>{{ $team->row }}</td>
      <td>{{ $team->gf }}</td>
      <td>{{ $team->ga }}</td>
      <td>{{ $team->gf - $team->ga }}</td>
      <td>{{ $team->streak_code }}</td>
      <td class="never">{{ $team->division }}</td>
      <td class="never">{{ $team->conference }}</td>
    </tr>
  @endforeach
  </tbody>
</table>
<table class="wildcard-table">
  <thead>
    <tr>
      <th>{{ $data['div2Title'] }}</th>
      <th>GP</th>
      <th>W</th>
      <th>L</th>
      <th>OT</th>
      <th>PTS</th>
      <th>ROW</th>
      <th>GF</th>
      <th>GA</th>
      <th>DIFF</th>
      <th>STREAK</th>
      <th class="never">Division</th>
      <th class="never">Conference</th>
    </tr>
  </thead>
  <tbody>
  @foreach($data['div2'] as $team)
  @php
    $logo = strtolower(str_replace("é","e",str_replace(".","",str_replace(" ", "_", $team->name)))) . '.svg';
  @endphp
    <tr>
      <td>
        <img width="30" height="30" src="/img/logos/{{ $logo }}" alt="{{ $team->name }} logo" class="sidebar-schedule-logo"/>
        <span class="hidden-xs">{{ $team->name }}</span>
        <span style="font-size: 0.75rem" class="team-toggle"><i class="fa fa-chevron-down"></i></span>
      </td>
      <td>{{ $team->gp }}</td>
      <td>{{ $team->wins }}</td>
      <td>{{ $team->losses }}</td>
      <td>{{ $team->ot }}</td>
      <td>{{ $team->points }}</td>
      <td>{{ $team->row }}</td>
      <td>{{ $team->gf }}</td>
      <td>{{ $team->ga }}</td>
      <td>{{ $team->gf - $team->ga }}</td>
      <td>{{ $team->streak_code }}</td>
      <td class="never">{{ $team->division }}</td>
      <td class="never">{{ $team->conference }}</td>
    </tr>
  @endforeach
  </tbody>
</table>
<table class="wildcard-table">
  <thead>
    <tr>
      <th>Wildcard</th>
      <th>GP</th>
      <th>W</th>
      <th>L</th>
      <th>OT</th>
      <th>PTS</th>
      <th>ROW</th>
      <th>GF</th>
      <th>GA</th>
      <th>DIFF</th>
      <th>STREAK</th>
      <th class="never">Division</th>
      <th class="never">Conference</th>
    </tr>
  </thead>
  <tbody class="rest-of-conference">
  @foreach($data['rest'] as $team)
  @php
    $logo = strtolower(str_replace("é","e",str_replace(".","",str_replace(" ", "_", $team->name)))) . '.svg';
  @endphp
    <tr>
      <td>
        <img width="30" height="30" src="/img/logos/{{ $logo }}" alt="{{ $team->name }} logo" class="sidebar-schedule-logo"/>
        <span class="hidden-xs">{{ $team->name }}</span>
        <span style="font-size: 0.75rem" class="team-toggle"><i class="fa fa-chevron-down"></i></span>
      </td>
      <td>{{ $team->gp }}</td>
      <td>{{ $team->wins }}</td>
      <td>{{ $team->losses }}</td>
      <td>{{ $team->ot }}</td>
      <td>{{ $team->points }}</td>
      <td>{{ $team->row }}</td>
      <td>{{ $team->gf }}</td>
      <td>{{ $team->ga }}</td>
      <td>{{ $team->gf - $team->ga }}</td>
      <td>{{ $team->streak_code }}</td>
      <td class="never">{{ $team->division }}</td>
      <td class="never">{{ $team->conference }}</td>
    </tr>
  @endforeach
  </tbody>
</table>