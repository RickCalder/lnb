@extends('layouts.app-plain')

@section('content')
<div class="container center">
  <div class="row">
    <div class="col-md-12">
      {!! $page->body !!}
      @include('components/_article_teaser')
    </div>
  </div>
</div>

@endsection