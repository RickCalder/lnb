@extends('layouts.app-plain')

@section('extra-head')
<link href="/css/calendar.css" rel="stylesheet">
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Segments</h1>
        {!! $page->body !!}
        @php
          $point_total = 0;
          $segment_games = 0;
          $segments_won = 0;
          $segments_lost = 0;
          $total_segments = 0;
        @endphp
        @foreach( $games as $index => $game )
          @php
            $where = 'vs';
            if( $game['game_class'] === 'away_game' ) {
              $where = '@';
            }
            $point_total += $game['points'];
            $class = 'not-played';
            if( $game['final_period'] !== 'Upcoming' ) {
              if( $game['points'] === 2 ) {
                $class='win';
              } else if( $game['points'] === 1 ) {
                $class='pity';
              } else {
                $class='loss';
              }
            } 
            if( $game['final_period'] !== 'Upcoming' ) {
              if($segment_games === 5){
                $segment_games = 0;
              }
              $segment_games++;
            } else {
              $segment_games  =0;
            }
          @endphp
          <div class="segment-block {{ $class }}">
            <p>{{ date('M j', strtotime($game['start'])) }}</p>
            <p>
              <img src="/img/logos/toronto_maple_leafs.svg" alt="Toronto Maple Leafs logo">
              {{ $where }} 
              <img src="/img/logos/{{ $game['opponent_logo'] }}" alt="{{ $game['opponent_name'] }} logo">
            </p>
            @if($game['final_period'] !== 'Upcoming')
              <p>
                {{ $game['leafs_score'] }}
                &nbsp;&mdash;&nbsp;
                {{ $game['opponent_score'] }}
              </p>
              <p class="small">Pts Gained: {{ $game['points'] }}</p>
            @else 
              <p>
                0
                &nbsp;&mdash;&nbsp;
                0
              </p>
              <p class="small">Pts Gained: {{ $game['points'] }}</p>
            @endif
          </div>
          @if( ($index + 1) % 5 === 0)
            <div class="segment-block">
              <p>Segment Total</p>
              <p>{{ $point_total }}/10</p>
              @if($segment_games === 5 || $point_total >= 6)
                @if( $point_total >= 6 )
                @php
                  $segments_won++;

                @endphp
                  <p class="segment-win">Segment Won</p>
                  <p class="small">Record: {{ $segments_won }}/{{ $segments_won + $segments_lost }}</p>
                @elseif( $point_total == 5 )
                  <p class="segment-tie">Break Even</p>
                  <p class="small">Record: {{ $segments_won }}/{{ $segments_won + $segments_lost }}</p>
                @elseif($segment_games === 5)
                @php 
                  $segments_lost++;
                @endphp
                  <p class="segment-loss">Segment Lost</p>
                  <p class="small">Record: {{ $segments_won }}/{{ $segments_won + $segments_lost }}</p>
                @endif
              @endif
              @php
                $point_total = 0;
              @endphp
            </div>
            <div class="clearfix"></div>
          @endif
        @endforeach
      </div>
    </div>
  </div>

@endsection

@section('scripts')
@endsection

