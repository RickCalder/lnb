@extends('layouts.app-plain')

@section('extra-head')
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <div class="page-container">
          <h1>Contact Us</h1>
          {!! $page->body !!}
          <form>
            <div class="form-group">
              <label for="name" class="sr-only">Name:</label>
              <input type="text" class="form-control" name="name" id="name" placeholder="Name">
            </div>
            <div class="form-group">
              <label for="email" class="sr-only">Email address:</label>
              <input type="email" class="form-control" name="email" id="email" placeholder="Email Address">
            </div>
            <div class="form-group">
              <label for="message" class="sr-only">Email address:</label>
              <textarea type="email" class="form-control" id="message" name="message" placeholder="Message"></textarea>
            </div>
            <div class="alert alert-success center">
              <p>You take a pass over centre ice, split the D, deke left, deke right...<br>
                You shoot, You Score!<br><br>Your message has been sent successfully. We'll get back to you ASAP!</p>
            </div>
            <div class="alert alert-danger center">
              <p>Here comes the pass from the corner, you wind up for a one timer and...<br>
                Oh no! You fan on your shot, there's a break away the other way, and...<br>
                Goal. There goes your plus/minus<br><br>Something went wrong. Please try again later.</p>
            </div>
            <p class="center">
              <button type="submit" class="btn btn-lg btn-primary contact-submit">Submit</button>
            </p>
          </form>
        </div>
      </div>
      <div class="col-lg-3 sidebar-container">
        <div class="row">
          @include('components/sidebars/_web-sidebar')
          @include('components/sidebars/_social-sidebar')
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
@endsection

