@extends('layouts.app')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-lg-9">
        <div class="page-container">
          {!! $page->body !!}
          <h2>The Maple Leafs on YouTube</h2>
          <div class="row">
            @for ($i = 1; $i < 4; $i++)
              <div class="col-md-4">
                @include('components/_youtube')
              </div>
            @endfor
            <div class="col-md-12">
              <p class="right"><a href="https://www.youtube.com/channel/UCMrpTL3zZyNA5S1p9B3FhWg" target="_blank">View more of TML on YouTube</a></p>
            </div>
          </div>
          <h2>Steve Dangle on YouTube</h2>
          <div class="row">
            @for ($i = 5; $i < 8; $i++)
              <div class="col-md-4">
                @include('components/_youtube')
              </div>
            @endfor
            <div class="col-md-12">
              <p class="right"><a href="https://www.youtube.com/channel/UCkUjSzthJUlO0uyUpiJfnxg" target="_blank">View more of Steve on YouTube</a></p>
            </div>
          </div>
          <div id="social-media"></div>
          <h2>Social Media</h2>
          <h3 class="reverse">Team</h3>
          <div class="row">
            @include('components/_player-media')
          </div>
        </div>
      </div>
      <div class="col-lg-3 sidebar-container">
        <div class="row">
          @include('components/sidebars/_web-sidebar')
          @include('components/sidebars/_social-sidebar')
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script>
  var lfr = $.get('https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=UUkUjSzthJUlO0uyUpiJfnxg&maxResults=3&key=AIzaSyD_zIaqBm2jHqejdWYKchwQWUtx8_vMO04',
   function( data ) {
    var items = data.items;
    for(var x=0; x <=2; x++) {
    $( this ).find(".loader").fadeOut();
      var div = "#video-" + (x + 5);
      $(div).html('<iframe src="https://www.youtube.com/embed/' + items[x].snippet.resourceId.videoId +'" frameborder="0" allowfullscreen></iframe>').fadeIn()
    }
  })

  var tml = $.get('https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=UUMrpTL3zZyNA5S1p9B3FhWg&maxResults=3&key=AIzaSyD_zIaqBm2jHqejdWYKchwQWUtx8_vMO04',
   function( data ) {
    var items = data.items;
    for(var x=0; x <=2; x++) {
    $( this ).find(".loader").hide();
      var div = "#video-" + (x + 1);
      $(div).html('<iframe src="https://www.youtube.com/embed/' + items[x].snippet.resourceId.videoId +'" frameborder="0" allowfullscreen></iframe>').fadeIn()
    }
  })
</script>
@endsection