@extends('layouts.app-plain')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h1>{{ env('TEAM') }} at a Glance</h1> 
        @if( isset($games['next']))
        <div class="row">
          @include('components/_gameday')
        </div>
        @endif
        <div class="row">
          <div class="col-md-6 col-sm-6 col-lg-3 front-col">
            @include('components/sidebars/_schedule-sidebar')
          </div>
          <div class="col-md-6 col-sm-6 col-lg-3 front-col">
            @include('components/sidebars/_standings-sidebar')
          </div>
          <div class="col-md-6 col-sm-6 col-lg-3 front-col">
            @include('components/sidebars/_leaders-sidebar')
          </div>
          <div class="col-md-6 col-sm-6 col-lg-3 front-col">
            @include('components/sidebars/_record-sidebar')
          </div>
        </div>
      </div>
      <div class="col-lg-9">
        <div class="page-container">
          @include('components/_featured_teaser')
          <div class="row">
            <div class="col-md-12">
              <p class="center">
                <a href="{{ route('articles') }}" class="btn btn-primary btn-lg">View all Articles</a>
              </p>
            </div>
          </div>
          <div class="row">
            @include('components/_random-tweet')
            <div class="col-md-12 center">
              <p><a href="/media#social-media" class="btn btn-primary btn-lg">View Leafs Social Media</a></p>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-3 sidebar-container">
        <div class="row">
          @include('components/sidebars/_web-sidebar')
          @include('components/sidebars/_social-sidebar')
        </div>
      </div>
    </div>

  </div>
@endsection