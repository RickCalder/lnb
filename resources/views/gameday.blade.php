@extends('layouts.app-plain')
@section('content')

@php 
$opponent = isset($todaysOpponent[0]) ? $todaysOpponent[0] : $nextOpponent[0];
$opponentLogo = strtolower(str_replace("é","e",str_replace(".","",str_replace(" ", "_", $opponent->name)))) . '.svg';
foreach ($standings as $item) {
  if($item->team_id == env('TEAM_ID')) {
    $team = $item;
    break;
  }
}
$where = $games['next']->awayId == env('TEAM_ID') ? 'Away Game' : 'Home Game';
@endphp
  <div class="container">
    <div class="row gameday-container">
      <div class="col-lg-12"> 
        <div class="gameday-thread">
          <h2 class="center">Game Day &mdash; {{ date('M j, g:ia', strtotime($games['next']->date)) }} &mdash; {{ $where }}</h2>
          <div class="team-col">
            <img src="/img/logos/{{ $opponentLogo }}" alt="{{ $opponent->name }} logo"/>
          </div>
          <div class="team-col">
            <img src="/img/logos/toronto_maple_leafs.svg" alt="Toronto Maple Leafs logo" />
          </div>
          <img class="gameday-logo" src="{{ asset('img/lnb-logo-small-50.png') }}" alt="Leaf's Nation Blueprint Logo">
        </div>
          <div class="row">
            <div class="col-lg-4">
                <table class="todays-game">
                  <thead>
                    <tr>
                      <th>
                        <img height="30" src="/img/logos/{{ $opponentLogo }}" alt="{{ $opponent->name }} logo"/>
                        <strong>{{$opponent->name}}</strong>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Games Played:</td><td class="right">{{ $opponent->gp }}</td>
                    </tr>
                    <tr>
                      <td>Wins:</td><td class="right">{{ $opponent->wins }}</td>
                    </tr>
                    <tr>
                      <td>Losses:</td><td class="right">{{ $opponent->losses }}</td>
                    </tr>
                    <tr>
                      <td>OT:</td><td class="right">{{ $opponent->ot }}</td>
                    </tr>
                    <tr>
                      <td>Points:</td><td class="right">{{ $opponent->points }}</td>
                    </tr>
                    <tr>
                      <td>Goals For:</td><td class="right">{{ $opponent->gf }}</td>
                    </tr>
                    <tr>
                      <td>Goals Against:</td><td class="right">{{ $opponent->ga }}</td>
                    </tr>
                  </tbody>
                </table>
            </div>
            <div class="col-lg-4">
              <table class="todays-game">
                  <thead>
                    <tr>
                      <th>
                        <img height="30"  src="/img/logos/toronto_maple_leafs.svg" alt="Toronto Maple Leafs logo" />
                        <strong>Toronto Maple Leafs</strong>
                      </th>
                    </tr>
                  </thead>
                <tbody>
                  <tr>
                    <td>Games Played:</td><td class="right">{{ $team->gp }}</td>
                  </tr>
                  <tr>
                    <td>Wins:</td><td class="right">{{ $team->wins }}</td>
                  </tr>
                  <tr>
                    <td>Losses:</td><td class="right">{{ $team->losses }}</td>
                  </tr>
                  <tr>
                    <td>OT:</td><td class="right">{{ $team->ot }}</td>
                  </tr>
                  <tr>
                    <td>Points:</td><td class="right">{{ $team->points }}</td>
                  </tr>
                  <tr>
                    <td>Goals For:</td><td class="right">{{ $team->gf }}</td>
                  </tr>
                  <tr>
                    <td>Goals Against:</td><td class="right">{{ $team->ga }}</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="col-lg-4">
              <table class="todays-game">
                  <thead>
                    <tr>
                      <th>
                        <strong>Difference</strong>
                      </th>
                    </tr>
                  </thead>
                <tbody>
                  <tr>
                    <td>Games Played:</td><td class="right">{{ $team->gp - $opponent->gp }}</td>
                  </tr>
                  <tr>
                    <td>Wins:</td><td class="right">{{ $team->wins - $opponent->wins }}</td>
                  </tr>
                  <tr>
                    <td>Losses:</td><td class="right">{{ $team->losses - $opponent->losses }}</td>
                  </tr>
                  <tr>
                    <td>OT:</td><td class="right">{{ $team->ot - $opponent->ot }}</td>
                  </tr>
                  <tr>
                    <td>Points:</td><td class="right">{{ $team->points - $opponent->points }}</td>
                  </tr>
                  <tr>
                    <td>Goals For:</td><td class="right">{{ $team->gf - $opponent->gf }}</td>
                  </tr>
                  <tr>
                    <td>Goals Against:</td><td class="right">{{ $team->ga - $opponent->ga }}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
      </div>
  </div>
  <div class="row" style="margin-top: 50px;">
    <div class="col-md-12">
      <button id="printimg" class="btn btn-primary btn-lg">Print</button>
    </div>
  </div>
</div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.5.0-beta4/html2canvas.min.js"></script>

<script>
$("#printimg").on("click", function(e) {
  e.preventDefault();
  PrintDiv($(".gameday-container"))
})
function PrintDiv(div) {
    html2canvas((div), {
      onrendered: function(canvas) {
        var myImage = canvas.toDataURL();
        downloadURI(myImage, "gameday.png");
      },
      width: 1410,
      height: 566,
      background: '#fff'
    });
}

function downloadURI(uri, name) {
    var link = document.createElement("a");

    link.download = name;
    link.href = uri;
    document.body.appendChild(link);
    link.click();   
    //after creating link you should delete dynamic link
    //clearDynamicLink(link); 
}

</script>
@endsection