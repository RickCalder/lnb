@extends('layouts.app-plain')

@section('extra-head')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Standings</h1>
        {!! $page->body !!}
        <h2 class="reverse"><span class="standings-type">Wild Card</span> Standings</h2>
        <form>
          <div class="form-group">
            <label for="standings-select" class="sr-only">Filter:</label>
            <select class="form-control" name="standings-select" id="standings-select">
              <option data-stype="wildcard" value="Wild Card">Wild Card</option>
              <option data-stype="league" value="League">League</option>
              <option data-stype="conference" value="Eastern">Eastern Conference</option>
              <option data-stype="conference" value="Western">Western Conference</option>
              <option data-stype="division" value="Atlantic">Atlantic Division</option>
              <option data-stype="division" value="Metropolitan">Metropolitan Division</option>
              <option data-stype="division" value="Central">Central Division</option>
              <option data-stype="division" value="Pacific">Pacific Division</option>
            </select>
          </div>
        </form>
        <div id="wildcard">
          @php
            $data['conference'] = 'Eastern Conference';
            $data['div1Title'] = 'Atlantic';
            $data['div2Title'] = 'Metropolitan';
            $data['div1'] = $wildcard['atlantic'];
            $data['div2'] = $wildcard['metro'];
            $data['rest'] = $wildcard['east'];
          @endphp
          @include('components/_wildcard')
          @php
            $data['conference'] = 'Western Conference';
            $data['div1Title'] = 'Central';
            $data['div2Title'] = 'Pacific';
            $data['div1'] = $wildcard['central'];
            $data['div2'] = $wildcard['pacific'];
            $data['rest'] = $wildcard['west'];
          @endphp
          @include('components/_wildcard')
        </div>
        <div id="full-standings">
          <table id="standingsTable">
            <thead>
              <tr>
                <th>Team</th>
                <th>GP</th>
                <th>W</th>
                <th>L</th>
                <th>OT</th>
                <th>PTS</th>
                <th>ROW</th>
                <th>GF</th>
                <th>GA</th>
                <th>DIFF</th>
                <th>STREAK</th>
                <th class="never">Division</th>
                <th class="never">Conference</th>
              </tr>
            </thead>
            <tbody>
            @foreach($standings as $team)
            @php
              $logo = strtolower(str_replace("é","e",str_replace(".","",str_replace(" ", "_", $team->name)))) . '.svg';
            @endphp
              <tr>
                <td>
                  <img width="30" height="30" src="/img/logos/{{ $logo }}" alt="{{ $team->name }} logo" class="sidebar-schedule-logo"/>
                  <span class="hidden-xs">{{ $team->name }}</span>
                  <span style="font-size: 0.75rem" class="team-toggle"><i class="fa fa-chevron-down"></i></span>
                </td>
                <td>{{ $team->gp }}</td>
                <td>{{ $team->wins }}</td>
                <td>{{ $team->losses }}</td>
                <td>{{ $team->ot }}</td>
                <td>{{ $team->points }}</td>
                <td>{{ $team->row }}</td>
                <td>{{ $team->gf }}</td>
                <td>{{ $team->ga }}</td>
                <td>{{ $team->gf - $team->ga }}</td>
                <td>{{ $team->streak_code }}</td>
                <td class="never">{{ $team->division }}</td>
                <td class="never">{{ $team->conference }}</td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript" charset="utf8" src="/standalone_js/datatables.min.js"></script>
<script type="text/javascript" charset="utf8" src="/standalone_js/standings.js"></script>
@endsection

