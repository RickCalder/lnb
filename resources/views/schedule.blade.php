@extends('layouts.app-plain')

@section('extra-head')
<link href="/css/calendar.css" rel="stylesheet">
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Schedule</h1>
        {!! $page->body !!}
        <div id="schedule"></div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div id="videoModal" class="modal fade" role="dialog" data-backdrop="static">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body game-video-modal">
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary close-video" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.0/moment.min.js"></script>
<script src="/standalone_js/fullcalendar.min.js"></script>
<script>var games = <?php echo $gameSchedule ?>;</script>
<script src="/standalone_js/calendar-create.js"></script>
@endsection

