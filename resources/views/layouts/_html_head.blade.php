<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
@if(isset($page))
  <meta name="description" content="{{ $page->meta_description }}">

    <!-- Social Media -->

  <!-- Common -->
  <meta property="og:url"                 content="{{ Request::url() }}" />
  <meta property="og:type"                content="website" />
  <meta property="og:title"               content="{{ config('app.name', 'Laravel') }} :: {{ $page->title }}" />
  <meta property="og:description"         content="{{ $page->meta_description }}" />
  @if( isset($page->image) )
  <meta property="og:image"               content="{{ config('app.url') . '/storage/' . $page->image }}" />
  @else
  <meta property="og:image"               content="{{ config('app.url') . '/storagepages/October2017/9782709-nhl-centennial-classic-detroit-red-wings-at-toronto-maple-leafs.jpeg' }}" />
  @endif
  <meta property="og:site_name"           content="Leafs Nation Blueprint" />

  <!-- Schema.org markup for Google+ -->
  <html itemscope itemtype="http://schema.org/WebPage">
  <meta itemprop="name"                   content="{{ config('app.name', 'Laravel') }} :: {{ $page->title }}">
  <meta itemprop="description"            content="{{ $page->meta_description }}" />
  @if( isset($page->image) )
  <meta itemprop="image"                  content="{{ config('app.url') . '/storage/' . $page->image }}" />
  @else
  <meta itemprop="image"                  content="{{ config('app.url') . '/storagepages/October2017/9782709-nhl-centennial-classic-detroit-red-wings-at-toronto-maple-leafs.jpeg'}}"/>
  @endif

  <!-- Twitter -->
  <meta name="twitter:card"               content="summary_large_image" />
  <meta name="twitter:site"               content="@leafsnb" />
  <meta name="twitter:creator"            content="@leafsnb" />
  <meta name="twitter:title"              content="{{ config('app.name', 'Laravel') }} :: {{ $page->title }}">
  <meta name="twitter:description"        content="{{ $page->meta_description }}" />
  @if( isset($page->image) )
  <meta name="twitter:image"              content="{{ config('app.url') . '/storage/' . $page->image }}" />
  @else
  <meta name="twitter:image"              content="{{ config('app.url') . '/storagepages/October2017/9782709-nhl-centennial-classic-detroit-red-wings-at-toronto-maple-leafs.jpeg'}}"/>
  @endif
@endif
  <title>{{ config('app.name', 'Laravel') }} @if(isset($page)) :: {{ $page->title }}@endif</title>

  <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/manifest.json">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="theme-color" content="#ffffff">

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,500,700%7CRaleway:400,600" rel="stylesheet">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  @yield('extra-head')
  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-109408108-1"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-109408108-1');
  </script>

</head>

