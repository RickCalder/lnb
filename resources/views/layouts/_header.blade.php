<div class="page-header" style="background-image:url(/storage/{{ $page->image }})">
  @include('layouts/_nav')
  <div class="screen"></div>
  <div class="container header-container">
    <div class="header-content">
      <h1>{{ $page->title }}</h1>
      <p>{{ $page->excerpt }}</p>
    </div>
  </div>
</div>