@include('layouts/_html_head')
<body>
  <div id="app">
    @include('layouts/_plain-header')

    @yield('content')
    @include('layouts/_footer')
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
@yield('scripts')

</body>
</html>
