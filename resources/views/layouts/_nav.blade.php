  <nav class="navbar navbar-default transparent-nav navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">

        <!-- Collapsed Hamburger -->
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
          <span class="sr-only">Toggle Navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <!-- Branding Image -->
        <a class="navbar-brand desktop-brand" href="{{ url('/') }}">
          <img src="{{ asset('img/lnb-logo-wide-50.png') }}" alt="Leaf's Nation Blueprint Logo">
        </a>
        <a class="navbar-brand mobile-brand" href="{{ url('/') }}">
          <img src="{{ asset('img/lnb-logo-small-50.png') }}" alt="Leaf's Nation Blueprint Logo">
        </a>
      </div>

      <div class="collapse navbar-collapse" id="app-navbar-collapse">
        <!-- Left Side Of Navbar -->
        <ul class="nav navbar-nav">
        </ul>

      <!-- Right Side Of Navbar -->
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            Stats <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li>
              <a href="{{ route('stats') }}">
                Player Stats
              </a>
            </li>
            <li>
              <a href="{{ route('standings') }}">
                Standings
              </a>
            </li>
            <li>
              <a href="{{ route('versus') }}">
                Versus
              </a>
            </li>
            <li>
              <a href="{{ route('segments') }}">
                Segments
              </a>
            </li>
          </ul>
        </li>
        <li>
          <a href="{{ route('schedule') }}">
            Schedule
          </a>
        </li>
        <li>
          <a href="{{ route('articles') }}">
            Articles
          </a>
        </li>
        <li>
          <a href="{{ route('media') }}">
            Media
          </a>
        </li>
        {{-- <li>
          <a href="{{ route('contact') }}">
            Contact
          </a>
        </li> --}}
        @guest
        <li>
          <a href="{{ url('/login') }}">
            Log in
          </a>
        </li>
        @else
        <li>
        <li class="dropdown">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <img class="user-avatar" src="/storage/{{ Auth::user()->avatar }}"/>
            {{ Auth::user()->name }} <span class="caret"></span>
          </a>
          <ul class="dropdown-menu">
            <li>
              <a href="{{ url('/logout') }}">
                Log Out
              </a>
            </li>
          </ul>
        </li>

        @endauth
      </ul>
    </div>
  </div>
</nav>