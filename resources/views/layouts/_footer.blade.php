<footer>
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <h4>Site Links</h4>
        {{ menu('Footer Site') }}
      </div>
      <div class="col-sm-4">
        <h4>Leaf Links</h4>
        {{ menu('Footer Links') }}
      </div>
      <div class="col-sm-4">
        @guest
        <h4>Profile</h4>
        @else
        <h4>{{ Auth::user()->name }}</h4>
        @endauth
        <ul>
          @guest
          <li><a href="{{ url('/login') }}" target="_self"><span>Log in</span></a></li>
          @else
          <li><a href="{{ url('/logout') }}" target="_self"><span>Log out</span></a></li>
          @endauth
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <p class="center copyright">&copy;{{ date('Y') }} Leafs Nation Blueprint</p>
        <p class="center copyright-small">All NHL and team names and imagery are trademarks™ or registered® trademarks of their respective holders. Use of them does not imply any affiliation with or endorsement by them.</p>
      </div>
    </div>
  </div>
</footer>