@extends('layouts.app-plain')
@section('content')
<?php
  $even_goals = $player->goals - ($player->pp_goals + $player->shg);
  $even_assists = $player->assists - (($player->pp_points - $player->pp_goals) + ($player->shp - $player->shg ));
  $even_points = $even_assists + $even_goals;
  $pp_goals = $player->pp_goals;
  $pp_assists = $player->pp_points - $player->pp_goals;
  $pp_points = $player->pp_points;
  $sh_goals = $player->shp  - $player->shp;
  $sh_assists = $player->shp - $player->shg;
  $sh_points = $player->shp;
  $twitter = $player->twitter ? '&nbsp;|&nbsp;<a href="https://twitter.com/' . $player->twitter . '" style="color:white" target="_blank"><i class="fa fa-twitter"></i></a>' : '';
?>
  <div class="container player-stats">
    <div class="row">
      <div class="col-md-12">
        <p style="margin-top:1rem"><a href="{{ route('stats') }}"><< Back to all statistics</a></p>
        <div>
          <div class="header">
            <img 
              src="https://nhl.bamcontent.com/images/headshots/current/168x168/{{ $player->league_id }}.jpg" 
              alt="Image of {{ $player->first_name }} {{ $player->last_name }}" 
              onerror="this.src = 'https://nhl.bamcontent.com/images/headshots/current/168x168/skater.jpg'">
              <h1 class="reverse">{{ $player->first_name }} {{ $player->last_name }} &mdash; #{{ $player->jersey_number }}</h1>
              <div class="stat-bar">
                <p>{{ $player->position }} &#124; {{ $player->height }} &#124; {{ $player->weight }}lbs &#124; Age: {{ $player->age }}{!! $twitter !!}</p>
              </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <p><strong>Birthday: </strong>{{ Carbon\Carbon::parse($player->birth_date)->format('F d, Y') }}</p>
        <p><strong>Birthplace: </strong>
          {{ $player->birth_city }},
          @if( $player->birth_state_province )
            {{ $player->birth_state_province }},
          @endif
          {{ $player->birth_country }}
        </p>
        <p>
          @if( $player->position_abbr != 'G' )
            <strong>Shoots: </strong>
            @if( $player->shoots_catches == 'L')
              Left
            @else
              Right
            @endif
          @else
            <strong>Catches: </strong>
            @if( $player->shoots_catches == 'L')
              Left
            @else
              Right
            @endif
          @endif
          <p><a href="https://www.nhl.com/player/{{ $player->nhl_slug }}" target="_blank">See more on NHL.com</a></p>
        </div>
        <div class="col-md-6">
          @if(!empty($tweets))
          <h3 class="h4 reverse pt0 mt0">{{ $player->first_name }} {{$player->last_name }} on Twitter</h3>
          
            @foreach($tweets as $tweet)
              <div class="tweet">
                <p>{!! Twitter::linkify($tweet['text']) !!}</p>
                <p class="tweet-time">
                  <a href="https://twitter.com/intent/tweet?in_reply_to={{ $tweet['id'] }}" target="_blank">
                    <i class="fa fa-reply" aria-hidden="true"></i>
                  </a>
                  <a href="https://twitter.com/intent/retweet?tweet_id={{ $tweet['id'] }}" target="_blank">
                    <i class="fa fa-retweet" aria-hidden="true"></i>
                  </a>
                  <a href="https://twitter.com/intent/like?tweet_id={{ $tweet['id'] }}" target="_blank">
                    <i class="fa fa-heart" aria-hidden="true"></i>
                  </a>
                  {{ Twitter::ago($tweet['created_at']) }}
                </p>
              </div>
            @endforeach
          @endif
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <h2>2017-2018 Stats for {{ $player->first_name }} {{ $player->last_name }}</h2>
          @if( $player->position_abbr == 'G' )
            @include('components/_goalie-stats-table')
        </div>
      </div>
            @include('components/_goalie-extra-stats')
          @else
            @include('components/_skater-stats-table')
        </div>
      </div>
            @include('components/_skater-extra-stats')

        @endif
  </div>
@endsection