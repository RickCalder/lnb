@extends('layouts.app')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <div class="page-container post-container">
          {!! $page->body !!}
        </div>
        @include('components/_sharing')
      </div>
      <div class="col-md-3 sidebar-container">
        <div class="row">
          @include('components/sidebars/_web-sidebar')
          @include('components/sidebars/_social-sidebar')
          @include('components/sidebars/_sharing-sidebar')
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/fitvids/1.2.0/jquery.fitvids.min.js" integrity="sha256-nA828blBCdUSSdI8jpciOZsf72IGt1eBcdx1ioEfa8o=" crossorigin="anonymous"></script>
<script>
  $(document).ready(function(){
    $(".post-container").fitVids();
  });
</script>
@endsection