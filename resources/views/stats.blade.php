@extends('layouts.app-plain')

@section('extra-head')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Player Stats</h1>
        {!! $page->body !!}
        <h2 class="reverse">Skaters</h2>
        <a href="" class="forwards"><button class="btn btn-primary">Forwards</button></a>
        <a href="" class="defense"><button class="btn btn-primary">Defensemen</button></a>
        <a href="" class="all"><button class="btn btn-primary">View All</button></a>
        <table id="skatersTable" class="stat-table">
          <thead>
            <tr>
              <th class="never">Type</th>
              <th>Player</th>
              <th>GP</th>
              <th>G</th>
              <th>A</th>
              <th>P</th>
              <th>+/-</th>
              <th>PIM</th>
              <th>PPG</th>
              <th>PPP</th>
              <th>SHG</th>
              <th>GWG</th>
              <th>OTG</th>
              <th>S</th>
              <th>S%</th>
              <th>FO%</th>
            </tr>
          </thead>
          <tbody>
          @foreach($skaters as $player)
            <tr>
              <td class="never">{{ $player->position_type }}</td>
              <td class="player-name">
                <a href="{{ route('player', $player->nhl_slug) }}" title="Link to {{ $player->first_name }} {{ $player->last_name }}">
                  <img 
                    src="https://nhl.bamcontent.com/images/headshots/current/60x60/{{ $player->league_id }}.jpg"
                    class="small-headshot"
                    alt="Image of {{ $player->first_name }} {{ $player->last_name}}" 
                    onerror="this.src = 'https://nhl.bamcontent.com/images/headshots/current/60x60/skater.jpg'">
                    <span class="name-break"></span>
                  {{ $player->first_name }} {{ $player->last_name}}
                </a>
              </td>
              <td>{{ $player->games }}</td>
              <td>{{ $player->goals }}</td>
              <td>{{ $player->assists }}</td>
              <td>{{ $player->points }}</td>
              <td>{{ $player->plus_minus }}</td>
              <td>{{ $player->pim }}</td>
              <td>{{ $player->pp_goals }}</td>
              <td>{{ $player->pp_points }}</td>
              <td>{{ $player->shg }}</td>
              <td>{{ $player->gwg }}</td>
              <td>{{ $player->otg }}</td>
              <td>{{ $player->shots }}</td>
              <td>{{ $player->shot_pct }}</td>
              <td>{{ $player->face_off_pct }}</td>
            </tr>
          @endforeach
        </tbody>
        </table>
        <h2 class="reverse">Goalies</h2>
        <table id="goaliesTable" class="stat-table">
          <thead>
            <tr>
              <th>Player</th>
              <th>GP</th>
              <th>GS</th>
              <th>W</th>
              <th>L</th>
              <th>T</th>
              <th>OT</th>
              <th>SA</th>
              <th>GA</th>
              <th>GAA</th>
              <th>S</th>
              <th>Sv%</th>
              <th>SO</th>
              <th>MIN</th>
            </tr>
          </thead>
          <tbody>
          @foreach($goalies as $player)
            <tr>
              <td class="player-name">
                <a href="{{ route('player', $player->nhl_slug) }}" title="Link to {{ $player->first_name }} {{ $player->last_name }}">
                  <img 
                    src="https://nhl.bamcontent.com/images/headshots/current/60x60/{{ $player->league_id }}.jpg"
                    class="small-headshot"
                    alt="Image of {{ $player->first_name }} {{ $player->last_name}}" 
                    onerror="this.src = 'https://nhl.bamcontent.com/images/headshots/current/60x60/skater.jpg'">
                    <span class="name-break"></span>
                  {{ $player->first_name }} {{ $player->last_name}}
                </a>
              </td>
              <td>{{ $player->games }}</td>
              <td>{{ $player->games_started }}</td>
              <td>{{ $player->wins }}</td>
              <td>{{ $player->losses }}</td>
              <td>{{ $player->ties }}</td>
              <td>{{ $player->ot }}</td>
              <td>{{ $player->shots_against }}</td>
              <td>{{ $player->goals_against }}</td>
              <td>{{ $player->goals_against_average }}</td>
              <td>{{ $player->saves }}</td>
              <td>{{ $player->save_pct }}</td>
              <td>{{ $player->shutouts }}</td>
              <td>{{ $player->toi }}</td>
            </tr>
          @endforeach
        </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript" charset="utf8" src="/standalone_js/datatables.min.js"></script>
<script type="text/javascript" charset="utf8" src="/standalone_js/stats.js"></script>
@endsection

