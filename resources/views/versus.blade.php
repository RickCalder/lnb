@extends('layouts.app-plain')

@section('extra-head')
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.css">
@endsection

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>The Leafs vs Everybody</h1>
        {!! $page->body !!}
        <h2 class="reverse"> Versus the <span class="standings-type">League</span></h2>
        <form>
          <div class="form-group">
            <label for="standings-select" class="sr-only">Filter:</label>
            <select class="form-control" name="standings-select" id="standings-select">
              <option data-stype="league" value="League">League</option>
              <option data-stype="conference" value="Eastern">Eastern Conference</option>
              <option data-stype="conference" value="Western">Western Conference</option>
              <option data-stype="division" value="Atlantic">Atlantic Division</option>
              <option data-stype="division" value="Metropolitan">Metropolitan Division</option>
              <option data-stype="division" value="Central">Central Division</option>
              <option data-stype="division" value="Pacific">Pacific Division</option>
            </select>
          </div>
        </form>
        @php
        $vs['eastern'] = array (
          'reg_win' => 0,
          'reg_loss' => 0,
          'ot_win' => 0,
          'ot_loss' => 0,
          'so_win' => 0,
          'so_loss' =>0,
          'gf' => 0,
          'ga' =>0
        );
        $vs['western'] = array (
          'reg_win' => 0,
          'reg_loss' => 0,
          'ot_win' => 0,
          'ot_loss' => 0,
          'so_win' => 0,
          'so_loss' =>0,
          'gf' => 0,
          'ga' =>0
        );
        $vs['atlantic'] = array (
          'reg_win' => 0,
          'reg_loss' => 0,
          'ot_win' => 0,
          'ot_loss' => 0,
          'so_win' => 0,
          'so_loss' =>0,
          'gf' => 0,
          'ga' =>0
        );
        $vs['metropolitan'] = array (
          'reg_win' => 0,
          'reg_loss' => 0,
          'ot_win' => 0,
          'ot_loss' => 0,
          'so_win' => 0,
          'so_loss' =>0,
          'gf' => 0,
          'ga' =>0
        );
        $vs['pacific'] = array (
          'reg_win' => 0,
          'reg_loss' => 0,
          'ot_win' => 0,
          'ot_loss' => 0,
          'so_win' => 0,
          'so_loss' =>0,
          'gf' => 0,
          'ga' =>0
        );
        $vs['central'] = array (
          'reg_win' => 0,
          'reg_loss' => 0,
          'ot_win' => 0,
          'ot_loss' => 0,
          'so_win' => 0,
          'so_loss' =>0,
          'gf' => 0,
          'ga' =>0
        );
        $vs['league'] = array (
          'reg_win' => 0,
          'reg_loss' => 0,
          'ot_win' => 0,
          'ot_loss' => 0,
          'so_win' => 0,
          'so_loss' =>0,
          'gf' => 0,
          'ga' =>0
        );

        foreach( $versus as $game ) {
          if( $game['conference']  === 'Eastern') {
            $vs['eastern']['reg_win'] += $game['reg_win'];
            $vs['eastern']['reg_loss'] += $game['reg_loss'];
            $vs['eastern']['ot_win'] += $game['ot_win'];
            $vs['eastern']['ot_loss'] += $game['ot_loss'];
            $vs['eastern']['so_win'] += $game['so_win'];
            $vs['eastern']['so_loss'] += $game['so_loss'];
            $vs['eastern']['gf'] += $game['gf'];
            $vs['eastern']['ga'] += $game['ga'];
          }
          if( $game['conference']  === 'Western') {
            $vs['western']['reg_win'] += $game['reg_win'];
            $vs['western']['reg_loss'] += $game['reg_loss'];
            $vs['western']['ot_win'] += $game['ot_win'];
            $vs['western']['ot_loss'] += $game['ot_loss'];
            $vs['western']['so_win'] += $game['so_win'];
            $vs['western']['so_loss'] += $game['so_loss'];
            $vs['western']['gf'] += $game['gf'];
            $vs['western']['ga'] += $game['ga'];
          }
          if( $game['division']  === 'Atlantic') {
            $vs['atlantic']['reg_win'] += $game['reg_win'];
            $vs['atlantic']['reg_loss'] += $game['reg_loss'];
            $vs['atlantic']['ot_win'] += $game['ot_win'];
            $vs['atlantic']['ot_loss'] += $game['ot_loss'];
            $vs['atlantic']['so_win'] += $game['so_win'];
            $vs['atlantic']['so_loss'] += $game['so_loss'];
            $vs['atlantic']['gf'] += $game['gf'];
            $vs['atlantic']['ga'] += $game['ga'];
          }
          if( $game['division']  === 'Pacific') {
            $vs['pacific']['reg_win'] += $game['reg_win'];
            $vs['pacific']['reg_loss'] += $game['reg_loss'];
            $vs['pacific']['ot_win'] += $game['ot_win'];
            $vs['pacific']['ot_loss'] += $game['ot_loss'];
            $vs['pacific']['so_win'] += $game['so_win'];
            $vs['pacific']['so_loss'] += $game['so_loss'];
            $vs['pacific']['gf'] += $game['gf'];
            $vs['pacific']['ga'] += $game['ga'];
          }
          if( $game['division']  === 'Metropolitan') {
            $vs['metropolitan']['reg_win'] += $game['reg_win'];
            $vs['metropolitan']['reg_loss'] += $game['reg_loss'];
            $vs['metropolitan']['ot_win'] += $game['ot_win'];
            $vs['metropolitan']['ot_loss'] += $game['ot_loss'];
            $vs['metropolitan']['so_win'] += $game['so_win'];
            $vs['metropolitan']['so_loss'] += $game['so_loss'];
            $vs['metropolitan']['gf'] += $game['gf'];
            $vs['metropolitan']['ga'] += $game['ga'];
          }
          if( $game['division']  === 'Central') {
            $vs['central']['reg_win'] += $game['reg_win'];
            $vs['central']['reg_loss'] += $game['reg_loss'];
            $vs['central']['ot_win'] += $game['ot_win'];
            $vs['central']['ot_loss'] += $game['ot_loss'];
            $vs['central']['so_win'] += $game['so_win'];
            $vs['central']['so_loss'] += $game['so_loss'];
            $vs['central']['gf'] += $game['gf'];
            $vs['central']['ga'] += $game['ga'];
          }
          $vs['league']['reg_win'] += $game['reg_win'];
          $vs['league']['reg_loss'] += $game['reg_loss'];
          $vs['league']['ot_win'] += $game['ot_win'];
          $vs['league']['ot_loss'] += $game['ot_loss'];
          $vs['league']['so_win'] += $game['so_win'];
          $vs['league']['so_loss'] += $game['so_loss'];
          $vs['league']['gf'] += $game['gf'];
          $vs['league']['ga'] += $game['ga'];
        }
        // echo '<xmp>'; print_r($vs); die;
        @endphp
        <div class="row hidden-xs">
          @foreach( $vs as $key => $value )
          <div class="col-md-12 vs-div" id="{{strtolower($key)}}-div">
            <table class="vs-table">
              <thead>
                <tr>
                  <th>Reg Wins</th>
                  <th>Reg Losses</th>
                  <th>OT Wins</th>
                  <th>OT Losses</th>
                  <th>SO Wins</th>
                  <th>SO Losses</th>
                  <th>GF</th>
                  <th>GA</th>
                  <th>DIFF</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>{{ $value['reg_win'] }}</td>
                  <td>{{ $value['reg_loss'] }}</td>
                  <td>{{ $value['ot_win'] }}</td>
                  <td>{{ $value['ot_loss'] }}</td>
                  <td>{{ $value['so_win'] }}</td>
                  <td>{{ $value['so_loss'] }}</td>
                  <td>{{ $value['gf'] }}</td>
                  <td>{{ $value['ga'] }}</td>
                  <td>{{ $value['gf'] - $value['ga'] }}</td>
                </tr>
            </tbody>
            </table>
          </div>
          @endforeach
        </div>
        <table id="standingsTable">
          <thead>
            <tr>
              <th>Team</th>
              <th>Reg Wins</th>
              <th>Reg Losses</th>
              <th>OT Wins</th>
              <th>OT Losses</th>
              <th>SO Wins</th>
              <th>SO Losses</th>
              <th>GF</th>
              <th>GA</th>
              <th>DIFF</th>
              <th class="never">Division</th>
              <th class="never">Conference</th>
            </tr>
          </thead>
          <tbody>
          @foreach($versus as $team)
          @if( $team['name'] == 'Toronto Maple Leafs')
            @php
              continue;
            @endphp
          @endif
            <tr>
              <td>
                <img width="30" height="30" src="/img/logos/{{ $team['logo'] }}" alt="{{ $team['name'] }} logo" class="sidebar-schedule-logo"/>
                <span class="hidden-xs">{{ $team['name'] }}</span>
              </td>
              <td>{{ $team['reg_win'] }}</td>
              <td>{{ $team['reg_loss'] }}</td>
              <td>{{ $team['ot_win'] }}</td>
              <td>{{ $team['ot_loss'] }}</td>
              <td>{{ $team['so_win'] }}</td>
              <td>{{ $team['so_loss'] }}</td>
              <td>{{ $team['gf'] }}</td>
              <td>{{ $team['ga'] }}</td>
              <td>{{ $team['gf'] - $team['ga'] }}</td>
              <td class="never">{{ $team['division'] }}</td>
              <td class="never">{{ $team['conference'] }}</td>
            </tr>
          @endforeach
        </tbody>
        </table>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
<script type="text/javascript" charset="utf8" src="/standalone_js/datatables.min.js"></script>
<script type="text/javascript" charset="utf8" src="/standalone_js/versus.js"></script>
@endsection

