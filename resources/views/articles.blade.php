@extends('layouts.app')
@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <div class="page-container">
          {!! $page->body !!}
          @include('components/_article_teaser')
        </div>
      </div>
      <div class="col-md-3 sidebar-container">
        <div class="row">
          @include('components/sidebars/_web-sidebar')
          @include('components/sidebars/_social-sidebar')
          @include('components/sidebars/_sharing-sidebar')
        </div>
      </div>
    </div>
  </div>
@endsection