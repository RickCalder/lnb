<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Twitter;

class Player extends Model
{
  protected $fillable = array(
    'league_id',
    'first_name',
    'last_name',
    'nhl_slug',
    'jersey_number',
    'birth_date',
    'age',
    'birth_city',
    'birth_state_province',
    'birth_country',
    'height',
    'weight',
    'shoots_catches',
    'position',
    'position_abbr',
    'position_type',
    'toi',
    'assists',
    'goals',
    'pim',
    'shots',
    'games',
    'hits',
    'pp_goals',
    'pp_points',
    'pp_toi',
    'even_toi',
    'sh_toi',
    'face_off_pct',
    'shot_pct',
    'gwg',
    'otg',
    'shg',
    'shp',
    'blocked',
    'plus_minus',
    'points',
    'shifts',
    'toi_game',
    'even_toi_game',
    'sh_toi_game',
    'pp_toi_game',
    'ot',
    'shutouts',
    'ties',
    'wins',
    'losses',
    'saves',
    'pp_saves',
    'sh_saves',
    'even_saves',
    'sh_shots',
    'even_shots',
    'pp_shots',
    'save_pct',
    'goals_against_average',
    'games_started',
    'shots_against',
    'goals_against',
    'pp_save_pct',
    'sh_save_pct',
    'even_save_pct',
    'twitter',
    'facebook',
    'web'
  );

  protected static function team_leaders() {
    $leaders['goals'] = Player::orderBy('goals', 'desc')->take(1)->get();
    $leaders['assists'] = Player::orderBy('assists', 'desc')->take(1)->get();
    $leaders['points'] = Player::orderBy('points', 'desc')->take(1)->get();
    $leaders['shots'] = Player::orderBy('shots', 'desc')->take(1)->get();
    $leaders['pim'] = Player::orderBy('pim', 'desc')->take(1)->get();
    return $leaders;
  }

  protected static function get_twitter($screenname) {
    $twitter =Twitter::getUserTimeline(['screen_name' => $screenname, 'count' => 50, 'format' => 'array', 'include_rts' => false]);
    // echo '<xmp>';print_r($twitter);die;
    return $twitter;
  }



  protected static function update_players() {
    $team_id = env('TEAM_ID');
    $season_id = env('SEASON_ID');
    $team = json_decode(file_get_contents('https://statsapi.web.nhl.com/api/v1/teams/' . $team_id .'?hydrate=franchise(roster(season=' . $season_id . ',person(name,stats(splits=statsSingleSeason))))'));
    $players = $team->teams[0]->franchise->roster->roster;
    // echo '<xmp>';print_r($players);die;
    $x = 0;
    foreach( $players as $player ) {
      // echo '<xmp>';print_r($player);die;
      // if(! isset($player->person->id)) {break;}
      $playerData = Player::firstOrNew(array('league_id' => $player->person->id));
      $playerData->league_id = $player->person->id;
      $playerData->first_name = $player->person->firstName;
      $playerData->last_name = $player->person->lastName;
      $playerData->jersey_number = $player->person->primaryNumber;
      $playerData->nhl_slug = $player->person->otherNames->slug;
      $playerData->birth_date = $player->person->birthDate;
      $playerData->age = isset($player->person->currentAge) ? $player->person->currentAge : 0;
      $playerData->birth_city = $player->person->birthCity;
      if(isset($player->person->birthStateProvince)) {
        $playerData->birth_state_province = $player->person->birthStateProvince;
      }
      $playerData->birth_country = $player->person->birthCountry;
      $playerData->height = $player->person->height;
      $playerData->weight = $player->person->weight;
      $playerData->shoots_catches = $player->person->shootsCatches;
      $playerData->position = $player->person->primaryPosition->name;
      $playerData->position_abbr = $player->person->primaryPosition->abbreviation;
      $playerData->position_type = $player->person->primaryPosition->type;
      if(!isset($player->person->stats[0]->splits[0])){$x++;$playerData->save();continue;}
      $playerData->toi_game = $player->person->stats[0]->splits[0]->stat->timeOnIcePerGame;
      $playerData->toi = $player->person->stats[0]->splits[0]->stat->timeOnIce;
      $playerData->games = $player->person->stats[0]->splits[0]->stat->games;
      if( $player->person->primaryPosition->code != 'G') {
        $playerData->assists = $player->person->stats[0]->splits[0]->stat->assists;
        $playerData->goals = $player->person->stats[0]->splits[0]->stat->goals;
        $playerData->pim = $player->person->stats[0]->splits[0]->stat->pim;
        $playerData->shots = $player->person->stats[0]->splits[0]->stat->shots;
        $playerData->hits = $player->person->stats[0]->splits[0]->stat->hits;
        $playerData->pp_goals = $player->person->stats[0]->splits[0]->stat->powerPlayGoals;
        $playerData->pp_points = $player->person->stats[0]->splits[0]->stat->powerPlayPoints;
        $playerData->pp_toi = $player->person->stats[0]->splits[0]->stat->powerPlayTimeOnIce;
        $playerData->even_toi = $player->person->stats[0]->splits[0]->stat->evenTimeOnIce;
        $playerData->sh_toi = $player->person->stats[0]->splits[0]->stat->shortHandedTimeOnIce;
        $playerData->face_off_pct = $player->person->stats[0]->splits[0]->stat->faceOffPct;
        $playerData->shot_pct = $player->person->stats[0]->splits[0]->stat->shotPct;
        $playerData->gwg = $player->person->stats[0]->splits[0]->stat->gameWinningGoals;
        $playerData->otg = $player->person->stats[0]->splits[0]->stat->overTimeGoals;
        $playerData->shg = $player->person->stats[0]->splits[0]->stat->shortHandedGoals;
        $playerData->shp = $player->person->stats[0]->splits[0]->stat->shortHandedPoints;
        $playerData->blocked = $player->person->stats[0]->splits[0]->stat->blocked;
        $playerData->plus_minus = $player->person->stats[0]->splits[0]->stat->plusMinus;
        $playerData->points = $player->person->stats[0]->splits[0]->stat->points;
        $playerData->shifts = $player->person->stats[0]->splits[0]->stat->shifts;
        $playerData->even_toi_game = $player->person->stats[0]->splits[0]->stat->evenTimeOnIcePerGame;
        $playerData->sh_toi_game = $player->person->stats[0]->splits[0]->stat->shortHandedTimeOnIcePerGame;
        $playerData->pp_toi_game = $player->person->stats[0]->splits[0]->stat->powerPlayTimeOnIcePerGame;
      } else {
        $playerData->ot = $player->person->stats[0]->splits[0]->stat->ot;
        $playerData->shutouts = $player->person->stats[0]->splits[0]->stat->shutouts;
        $playerData->ties = $player->person->stats[0]->splits[0]->stat->ties;
        $playerData->wins = $player->person->stats[0]->splits[0]->stat->wins;
        $playerData->losses = $player->person->stats[0]->splits[0]->stat->losses;
        $playerData->saves = $player->person->stats[0]->splits[0]->stat->saves;
        $playerData->pp_saves = $player->person->stats[0]->splits[0]->stat->powerPlaySaves;
        $playerData->sh_saves = $player->person->stats[0]->splits[0]->stat->shortHandedSaves;
        $playerData->even_saves = $player->person->stats[0]->splits[0]->stat->evenSaves;
        $playerData->sh_shots = $player->person->stats[0]->splits[0]->stat->shortHandedShots;
        $playerData->even_shots = $player->person->stats[0]->splits[0]->stat->evenShots;
        $playerData->pp_shots = $player->person->stats[0]->splits[0]->stat->powerPlayShots;
        $playerData->save_pct = $player->person->stats[0]->splits[0]->stat->savePercentage;
        $playerData->goals_against_average = $player->person->stats[0]->splits[0]->stat->goalAgainstAverage;
        $playerData->goals_against = $player->person->stats[0]->splits[0]->stat->goalsAgainst;
        $playerData->games_started = $player->person->stats[0]->splits[0]->stat->gamesStarted;
        $playerData->shots_against = $player->person->stats[0]->splits[0]->stat->shotsAgainst;
        $playerData->pp_save_pct = $player->person->stats[0]->splits[0]->stat->powerPlaySavePercentage;
        $playerData->sh_save_pct = isset($player->person->stats[0]->splits[0]->stat->shortHandedSavePercentage) ? $player->person->stats[0]->splits[0]->stat->shortHandedSavePercentage : 1.00;
        $playerData->even_save_pct = $player->person->stats[0]->splits[0]->stat->evenStrengthSavePercentage;
      }
      $playerData->save();
      echo $x;
      $x++;
    }
  }
}
