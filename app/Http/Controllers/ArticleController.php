<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Post;
use App\Schedule;
use App\Team;
use App\Traits\Feeds;

class ArticleController extends Controller {
  /**
   * Create a new controller instance.
   *
   * @return void
   */

  /**
   * Show the application homepage.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {
    $standings = Team::getAtlanticStandings();
    $fullStandings = Team::getFullStandings();
    $eastStandings = Team::getEasternStandings();
    $games = Team::GetNextPreviousGames();
    $feeds = Feeds::get();
    $page = Page::where('slug', '=', 'articles')->firstOrFail();
    $posts = Post::where('status', '=', 'PUBLISHED')->orderBy('created_at', 'DESC')->get();
    return view('articles', compact('page', 'posts', 'feeds', 'games', 'standings', 'fullStandings', 'eastStandings'));
  }

  public function show($slug) {
    $standings = Team::getAtlanticStandings();
    $fullStandings = Team::getFullStandings();
    $eastStandings = Team::getEasternStandings();
    $games = Team::GetNextPreviousGames();
    $feeds = Feeds::get();
    $page = Post::where('slug', '=', $slug)->firstOrFail();
    // echo '<xmp>';print_r($page);die;
    return view('article-single', compact('page',  'feeds', 'games', 'standings', 'fullStandings', 'eastStandings'));
  }
}
