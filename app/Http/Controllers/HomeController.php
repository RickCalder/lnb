<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Awjudd\FeedReader\FeedReaderServiceProvider;
use App\Page;
use App\Post;
use App\Schedule;
use App\Team;
use App\Player;
use App\Traits\Feeds;
use Twitter;

class HomeController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */

  /**
   * Show the application homepage.
   *
   * @return \Illuminate\Http\Response
   */
  public function index() {

    // $mn = Team::get_magic_number();
    
    $player = Player::where('twitter', '!=', null)->inRandomOrder()->take(1)->get();
    $randomTweets = Player::get_twitter($player[0]->twitter);
    $leaders = Player::team_leaders();
    $standings = Team::getAtlanticStandings();
    $fullStandings = Team::getFullStandings();
    $eastStandings = Team::getEasternStandings();
    $games = Team::GetNextPreviousGames();
    $feeds = Feeds::get();
    $page = Page::where('slug', '=', 'home')->firstOrFail();
    $posts = Post::where('status', '=', 'PUBLISHED')->orderBy('created_at','DESC')->take(3)->get();
    $featured = Post::where('featured', '=', 1)->orderBy('created_at','DESC')->firstOrFail();
    if( isset($games['next']) ) {
      $match_date = \DateTime::createFromFormat( "M d, Y g:ia", $games['next']->date );
    } else {
      $match_date = \DateTime::createFromFormat( "M d, Y g:ia", \date( "M d, Y g:ia" ) );;
    }
    
    $match_date->setTime(0,0,0);
    $today = new \DateTime();
    $today->setTime(0,0,0);
    $diff = $today->diff( $match_date );
    $diffDays = (integer)$diff->format( "%R%a" );
    $todaysOpponent = [];
    $nextOpponent = null;

    if( isset($games['next']) ) {
      $opponentID = $games['next']->awayId == env('TEAM_ID') ? $games['next']->homeId : $games['next']->awayId;
      if($diffDays === 0 ) {
        $todaysOpponent = Team::where('team_id', '=', $opponentID)->get();
        $games['today'] = $games['next'];
      } else {
        $nextOpponent = Team::where('team_id', '=', $opponentID)->get();
      }
      $oppId = isset($todaysOpponent[0]->team_id) ? $todaysOpponent[0]->team_id : $nextOpponent[0]->team_id;
      $head2head = Schedule::getHead2Head($oppId);
      $todaysGames = Team::getTodaysGames();
    }
    

    // echo '<xmp>'; print_r($todaysGames);die;
    return view('welcome', compact('page', 'posts', 'feeds', 'games', 'standings','leaders', 'fullStandings','eastStandings', 'featured', 'randomTweets', 'todaysOpponent', 'todaysGames', 'nextOpponent', 'head2head', 'mn'));
  }

  public function gameday() {
    $games = Team::GetNextPreviousGames();
    $feeds = Feeds::get();
    $standings = Team::getAtlanticStandings();
    $page = Page::where('slug', '=', 'home')->firstOrFail();
    $match_date = \DateTime::createFromFormat( "M d, Y g:ia", $games['next']->date );
    $match_date->setTime(0,0,0);
    $today = new \DateTime();
    $today->setTime(0,0,0);
    $diff = $today->diff( $match_date );
    $diffDays = (integer)$diff->format( "%R%a" );
    $todaysOpponent = [];
    $opponentID = $games['next']->awayId == env('TEAM_ID') ? $games['next']->homeId : $games['next']->awayId;
    if($diffDays === 0 ) {
      $todaysOpponent = Team::where('team_id', '=', $opponentID)->get();
      $games['today'] = $games['next'];
    } else {
      $nextOpponent = Team::where('team_id', '=', $opponentID)->get();
    }
    $oppId = isset($todaysOpponent[0]->team_id) ? $todaysOpponent[0]->team_id : $nextOpponent[0]->team_id;
    $head2head = Schedule::getHead2Head($oppId);
    $todaysGames = Team::getTodaysGames();
    

    // echo '<xmp>'; print_r($todaysGames);die;
    return view('gameday', compact('page','games', 'standings', 'todaysOpponent', 'todaysGames', 'nextOpponent', 'head2head'));
  }

  public function contact() {
    $standings = Team::getAtlanticStandings();
    $games = Team::GetNextPreviousGames();
    $feeds = Feeds::get();
    $page = Page::where('slug', '=', 'contact')->firstOrFail();
    return view('contact', compact('page', 'feeds', 'games', 'standings'));
  }

  public function media() {
    $leaders = Player::team_leaders();
    // echo '<xmp>';print_r($leaders);
    $standings = Team::getAtlanticStandings();
    $games = Team::GetNextPreviousGames();
    $feeds = Feeds::get();
    $page = Page::where('slug', '=', 'media')->firstOrFail();
    $posts = Post::where('status', '=', 'PUBLISHED')->take(3)->get();
    $players = Player::orderBy('last_name', 'ASC')->get();
    return view('media', compact('page', 'posts', 'feeds', 'games', 'standings','leaders', 'players'));
  }

  //Update database via APIs
  public function update() {
    // Schedule::update_schedule();
    Schedule::update_schedule_today();
    Team::update_standings();
    Player::update_players();
  }

  public function update_schedule_all(){
    Schedule::update_schedule();
  }

}
