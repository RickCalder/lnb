<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Awjudd\FeedReader\FeedReaderServiceProvider;
use App\Page;
use App\Schedule;
use App\Team;


class ScheduleController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */

  /**
   * Show the application homepage.
   *
   * @return \Illuminate\Http\Response
   */
  public function versus() {
    $versus = Schedule::get_versus();
    $teams = Team::getFullStandings();
    $page = Page::where('slug', '=', 'versus')->firstOrFail();
    return view('versus', compact('page', 'versus', 'teams'));
  }

  public function segments() {
    $games = Schedule::getFullSchedule();
    $page = Page::where('slug', '=', 'segments')->firstOrFail();
    // echo '<xmp>'; print_r($games);die;
    return view('segments', compact('page', 'games'));
  }

  public function index() {
    $gameSchedule = json_encode(Schedule::getFullSchedule());
    $page = Page::where('slug', '=', 'schedule')->firstOrFail();
    return view('schedule', compact('page', 'gameSchedule'));
  }
}
