<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Post;
use App\Team;
use App\Player;

class StatController extends Controller {
  /**
   * Create a new controller instance.
   *
   * @return void
   */

  /**
   * Show the application homepage.
   *
   * @return \Illuminate\Http\Response
   */
  public function standings() {
    $wildcard = Team::getWildCard();
    $standings = Team::getFullStandings();
    $page = Page::where('slug', '=', 'standings')->firstOrFail();
    return view('standings', compact('page', 'posts', 'feeds', 'games', 'standings', 'wildcard'));
  }

  public function stats() {
    $stats = Player::all();
    $skaters = Player::where('position_type', '!=', 'Goalie')->orderBy('points')->get();
    $goalies = Player::where('position_type', '=', 'Goalie')->get();
    // echo '<xmp>'; print_r($stats);die;
    $page = Page::where('slug', '=', 'player')->firstOrFail();
    return view('stats', compact('page', 'skaters', 'goalies'));
  }

  public function show($slug) {
    $id = explode('-', $slug);
    $player = Player::where('league_id', '=', $id[count($id)-1])->firstOrFail();
    $tweets = [];
    if(isset($player->twitter)) {
      $tweets = Player::get_twitter($player->twitter);
      $tweets = array_slice($tweets, 0, 5);
    }
    // echo '<xmp>'; print_r($tweets);die;
    $page = new \stdClass();
    $page->meta_description = 'Stats for ' . $player->first_name . ' ' . $player->last_name;
    $page->title = 'Stats for ' . $player->first_name . ' ' . $player->last_name;
    // echo '<xmp>'; print_r($player);die;
    return view('player-single', compact('player', 'page', 'tweets'));
  }
}
