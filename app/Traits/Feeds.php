<?php

namespace App\Traits;

use App\Team;

/**
 * Trait GetStandings
 * @package App\Traits
 *
 * Example usage:
 *
 * class MyController extends Controller
 * {
 *     use Feeds;
 *
 *     $feeds = Feeds::get();
 * }
 */
trait Feeds {
  public static function get() {
      $feed = \FeedReader::read('https://www.google.ca/alerts/feeds/04436023390450254344/8957369435999599719');
      $feeds = [];
      $x = 0;
      foreach( $feed->get_items() as $item ) {
        $feeds[$x]['link'] =  $item->get_link();
        $feeds[$x]['title'] = strip_tags($item->get_title());
        $date = new \DateTime($item->get_date(), new \DateTimeZone('America/New_York'));
        $feeds[$x]['date'] = date_format($date, 'M d, Y g:ia');
        if($x === 4) {
          break;
        }
        $x++;
      }
      return $feeds;
  }
}