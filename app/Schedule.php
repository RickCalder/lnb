<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
  protected $fillable = array(
    'video', 
    'date',
    'home',
    'away',
    'home_name',
    'away_name',
    'home_logo',
    'away_logo',
    'homeId',
    'awayId',
    'home_score',
    'away_score',
    'final_period',
    'winner',
    'winner_abbr',
    'class',
    'game_id',
    'recorded'
  );

  public static function getHead2Head( $teamId ) {
    $games = Schedule::where('final_period', '!=', 'Upcoming')->where(function($q) use($teamId){
      $q->where('awayId', $teamId)
        ->orWhere('homeId', $teamId);
    })->orderBy('game_id', 'ASC')->get();

    $h2h = [];
    foreach ($games as $game) {
      if( $game->awayId == env('TEAM_ID') || $game->homeId == env('TEAM_ID') ) {
        array_push($h2h, $game);
      }
    }
    return $h2h;
  }

  public static function get_versus( $teamId = 10 ) {
    $games = Schedule::where('final_period', '!=', 'Upcoming')->where(function($q) use($teamId){
      $q->where('awayId', $teamId)
        ->orWhere('homeId', $teamId);
    })->orderBy('game_id', 'ASC')->get();

    $teams = Team::all();       


    foreach( $teams as $team ) {
      $versus[$team->team_id] = array(
        'name' => $team->name,
        'logo' => strtolower(str_replace("é","e",str_replace(".","",str_replace(" ", "_", $team->name)))) . '.svg',
        'ot_loss' => 0,
        'ot_win' => 0,
        'reg_win' => 0,
        'reg_loss' => 0,
        'so_win' => 0,
        'so_loss' => 0,
        'gf' => 0,
        'ga' => 0,
        'division' => $team->division,
        'conference' => $team->conference
      );
      // $versus[];
    }
    // echo count($versus) . "hi"; echo '<xmp>'; print_r($versus); die;

    foreach( $games as $game ) {
      $oppId = $game->awayId === $teamId ? $game->homeId : $game->awayId;
      $opp = Team::where('team_id', '=', $oppId)->get();
      $home = $game->homeId === $teamId ? true : false;

      //Home Games
      if( $home ) {
        //Shootouts
        if($game->final_period === 'SO'){
          if($game->home_score > $game->away_score) {
            //Away stats
            $versus[$game->awayId]['so_win'] +=1;
            $versus[$game->awayId]['gf'] += $game->home_score;
            $versus[$game->awayId]['ga'] += $game->away_score;
          } else {
            $versus[$game->awayId]['so_loss'] +=1;
            $versus[$game->awayId]['gf'] += $game->home_score;
            $versus[$game->awayId]['ga'] += $game->away_score;
          }
        }
        //Overtime
        if($game->final_period === 'OT'){
          if($game->home_score > $game->away_score) {
            //Away stats
            $versus[$game->awayId]['ot_win'] +=1;
            $versus[$game->awayId]['gf'] += $game->home_score;
            $versus[$game->awayId]['ga'] += $game->away_score;
          } else {
            $versus[$game->awayId]['ot_loss'] +=1;
            $versus[$game->awayId]['gf'] += $game->home_score;
            $versus[$game->awayId]['ga'] += $game->away_score;
          }
        }
        //Regulation
        if($game->final_period === '3rd'){
          if($game->home_score > $game->away_score) {
            //Away stats
            $versus[$game->awayId]['reg_win'] +=1;
            $versus[$game->awayId]['gf'] += $game->home_score;
            $versus[$game->awayId]['ga'] += $game->away_score;
          } else {
            $versus[$game->awayId]['reg_loss'] +=1;
            $versus[$game->awayId]['gf'] += $game->home_score;
            $versus[$game->awayId]['ga'] += $game->away_score;
          }
        }
      }
      //Away Games
      if( ! $home ) {

        //Shootouts
        if($game->final_period === 'SO'){
          if($game->home_score < $game->away_score) {
            //Away stats
            $versus[$game->homeId]['so_win'] +=1;
            $versus[$game->homeId]['ga'] += $game->home_score;
            $versus[$game->homeId]['gf'] += $game->away_score;
          } else {
            $versus[$game->homeId]['so_loss'] +=1;
            $versus[$game->homeId]['ga'] += $game->home_score;
            $versus[$game->homeId]['gf'] += $game->away_score;
          }
        }
        //Overtime
        if($game->final_period === 'OT'){
          if($game->home_score < $game->away_score) {
            //Away stats
            $versus[$game->homeId]['ot_win'] +=1;
            $versus[$game->homeId]['ga'] += $game->home_score;
            $versus[$game->homeId]['gf'] += $game->away_score;
          } else {
            $versus[$game->homeId]['ot_loss'] +=1;
            $versus[$game->homeId]['ga'] += $game->home_score;
            $versus[$game->homeId]['gf'] += $game->away_score;
          }
        }
        //Regulation
        if($game->final_period === '3rd'){
          if($game->home_score < $game->away_score) {
            //Away stats
            $versus[$game->homeId]['reg_win'] +=1;
            $versus[$game->homeId]['ga'] += $game->home_score;
            $versus[$game->homeId]['gf'] += $game->away_score;
          } else {
            $versus[$game->homeId]['reg_loss'] +=1;
            $versus[$game->homeId]['ga'] += $game->home_score;
            $versus[$game->homeId]['gf'] += $game->away_score;
          }
        }
      }

    }
    // echo '<xmp>';print_r($versus);die;
    return $versus;
  }

  public static function getFullSchedule() {
    $schedule = Schedule::orderBy('game_id')->get();
    $games = [];
    $x = 0;
    foreach( $schedule as $game ) {
      if($game->homeId === 10 || $game->awayId === 10 ) {
        $t = strtotime($game->date);
        $games[$x]['start'] = date('Y-m-d',$t);
        $games[$x]['time'] = date('g:ia', $t);
        $games[$x]['points'] = 0;
        if( $game->awayId === 10 ) {
          $games[$x]['game_class'] = "away_game";
          $games[$x]['link'] = $game->video;
          $games[$x]['leafs_win'] = $game->away_score > $game->home_score ? 'win' : '';
          $games[$x]['opponent_win'] = $game->home_score > $game->away_score ? 'win' : '';
          $games[$x]['opponent_name'] = $game->home_name;
          $games[$x]['opponent_logo'] = $game->home_logo;
          $games[$x]['opponent_abbr'] = $game->home;
          $games[$x]['final_period'] = $game->final_period;
          $games[$x]['leaf_abbr'] = $game->away;
          if( $game->final_period !== 'Upcoming') {
            $games[$x]['leafs_score'] = $game->away_score;
            $games[$x]['opponent_score'] = $game->home_score;
            if($game->away_score > $game->home_score) {
              $games[$x]['points'] = 2;
            } else if (($game->home_score > $game->away_score) && ($game->final_period == '3rd')) {
              $games[$x]['points'] = 0;
            } else if (($game->home_score > $game->away_score) && ($game->final_period != '3rd')) {
              $games[$x]['points'] = 1;
            }
          }
        } else if($game->homeId ===10){
          $games[$x]['game_class'] = "home_game";
          $games[$x]['link'] = $game->video;
          $games[$x]['opponent_win'] = $game->away_score > $game->home_score ? 'win' : '';
          $games[$x]['leafs_win'] = $game->home_score > $game->away_score ? 'win' : '';
          $games[$x]['opponent_name'] = $game->away_name;
          $games[$x]['opponent_logo'] = $game->away_logo;
          $games[$x]['opponent_abbr'] = $game->away;
          $games[$x]['final_period'] = $game->final_period;
          $games[$x]['leaf_abbr'] = $game->home;
          if( $game->final_period !== 'Upcoming') {
            $games[$x]['leafs_score'] = $game->home_score;
            $games[$x]['opponent_score'] = $game->away_score;
            if($game->away_score < $game->home_score) {
              $games[$x]['points'] = 2;
            } else if (($game->home_score < $game->away_score) && ($game->final_period == '3rd')) {
              $games[$x]['points'] = 0;
            } else if (($game->home_score < $game->away_score) && ($game->final_period != '3rd')) {
              $games[$x]['points'] = 1;
            }
          }
        }
        $x++;
      };
    }
    return $games;
  }

  protected function update_schedule() {
    $teamIds = [1,2,3,4,5,6,7,8,9,10,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,28,29,30,52,53,54];
    // echo count($teamIds); die();
    for($a=0; $a<=30; $a++) {
      $posts = json_decode(file_get_contents('https://statsapi.web.nhl.com/api/v1/schedule?site=en_nhlCA&expand=schedule.teams,schedule.venue,schedule.metadata,schedule.linescore,schedule.decisions,schedule.game.content.media.epg&startDate=2018-10-03&endDate=2019-5-30&teamId='.$teamIds[$a]));
      
      for ($i=0;$i<=200;$i++ ) {
        if( ! isset($posts->dates[$i]) ) {
          break;
        }
        $game_id = $posts->dates[$i]->games[0]->gamePk;
        $gameData = Schedule::firstOrNew(array('game_id' => $game_id));
        $data = $posts->dates[$i]->games[0]->linescore; 
        $videos = isset($posts->dates[$i]->games[0]->content->media->epg[2]->items[0]->playbacks) ? $posts->dates[$i]->games[0]->content->media->epg[2]->items[0]->playbacks : array();
        $url="";
        for($x=0;$x <= count($videos); $x++){
          foreach($videos as $video) {
            $url = $video->url ? $video->url : "";
          }
          $gameData->video= $url;
        }
        $teams = $posts->dates[$i]->games[0]->teams;
        $t = strtotime($posts->dates[$i]->games[0]->gameDate);
        $gameData->date = date('M d, Y g:ia',$t);
        $gameData->game_id = $posts->dates[$i]->games[0]->gamePk;
        $gameData->game_date = date('Y-m-d G:i:s',$t);
        $gameData->home = $teams->home->team->abbreviation;
        $gameData->away = $teams->away->team->abbreviation;
        $gameData->home_name = $teams->home->team->name;
        $gameData->away_name = $teams->away->team->name;
        $gameData->home_logo =  strtolower(str_replace("é","e",str_replace(".","",str_replace(" ", "_", $gameData->home_name)))) . '.svg';
        $gameData->away_logo =  strtolower(str_replace("é","e",str_replace(".","",str_replace(" ", "_", $gameData->away_name)))) . '.svg';
        $gameData->homeId = $data->teams->home->team->id;
        $gameData->awayId = $data->teams->away->team->id;
        $gameData->home_score = $data->teams->home->goals;
        $gameData->away_score = $data->teams->away->goals;
        $gameData->game_state = $posts->dates[$i]->games[0]->status->detailedState;
        if(isset($data->currentPeriodOrdinal)) {
          $gameData->final_period = $data->currentPeriodOrdinal;
        }
        if( $gameData->home_score != $gameData->away_score && $gameData->home_score >= 0 && $gameData->away_score >= 0 ) {
          $gameData->winner = $gameData->home_score > $gameData->away_score ? $gameData->homeId : $gameData->awayId;
          $gameData->winner_abbr = $gameData->home_score > $gameData->away_score ? $gameData->home : $gameData->away;
          if( $gameData->winner == 10 && $data->currentPeriodOrdinal == '3rd' ) {
            $gameData->class = "leaf_win";
          } else if( $data->currentPeriodOrdinal == '3rd' && $gameData->winner != 10 ) {
            $gameData->class = "leaf_loss";
          } else if ( $data->currentPeriodOrdinal !== '3rd' && $gameData->winner == 10 ) {
            if($data->currentPeriodOrdinal == 'OT') {
              $gameData->class = "leaf_ot_win";
            } else {
              $gameData->class = "leaf_so_win";
            }
          } else if ( $data->currentPeriodOrdinal !== '3rd' && $gameData->winner != 10 ) {
            if($data->currentPeriodOrdinal == 'OT') {
              $gameData->class = "leaf_ot_loss";
            } else {
              $gameData->class = "leaf_so_loss";
            }
          }
        } else {
          $gameData->video = "";
          $gameData->winner = '';
          $gameData->winner_abbr = '';
          $gameData->class = '';
          $gameData->final_period = "Upcoming";
        }
        // echo '<xmp>';print_r($gameData->date);
        $gameData->save();
      }
    }
  }


  protected function update_schedule_today() {
    $teamIds = [1,2,3,4,5,6,7,8,9,10,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,28,29,30,52,53,54];
    // echo count($teamIds); die();

    $dateNew = new \DateTime();
    $today = new \DateTime();
    $today->setTime(0,0,0);
    
    $startDate= $dateNew->sub(new \DateInterval('P1D'));
    $startDate->setTime(0,0,0);

    $dateStart = date_format($startDate,"Y-m-d");
    $dateEnd = date_format($today,"Y-m-d");
    for($a=0; $a<=30; $a++) {
      $posts = json_decode(file_get_contents('https://statsapi.web.nhl.com/api/v1/schedule?site=en_nhlCA&expand=schedule.teams,schedule.venue,schedule.metadata,schedule.linescore,schedule.decisions,schedule.game.content.media.epg&startDate=' . $dateStart .'&endDate=' . $dateEnd .'&teamId='.$teamIds[$a]));
      if(empty($posts->dates)){continue;}
        $game_id = $posts->dates[0]->games[0]->gamePk;
        $gameData = Schedule::firstOrNew(array('game_id' => $game_id));
        $data = $posts->dates[0]->games[0]->linescore; 
        $videos = isset($posts->dates[0]->games[0]->content->media->epg[2]->items[0]->playbacks) ? $posts->dates[0]->games[0]->content->media->epg[2]->items[0]->playbacks : array();
        $url="";
        for($x=0;$x <= count($videos); $x++){
          foreach($videos as $video) {
            $url = $video->url ? $video->url : "";
          }
          $gameData->video= $url;
        }
        echo $url . '<br>';
        $teams = $posts->dates[0]->games[0]->teams;
        $t = strtotime($posts->dates[0]->games[0]->gameDate);
        $gameData->date = date('M d, Y g:ia',$t);
        $gameData->game_date = date('Y-m-d G:i:s',$t);
        $gameData->game_id = $posts->dates[0]->games[0]->gamePk;
        $gameData->home = $teams->home->team->abbreviation;
        $gameData->away = $teams->away->team->abbreviation;
        $gameData->home_name = $teams->home->team->name;
        $gameData->away_name = $teams->away->team->name;
        $gameData->home_logo =  strtolower(str_replace("é","e",str_replace(".","",str_replace(" ", "_", $gameData->home_name)))) . '.svg';
        $gameData->away_logo =  strtolower(str_replace("é","e",str_replace(".","",str_replace(" ", "_", $gameData->away_name)))) . '.svg';
        $gameData->homeId = $data->teams->home->team->id;
        $gameData->awayId = $data->teams->away->team->id;
        $gameData->home_score = $data->teams->home->goals;
        $gameData->away_score = $data->teams->away->goals;
        $gameData->game_state = $posts->dates[0]->games[0]->status->detailedState;
        if(isset($data->currentPeriodOrdinal)) {
          $gameData->final_period = $data->currentPeriodOrdinal;
        }
        if( $gameData->home_score != $gameData->away_score && $gameData->home_score >= 0 && $gameData->away_score >= 0 ) {
          $gameData->winner = $gameData->home_score > $gameData->away_score ? $gameData->homeId : $gameData->awayId;
          $gameData->winner_abbr = $gameData->home_score > $gameData->away_score ? $gameData->home : $gameData->away;
          if( $gameData->winner == 10 && $data->currentPeriodOrdinal == '3rd' ) {
            $gameData->class = "leaf_win";
          } else if( $data->currentPeriodOrdinal == '3rd' && $gameData->winner != 10 ) {
            $gameData->class = "leaf_loss";
          } else if ( $data->currentPeriodOrdinal !== '3rd' && $gameData->winner == 10 ) {
            if($data->currentPeriodOrdinal == 'OT') {
              $gameData->class = "leaf_ot_win";
            } else {
              $gameData->class = "leaf_so_win";
            }
          } else if ( $data->currentPeriodOrdinal !== '3rd' && $gameData->winner != 10 ) {
            if($data->currentPeriodOrdinal == 'OT') {
              $gameData->class = "leaf_ot_loss";
            } else {
              $gameData->class = "leaf_so_loss";
            }
          }
        } else {
          $gameData->video = "";
          $gameData->winner = '';
          $gameData->winner_abbr = '';
          $gameData->class = '';
          $gameData->final_period = "Upcoming";
        }
        $gameData->save();
    }
  }
}
