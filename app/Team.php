<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
  protected $fillable = array(
    'team_id', 
    'name',
    'abbr',
    'division',
    'conference',
    'wins',
    'losses',
    'ot',
    'ga',
    'gf',
    'points',
    'dr',
    'cr',
    'lr',
    'wr',
    'row',
    'gp',
    'streak',
    'streak_code',
  );

  protected static function get_magic_number() {
   $team = Team::where('abbr', '=', 'FLA')->first();
   $leafs = Team::where('abbr', '=', 'TOR')->first();
   $val['chaseNumber'] = (((82 - $team->gp ) *2 + $team->points) - $leafs->points) +1;
   $val['remaining'] = ((82 - $leafs->gp ) *2);
   return $val;

  }
  protected static function getAtlanticStandings() {
    $standings = Team::where('division', '=', 'Atlantic')->orderBy('dr')->get();
    return $standings;
  }

  protected static function getEasternStandings() {
    $standings = Team::where('conference', '=', 'Eastern')->orderBy('cr')->get();
    return $standings;
  }

  protected static function getFullStandings(){
    $standings = Team::orderBy('lr')->get();
    return $standings;
  }

  protected static function getWildCard() {
    $wildcard['metro'] = Team::where('division', '=', 'Metropolitan')->where('dr', '<', 4)->orderBy('lr')->get();
    $wildcard['pacific'] = Team::where('division', '=', 'Pacific')->where('dr', '<', 4)->orderBy('lr')->get();
    $wildcard['atlantic'] = Team::where('division', '=', 'Atlantic')->where('dr', '<', 4)->orderBy('lr')->get();
    $wildcard['central'] = Team::where('division', '=', 'Central')->where('dr', '<', 4)->orderBy('lr')->get();
    $wildcard['east'] = Team::where('conference', '=', 'Eastern')->where('dr', '>=', 4)->orderBy('lr')->get();
    $wildcard['west'] = Team::where('conference', '=', 'Western')->where('dr', '>=', 4)->orderBy('lr')->get();
    return $wildcard;
  }


  protected static function getNextPreviousGames() {
    $schedule = \App\Schedule::orderBy('game_id')->get();
    // $schedule = \App\Schedule::all();
    $games = [];
    // echo 'boo<xmp>';print_r($schedule);die;
    foreach( $schedule as $game ) {
        if($game->homeId === 10 || $game->awayId === 10 ) {
        // die($game->date);
        $game_date = strtotime( $game->date );
        if( $game_date - (time()-(60*60*1)) < 0 ) {
          $games['last'] = $game;
        } else {
          $games['next'] = $game;
          break;
        }
      }
    }
        // echo '<xmp>';print_r($games);die;
    return $games;
  }

  protected static function getTodaysGames() {
    $schedule = \App\Schedule::orderBy('game_date')->get();
    $games = [];
    $games['yesterday'] = [];
    $games['today'] = [];
    $games['tomorrow'] = [];
    $today = new \DateTime();
    $today->setTime(0,0,0);
    foreach( $schedule as $game ) {
      $match_date = \DateTime::createFromFormat( "M d, Y g:ia", $game->date );
      $match_date->setTime(0,0,0);
      $diff = $today->diff( $match_date );
      $diffDays = (integer)$diff->format( "%R%a" );
      if( $diffDays === -1 ) {
        array_push( $games['yesterday'], $game );
      } else if( $diffDays === 0 ) {
        array_push( $games['today'], $game );
      } else if( $diffDays === 1 ) {
        array_push( $games['tomorrow'], $game );
      }
    }
        // echo '<xmp>';print_r($games);die;
    return $games;
  }

  protected static function update_standings() {
    $season_id = env('SEASON_ID');
    $posts = json_decode(file_get_contents('https://statsapi.web.nhl.com/api/v1/standings?expand=standings.record,standings.team,standings.division,standings.conference,team.schedule.next,team.schedule.previous&season=' . $season_id));
    $x = 0;
    for(  $y=0; $y<=3; $y++ ) {
      $team1 = $posts->records[$y]->teamRecords;
      foreach( $team1 as $team ) {
        $teamData = Team::firstOrNew(array('team_id' => $team->team->id));
        $teamData->team_id = $team->team->id;
        $teamData->name = $team->team->name;
        $teamData->abbr = $team->team->abbreviation;
        $teamData->division = $team->team->division->name;
        $teamData->conference = $team->team->conference->name;
        $teamData->wins = $team->leagueRecord->wins;
        $teamData->losses = $team->leagueRecord->losses;
        $teamData->ot = $team->leagueRecord->ot;
        $teamData->ga = $team->goalsAgainst;
        $teamData->gf = $team->goalsScored;
        $teamData->points = $team->points;
        $teamData->dr = $team->divisionRank;
        $teamData->cr = $team->conferenceRank;
        $teamData->lr = $team->leagueRank;
        $teamData->wr = $team->wildCardRank;
        $teamData->row = $team->row;
        $teamData->gp = $team->gamesPlayed;
        $teamData->streak = $team->streak->streakNumber;
        $teamData->streak_code = $team->streak->streakCode;
        $teamData->save();
        echo $y;
        $x++;
      }
    }
  }

}
