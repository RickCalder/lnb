-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: lnb
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,NULL,1,'Blueprint Rookie Ranking','blueprint-rookie-ranking','2017-10-06 17:48:21','2017-10-14 04:57:30'),(2,NULL,1,'The Marlies Report','the-marlies-report','2017-10-06 17:48:21','2017-10-14 04:57:17'),(3,NULL,1,'Blueprint Game Reaction','blueprint-game-reaction','2017-10-14 04:59:14','2017-10-14 04:59:14'),(4,NULL,1,'The Maple Leafs','the-maple-leafs','2017-10-14 05:24:47','2017-10-14 05:24:47');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=206 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,'',1),(2,1,'author_id','text','Author',1,0,1,1,0,1,'',2),(3,1,'category_id','text','Category',1,0,1,1,1,0,'',3),(4,1,'title','text','Title',1,1,1,1,1,1,'',4),(5,1,'excerpt','text_area','excerpt',1,0,1,1,1,1,'',5),(6,1,'body','rich_text_box','Body',1,0,1,1,1,1,'',6),(7,1,'image','image','Post Image',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}',7),(8,1,'slug','text','slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}',8),(9,1,'meta_description','text_area','meta_description',1,0,1,1,1,1,'',9),(10,1,'meta_keywords','text_area','meta_keywords',1,0,1,1,1,1,'',10),(11,1,'status','select_dropdown','status',1,1,1,1,1,1,'{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}',11),(12,1,'created_at','timestamp','created_at',0,1,1,0,0,0,'',12),(13,1,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',13),(14,2,'id','number','id',1,0,0,0,0,0,'',1),(15,2,'author_id','text','author_id',1,0,0,0,0,0,'',2),(16,2,'title','text','title',1,1,1,1,1,1,'',3),(17,2,'excerpt','text_area','excerpt',1,0,1,1,1,1,'',4),(18,2,'body','rich_text_box','body',1,0,1,1,1,1,'',5),(19,2,'slug','text','slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\"}}',6),(20,2,'meta_description','text','meta_description',1,0,1,1,1,1,'',7),(21,2,'meta_keywords','text','meta_keywords',1,0,1,1,1,1,'',8),(22,2,'status','select_dropdown','status',1,1,1,1,1,1,'{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}',9),(23,2,'created_at','timestamp','created_at',1,1,1,0,0,0,'',10),(24,2,'updated_at','timestamp','updated_at',1,0,0,0,0,0,'',11),(25,2,'image','image','image',0,1,1,1,1,1,'',12),(26,3,'id','number','id',1,0,0,0,0,0,'',1),(27,3,'name','text','name',1,1,1,1,1,1,'',2),(28,3,'email','text','email',1,1,1,1,1,1,'',3),(29,3,'password','password','password',0,0,0,1,1,0,'',4),(30,3,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}',10),(31,3,'remember_token','text','remember_token',0,0,0,0,0,0,'',5),(32,3,'created_at','timestamp','created_at',0,1,1,0,0,0,'',6),(33,3,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',7),(34,3,'avatar','image','avatar',0,1,1,1,1,1,'',8),(35,5,'id','number','id',1,0,0,0,0,0,'',1),(36,5,'name','text','name',1,1,1,1,1,1,'',2),(37,5,'created_at','timestamp','created_at',0,0,0,0,0,0,'',3),(38,5,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',4),(39,4,'id','number','id',1,0,0,0,0,0,'',1),(40,4,'parent_id','select_dropdown','parent_id',0,0,1,1,1,1,'{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',2),(41,4,'order','text','order',1,1,1,1,1,1,'{\"default\":1}',3),(42,4,'name','text','name',1,1,1,1,1,1,'',4),(43,4,'slug','text','slug',1,1,1,1,1,1,'{\"slugify\":{\"origin\":\"name\"}}',5),(44,4,'created_at','timestamp','created_at',0,0,1,0,0,0,'',6),(45,4,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',7),(46,6,'id','number','id',1,0,0,0,0,0,'',1),(47,6,'name','text','Name',1,1,1,1,1,1,'',2),(48,6,'created_at','timestamp','created_at',0,0,0,0,0,0,'',3),(49,6,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',4),(50,6,'display_name','text','Display Name',1,1,1,1,1,1,'',5),(51,1,'seo_title','text','seo_title',0,1,1,1,1,1,'',14),(52,1,'featured','checkbox','featured',1,1,1,1,1,1,'',15),(53,3,'role_id','text','role_id',1,1,1,1,1,1,'',9),(129,11,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(130,11,'name','text','Name',1,1,1,1,1,1,NULL,3),(131,11,'category','select_dropdown','Category',1,1,1,1,1,1,'{\"default\":\"Leafs\",\"options\":{\"Leafs\":\"leafs\",\"Management/Team\":\"management\",\"Minors\":\"minors\",\"Alum\":\"alum\"}}',4),(132,11,'type','select_dropdown','Type',1,1,1,1,1,1,'{\"default\":\"Twitter\",\"options\":{\"Twitter\":\"twitter\",\"Facebook\":\"facebook\",\"Web\":\"web\"}}',5),(133,11,'url','text','Url',1,1,1,1,1,1,NULL,6),(134,11,'player_id','text','Player Id',0,1,1,1,1,1,NULL,2),(135,11,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,7),(136,11,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,8),(138,12,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(139,12,'league_id','checkbox','League Id',1,0,0,0,0,0,NULL,2),(140,12,'first_name','text','First Name',1,1,1,1,1,0,NULL,3),(141,12,'last_name','text','Last Name',1,1,0,1,1,0,NULL,4),(142,12,'nhl_slug','text','Nhl Slug',1,0,0,0,0,0,NULL,5),(143,12,'jersey_number','text','Jersey Number',1,0,1,1,0,0,NULL,6),(144,12,'birth_date','text','Birth Date',1,0,0,0,0,0,NULL,7),(145,12,'age','checkbox','Age',1,0,0,0,0,0,NULL,8),(146,12,'birth_city','checkbox','Birth City',1,0,0,0,0,0,NULL,9),(147,12,'birth_state_province','checkbox','Birth State Province',0,0,0,0,0,0,NULL,10),(148,12,'birth_country','checkbox','Birth Country',1,0,0,0,0,0,NULL,11),(149,12,'height','checkbox','Height',1,0,0,0,0,0,NULL,12),(150,12,'weight','checkbox','Weight',1,0,0,0,0,0,NULL,13),(151,12,'shoots_catches','checkbox','Shoots Catches',1,0,0,0,0,0,NULL,14),(152,12,'position','checkbox','Position',1,0,0,0,0,0,NULL,15),(153,12,'position_abbr','checkbox','Position Abbr',1,0,0,0,0,0,NULL,16),(154,12,'position_type','checkbox','Position Type',1,0,0,0,0,0,NULL,17),(155,12,'toi','checkbox','Toi',0,0,0,0,0,0,NULL,18),(156,12,'assists','checkbox','Assists',0,0,0,0,0,0,NULL,19),(157,12,'goals','checkbox','Goals',0,0,0,0,0,0,NULL,20),(158,12,'pim','checkbox','Pim',0,0,0,0,0,0,NULL,21),(159,12,'shots','checkbox','Shots',0,0,0,0,0,0,NULL,22),(160,12,'games','checkbox','Games',0,0,0,0,0,0,NULL,23),(161,12,'hits','checkbox','Hits',0,0,0,0,0,0,NULL,24),(162,12,'pp_goals','checkbox','Pp Goals',0,0,0,0,0,0,NULL,25),(163,12,'pp_points','checkbox','Pp Points',0,0,0,0,0,0,NULL,26),(164,12,'pp_toi','checkbox','Pp Toi',0,0,0,0,0,0,NULL,27),(165,12,'even_toi','checkbox','Even Toi',0,0,0,0,0,0,NULL,28),(166,12,'sh_toi','checkbox','Sh Toi',0,0,0,0,0,0,NULL,29),(167,12,'face_off_pct','checkbox','Face Off Pct',0,0,0,0,0,0,NULL,30),(168,12,'shot_pct','checkbox','Shot Pct',0,0,0,0,0,0,NULL,31),(169,12,'gwg','checkbox','Gwg',0,0,0,0,0,0,NULL,32),(170,12,'otg','checkbox','Otg',0,0,0,0,0,0,NULL,33),(171,12,'shg','checkbox','Shg',0,0,0,0,0,0,NULL,34),(172,12,'shp','checkbox','Shp',0,0,0,0,0,0,NULL,35),(173,12,'blocked','checkbox','Blocked',0,0,0,0,0,0,NULL,36),(174,12,'plus_minus','checkbox','Plus Minus',0,0,0,0,0,0,NULL,37),(175,12,'points','checkbox','Points',0,0,0,0,0,0,NULL,38),(176,12,'shifts','checkbox','Shifts',0,0,0,0,0,0,NULL,39),(177,12,'toi_game','checkbox','Toi Game',0,0,0,0,0,0,NULL,40),(178,12,'even_toi_game','checkbox','Even Toi Game',0,0,0,0,0,0,NULL,41),(179,12,'sh_toi_game','checkbox','Sh Toi Game',0,0,0,0,0,0,NULL,42),(180,12,'pp_toi_game','checkbox','Pp Toi Game',0,0,0,0,0,0,NULL,43),(181,12,'ot','checkbox','Ot',0,0,0,0,0,0,NULL,44),(182,12,'shutouts','checkbox','Shutouts',0,0,0,0,0,0,NULL,45),(183,12,'ties','checkbox','Ties',0,0,0,0,0,0,NULL,46),(184,12,'wins','checkbox','Wins',0,0,0,0,0,0,NULL,47),(185,12,'losses','checkbox','Losses',0,0,0,0,0,0,NULL,48),(186,12,'saves','checkbox','Saves',0,0,0,0,0,0,NULL,49),(187,12,'pp_saves','checkbox','Pp Saves',0,0,0,0,0,0,NULL,50),(188,12,'sh_saves','checkbox','Sh Saves',0,0,0,0,0,0,NULL,51),(189,12,'even_saves','checkbox','Even Saves',0,0,0,0,0,0,NULL,52),(190,12,'sh_shots','checkbox','Sh Shots',0,0,0,0,0,0,NULL,53),(191,12,'even_shots','checkbox','Even Shots',0,0,0,0,0,0,NULL,54),(192,12,'pp_shots','checkbox','Pp Shots',0,0,0,0,0,0,NULL,55),(193,12,'save_pct','checkbox','Save Pct',0,0,0,0,0,0,NULL,56),(194,12,'goals_against_average','checkbox','Goals Against Average',0,0,0,0,0,0,NULL,57),(195,12,'games_started','checkbox','Games Started',0,0,0,0,0,0,NULL,58),(196,12,'shots_against','checkbox','Shots Against',0,0,0,0,0,0,NULL,59),(197,12,'goals_against','checkbox','Goals Against',0,0,0,0,0,0,NULL,60),(198,12,'pp_save_pct','checkbox','Pp Save Pct',0,0,0,0,0,0,NULL,61),(199,12,'sh_save_pct','checkbox','Sh Save Pct',0,0,0,0,0,0,NULL,62),(200,12,'even_save_pct','checkbox','Even Save Pct',0,0,0,0,0,0,NULL,63),(201,12,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,64),(202,12,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,65),(203,12,'twitter','text','Twitter',0,1,1,1,1,1,NULL,66),(204,12,'facebook','text','Facebook',0,1,1,1,1,1,NULL,67),(205,12,'web','text','Web',0,1,1,1,1,1,NULL,68);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'posts','posts','Post','Posts','voyager-news','TCG\\Voyager\\Models\\Post','TCG\\Voyager\\Policies\\PostPolicy','','',1,0,'2017-10-06 17:48:21','2017-10-06 17:48:21'),(2,'pages','pages','Page','Pages','voyager-file-text','TCG\\Voyager\\Models\\Page',NULL,'','',1,0,'2017-10-06 17:48:21','2017-10-06 17:48:21'),(3,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','','',1,0,'2017-10-06 17:48:21','2017-10-06 17:48:21'),(4,'categories','categories','Category','Categories','voyager-categories','TCG\\Voyager\\Models\\Category',NULL,'','',1,0,'2017-10-06 17:48:21','2017-10-06 17:48:21'),(5,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,'2017-10-06 17:48:21','2017-10-06 17:48:21'),(6,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,'2017-10-06 17:48:21','2017-10-06 17:48:21'),(11,'social_media','social-media','Social Media','Social Sedia',NULL,'App\\SocialMedia',NULL,NULL,NULL,1,0,'2017-10-26 11:25:28','2017-10-26 11:30:29'),(12,'players','players','Player','Players',NULL,'App\\Player',NULL,NULL,NULL,1,0,'2017-10-26 12:23:15','2017-10-26 12:23:15');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2017-10-06 17:48:21','2017-10-06 17:48:21','voyager.dashboard',NULL),(2,1,'Media','','_self','voyager-images',NULL,NULL,4,'2017-10-06 17:48:21','2017-10-26 11:53:43','voyager.media.index',NULL),(3,1,'Posts','','_self','voyager-news',NULL,NULL,5,'2017-10-06 17:48:21','2017-10-26 11:53:43','voyager.posts.index',NULL),(4,1,'Users','','_self','voyager-person',NULL,NULL,3,'2017-10-06 17:48:21','2017-10-06 17:48:21','voyager.users.index',NULL),(5,1,'Categories','','_self','voyager-categories',NULL,NULL,8,'2017-10-06 17:48:21','2017-10-06 17:48:21','voyager.categories.index',NULL),(6,1,'Pages','','_self','voyager-file-text',NULL,NULL,6,'2017-10-06 17:48:21','2017-10-26 11:53:43','voyager.pages.index',NULL),(7,1,'Roles','','_self','voyager-lock',NULL,NULL,2,'2017-10-06 17:48:21','2017-10-06 17:48:21','voyager.roles.index',NULL),(8,1,'Tools','','_self','voyager-tools',NULL,NULL,9,'2017-10-06 17:48:21','2017-10-06 17:48:21',NULL,NULL),(9,1,'Menu Builder','','_self','voyager-list',NULL,8,1,'2017-10-06 17:48:21','2017-10-26 11:53:43','voyager.menus.index',NULL),(10,1,'Database','','_self','voyager-data',NULL,8,2,'2017-10-06 17:48:21','2017-10-26 11:53:43','voyager.database.index',NULL),(11,1,'Compass','/admin/compass','_self','voyager-compass',NULL,8,3,'2017-10-06 17:48:21','2017-10-26 11:53:43',NULL,NULL),(12,1,'Hooks','/admin/hooks','_self','voyager-hook','#000000',8,4,'2017-10-06 17:48:21','2017-11-05 09:04:26',NULL,''),(13,1,'Settings','','_self','voyager-settings',NULL,NULL,10,'2017-10-06 17:48:21','2017-10-26 11:53:43','voyager.settings.index',NULL),(14,2,'Home','/','_self',NULL,'#000000',NULL,15,'2017-10-09 11:55:45','2017-10-09 11:55:45',NULL,''),(15,2,'Articles','/articles','_self',NULL,'#000000',NULL,16,'2017-10-09 11:56:02','2017-10-09 11:56:02',NULL,''),(16,2,'Statistics','/stats','_self',NULL,'#000000',NULL,17,'2017-10-09 11:56:21','2017-10-09 11:56:21',NULL,''),(17,2,'Schedule','/schedule','_self',NULL,'#000000',NULL,18,'2017-10-09 11:56:41','2017-10-09 11:56:41',NULL,''),(18,2,'Contact','/contact','_self',NULL,'#000000',NULL,19,'2017-10-09 11:56:59','2017-10-09 11:56:59',NULL,''),(20,3,'Maple Leafs Website','https://www.nhl.com/mapleleafs','_blank',NULL,'#000000',NULL,1,'2017-10-09 12:05:45','2017-10-09 12:06:17',NULL,''),(21,3,'Maple Leafs Twitter','https://twitter.com/MapleLeafs','_blank',NULL,'#000000',NULL,2,'2017-10-09 12:06:47','2017-10-09 12:08:20',NULL,''),(22,3,'Maple Leafs Facebook','https://www.facebook.com/torontomapleleafs/','_blank',NULL,'#000000',NULL,3,'2017-10-09 12:07:05','2017-10-09 12:08:20',NULL,''),(23,3,'Maple Leafs Instagram','https://www.instagram.com/MapleLeafs/','_blank',NULL,'#000000',NULL,4,'2017-10-09 12:07:25','2017-10-09 12:08:20',NULL,''),(24,3,'Maple Leafs YouTube','https://www.youtube.com/torontomapleleafs','_blank',NULL,'#000000',NULL,5,'2017-10-09 12:07:47','2017-10-09 12:08:20',NULL,''),(25,1,'Team Social Media','/admin/social-media','_self','voyager-browser','#000000',NULL,7,'2017-10-26 11:53:35','2017-10-26 11:54:50',NULL,''),(26,1,'Site','https://leafs.nationblueprint.com','_self',NULL,'#000000',NULL,11,'2017-11-05 08:56:38','2017-11-05 09:05:49',NULL,'');
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2017-10-06 17:48:21','2017-10-06 17:48:21'),(2,'Footer Site','2017-10-09 11:55:14','2017-10-09 11:55:14'),(3,'Footer Links','2017-10-09 12:04:36','2017-10-09 12:04:36');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_01_01_000000_create_pages_table',1),(6,'2016_01_01_000000_create_posts_table',1),(7,'2016_02_15_204651_create_categories_table',1),(8,'2016_05_19_173453_create_menu_table',1),(9,'2016_10_21_190000_create_roles_table',1),(10,'2016_10_21_190000_create_settings_table',1),(11,'2016_11_30_135954_create_permission_table',1),(12,'2016_11_30_141208_create_permission_role_table',1),(13,'2016_12_26_201236_data_types__add__server_side',1),(14,'2017_01_13_000000_add_route_to_menu_items_table',1),(15,'2017_01_14_005015_create_translations_table',1),(16,'2017_01_15_000000_add_permission_group_id_to_permissions_table',1),(17,'2017_01_15_000000_create_permission_groups_table',1),(18,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(19,'2017_03_06_000000_add_controller_to_data_types_table',1),(20,'2017_04_11_000000_alter_post_nullable_fields_table',1),(21,'2017_04_21_000000_add_order_to_data_rows_table',1),(22,'2017_07_05_210000_add_policyname_to_data_types_table',1),(23,'2017_08_05_000000_add_group_to_settings_table',1),(24,'2017_10_09_152413_create_schedules_table',2),(25,'2017_10_09_154515_add_timestamps_to_schedule',3),(26,'2017_10_10_063936_add_gameId_to_schedules',4),(27,'2017_10_10_113105_add_team_abbreviations_to_schedules',5),(28,'2017_10_11_123552_create_teams_table',6),(29,'2017_10_14_054323_create_players_table',7),(30,'2017_10_14_072604_change_birth_date_type_players_table',8),(31,'2017_10_14_073213_alter_table_players_change_decimal_params',9),(32,'2017_10_14_073552_alter_table_players_jersey_type',10),(33,'2017_10_14_075501_alter_table_players_toi_nullable',11),(34,'2017_10_14_075501_alter_table_players_toi__gamenullable',12),(35,'2017_10_14_075501_alter_table_players_state_nullable',13),(36,'2017_10_15_075501_alter_table_players_toi_nullable_again',14),(37,'2017_10_20_154437_add_vs_columns_to_teams_table',15),(38,'2017_10_20_154439_add_vs_columns_to_teams_table',16),(39,'2017_10_20_154440_add_recorded_schedule',17),(40,'2017_10_20_154439_add_vs_columns_to_teams_table2',18),(41,'2017_10_26_094502_add_twitter_to_players',18),(42,'2017_10_26_103302_create_social_media_table',19),(44,'2017_10_26_112308_create_social_media_table',20),(46,'2017_10_26_121812_alter_players_add_links',21);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,1,'Welcome to Leafs Nation Blueprint!','Stay up to date daily with Pages From The Blueprint, a blog jammed packed with up to the minute statistics, extensive analysis, and breaking news!','<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack.&nbsp;</p>','pages/October2017/9782709-nhl-centennial-classic-detroit-red-wings-at-toronto-maple-leafs.jpeg','home','Yar Meta Description','Keyword1, Keyword2','ACTIVE','2017-10-06 17:48:21','2017-10-07 19:13:38'),(2,1,'Articles','This is the articles page!',NULL,'pages/October2017/459828213.jpg','articles','Articles','articles','INACTIVE','2017-10-09 12:58:07','2017-10-10 08:28:40'),(3,1,'Schedule','Schedule',NULL,NULL,'schedule','Toronto Maple Leafs Schedule','Leafs Schedule','ACTIVE','2017-10-10 08:29:32','2017-10-10 08:29:32'),(4,1,'Standings','standings',NULL,NULL,'standings','standings','standings','INACTIVE','2017-10-11 15:46:29','2017-10-11 15:46:29'),(5,1,'Player Statistics','Player',NULL,NULL,'player','Player Statistics','Player','ACTIVE','2017-10-15 11:17:43','2017-10-15 19:32:38'),(6,1,'404','404 page not found','<h1 class=\"reverse\">Whoops! You\'re offside!</h1>\r\n<p>Were sorry but we couldn\'t find what you were looking for, maybe we can interest you in one of the following?</p>',NULL,'404','404 page not found','404 page not found','ACTIVE','2017-10-15 22:02:41','2017-10-15 22:05:16'),(7,1,'Contact Us','Get in touch with Leafs Nation Blueprint!',NULL,NULL,'contact','Get in touch with Leafs Nation Blueprint!','Contact Leafs Nation Blueprint','INACTIVE','2017-10-15 23:23:31','2017-10-15 23:23:31'),(8,1,'Media','Media',NULL,'pages/October2017/brown.jpg','media','Media','Media','INACTIVE','2017-10-16 13:46:16','2017-10-16 13:46:16'),(9,1,'Versus','The Leafs record vs other teams on an individual basis',NULL,NULL,'versus','The Leafs record vs other teams on an individual basis','Maple Leafs record vs other teams','ACTIVE','2017-10-20 17:27:41','2017-10-20 17:27:41'),(10,1,'Segments','Leafs schedule broken into 5 game segments',NULL,NULL,'segments','Leafs schedule broken into 5 game segments','Leafs schedule broken into 5 game segments','INACTIVE','2017-10-25 07:51:06','2017-10-25 07:51:06');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_groups`
--

DROP TABLE IF EXISTS `permission_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permission_groups_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_groups`
--

LOCK TABLES `permission_groups` WRITE;
/*!40000 ALTER TABLE `permission_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(50,1),(51,1),(52,1),(53,1),(54,1),(55,1),(56,1),(57,1),(58,1),(59,1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission_group_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(2,'browse_database',NULL,'2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(3,'browse_media',NULL,'2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(4,'browse_compass',NULL,'2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(5,'browse_menus','menus','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(6,'read_menus','menus','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(7,'edit_menus','menus','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(8,'add_menus','menus','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(9,'delete_menus','menus','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(10,'browse_pages','pages','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(11,'read_pages','pages','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(12,'edit_pages','pages','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(13,'add_pages','pages','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(14,'delete_pages','pages','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(15,'browse_roles','roles','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(16,'read_roles','roles','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(17,'edit_roles','roles','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(18,'add_roles','roles','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(19,'delete_roles','roles','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(20,'browse_users','users','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(21,'read_users','users','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(22,'edit_users','users','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(23,'add_users','users','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(24,'delete_users','users','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(25,'browse_posts','posts','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(26,'read_posts','posts','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(27,'edit_posts','posts','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(28,'add_posts','posts','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(29,'delete_posts','posts','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(30,'browse_categories','categories','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(31,'read_categories','categories','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(32,'edit_categories','categories','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(33,'add_categories','categories','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(34,'delete_categories','categories','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(35,'browse_settings','settings','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(36,'read_settings','settings','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(37,'edit_settings','settings','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(38,'add_settings','settings','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(39,'delete_settings','settings','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(50,'browse_social_media','social_media','2017-10-26 11:25:28','2017-10-26 11:25:28',NULL),(51,'read_social_media','social_media','2017-10-26 11:25:28','2017-10-26 11:25:28',NULL),(52,'edit_social_media','social_media','2017-10-26 11:25:28','2017-10-26 11:25:28',NULL),(53,'add_social_media','social_media','2017-10-26 11:25:28','2017-10-26 11:25:28',NULL),(54,'delete_social_media','social_media','2017-10-26 11:25:28','2017-10-26 11:25:28',NULL),(55,'browse_players','players','2017-10-26 12:23:15','2017-10-26 12:23:15',NULL),(56,'read_players','players','2017-10-26 12:23:15','2017-10-26 12:23:15',NULL),(57,'edit_players','players','2017-10-26 12:23:15','2017-10-26 12:23:15',NULL),(58,'add_players','players','2017-10-26 12:23:15','2017-10-26 12:23:15',NULL),(59,'delete_players','players','2017-10-26 12:23:15','2017-10-26 12:23:15',NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `players`
--

DROP TABLE IF EXISTS `players`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `players` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `league_id` int(11) NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nhl_slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jersey_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `birth_city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth_state_province` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth_country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `height` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shoots_catches` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position_abbr` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `toi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `assists` int(11) DEFAULT NULL,
  `goals` int(11) DEFAULT NULL,
  `pim` int(11) DEFAULT NULL,
  `shots` int(11) DEFAULT NULL,
  `games` int(11) DEFAULT NULL,
  `hits` int(11) DEFAULT NULL,
  `pp_goals` int(11) DEFAULT NULL,
  `pp_points` int(11) DEFAULT NULL,
  `pp_toi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `even_toi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sh_toi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `face_off_pct` decimal(8,2) DEFAULT NULL,
  `shot_pct` decimal(8,2) DEFAULT NULL,
  `gwg` int(11) DEFAULT NULL,
  `otg` int(11) DEFAULT NULL,
  `shg` int(11) DEFAULT NULL,
  `shp` int(11) DEFAULT NULL,
  `blocked` int(11) DEFAULT NULL,
  `plus_minus` int(11) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `shifts` int(11) DEFAULT NULL,
  `toi_game` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `even_toi_game` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sh_toi_game` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pp_toi_game` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ot` int(11) DEFAULT NULL,
  `shutouts` int(11) DEFAULT NULL,
  `ties` int(11) DEFAULT NULL,
  `wins` int(11) DEFAULT NULL,
  `losses` int(11) DEFAULT NULL,
  `saves` int(11) DEFAULT NULL,
  `pp_saves` int(11) DEFAULT NULL,
  `sh_saves` int(11) DEFAULT NULL,
  `even_saves` int(11) DEFAULT NULL,
  `sh_shots` int(11) DEFAULT NULL,
  `even_shots` int(11) DEFAULT NULL,
  `pp_shots` int(11) DEFAULT NULL,
  `save_pct` decimal(8,3) DEFAULT NULL,
  `goals_against_average` decimal(8,2) DEFAULT NULL,
  `games_started` int(11) DEFAULT NULL,
  `shots_against` int(11) DEFAULT NULL,
  `goals_against` int(11) DEFAULT NULL,
  `pp_save_pct` decimal(8,2) DEFAULT NULL,
  `sh_save_pct` decimal(8,2) DEFAULT NULL,
  `even_save_pct` decimal(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `web` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `players_league_id_unique` (`league_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `players`
--

LOCK TABLES `players` WRITE;
/*!40000 ALTER TABLE `players` DISABLE KEYS */;
INSERT INTO `players` VALUES (1,8466139,'Patrick','Marleau','patrick-marleau-8466139','12','1979-09-15',38,'Aneroid','SK','CAN','6\' 2\"','215','L','Center','C','Forward','202:33',3,4,2,34,12,15,0,2,'28:40','165:08','08:45',52.63,11.80,1,0,0,0,1,1,7,294,'16:52','13:45','00:43','02:23',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 07:39:14','2017-10-31 15:20:55',NULL,NULL,NULL),(2,8468493,'Ron','Hainsey','ron-hainsey-8468493','2','1981-03-24',36,'Bolton','CT','USA','6\' 3\"','205','L','Defenseman','D','Defenseman','254:05',7,0,2,12,12,19,0,0,'01:43','196:02','56:20',0.00,0.00,0,0,0,0,18,2,7,336,'21:10','16:20','04:41','00:08',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 07:48:53','2017-10-31 15:20:55',NULL,NULL,NULL),(3,8468575,'Dominic','Moore','dominic-moore-8468575','20','1980-08-03',37,'Thornhill','ON','CAN','6\' 0\"','192','L','Center','C','Forward','86:39',0,3,2,9,8,7,0,0,'00:57','71:19','14:23',54.68,33.30,0,0,0,0,4,-2,3,138,'10:49','08:54','01:47','00:07',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 07:48:53','2017-10-31 15:20:55',NULL,NULL,NULL),(4,8470147,'Curtis','McElhinney','curtis-mcelhinney-8470147','35','1983-05-23',34,'London','ON','CAN','6\' 2\"','200','L','Goalie','G','Goalie','60:00',NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'60:00',NULL,NULL,NULL,0,0,0,1,0,30,1,0,29,0,32,1,0.909,3.00,1,33,3,100.00,1.00,90.63,'2017-10-14 07:56:31','2017-10-31 15:20:55',NULL,NULL,NULL),(5,8470611,'Eric','Fehr','eric-fehr-8470611','23','1985-09-07',32,'Winkler','MB','CAN','6\' 4\"','208','R','Center','C','Forward','43:44',0,0,2,4,4,2,0,0,'00:13','28:42','14:49',45.94,0.00,0,0,0,0,2,-1,0,77,'10:56','07:10','03:42','00:03',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 07:56:31','2017-10-27 08:08:29',NULL,NULL,NULL),(6,8473463,'Leo','Komarov','leo-komarov-8473463','47','1987-01-23',30,'Narva',NULL,'EST','5\' 11\"','209','L','Center','C','Forward','184:30',3,1,6,16,12,51,1,2,'08:36','145:18','30:36',54.76,6.30,0,0,0,0,7,0,4,280,'15:22','12:06','02:33','00:43',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 08:01:24','2017-10-31 15:20:55','LKomarov',NULL,NULL),(7,8474037,'James','van Riemsdyk','james-van-riemsdyk-8474037','25','1989-05-04',28,'Middletown','NJ','USA','6\' 3\"','217','L','Left Wing','LW','Forward','157:00',4,5,4,36,11,11,2,5,'27:04','129:52','00:04',0.00,13.90,0,0,0,0,3,-6,9,233,'14:16','11:48','00:00','02:27',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 08:01:24','2017-10-31 15:20:55','JVReemer21',NULL,NULL),(8,8474581,'Jake','Gardiner','jake-gardiner-8474581','51','1990-07-04',27,'Minnetonka','MN','USA','6\' 2\"','200','L','Defenseman','D','Defenseman','264:51',5,1,8,13,12,5,1,2,'31:43','226:49','06:19',0.00,7.70,0,0,0,0,14,0,6,345,'22:04','18:54','00:31','02:38',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 08:01:24','2017-10-31 15:20:55',NULL,NULL,NULL),(9,8474709,'Matt','Martin','matt-martin-8474709','15','1989-05-08',28,'Windsor','ON','CAN','6\' 3\"','220','L','Left Wing','LW','Forward','85:32',2,1,14,11,11,34,0,0,'00:26','85:06','00:00',100.00,9.10,0,0,0,0,6,-3,3,136,'07:46','07:44','00:00','00:02',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 08:01:24','2017-10-31 15:20:56',NULL,NULL,NULL),(10,8475098,'Tyler','Bozak','tyler-bozak-8475098','42','1986-03-19',31,'Regina','SK','CAN','6\' 1\"','199','R','Center','C','Forward','172:17',4,2,4,16,12,9,1,4,'28:49','143:14','00:14',52.66,12.50,1,0,0,0,4,-9,6,259,'14:21','11:56','00:01','02:24',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 08:01:24','2017-10-31 15:20:56',NULL,NULL,NULL),(11,8475172,'Nazem','Kadri','nazem-kadri-8475172','43','1990-10-06',27,'London','ON','CAN','6\' 0\"','190','L','Center','C','Forward','193:15',5,6,6,33,12,17,2,3,'28:21','164:48','00:06',54.74,18.20,0,0,0,0,5,2,11,284,'16:06','13:44','00:00','02:21',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 08:01:24','2017-10-31 15:20:56',NULL,NULL,NULL),(12,8475786,'Zach','Hyman','zach-hyman-8475786','11','1992-06-09',25,'Toronto','ON','CAN','6\' 1\"','213','R','Center','C','Forward','209:43',2,4,8,22,12,20,0,0,'03:16','171:35','34:52',37.50,18.20,0,0,0,0,8,6,6,303,'17:28','14:17','02:54','00:16',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 08:01:24','2017-10-31 15:20:56',NULL,NULL,NULL),(13,8475883,'Frederik','Andersen','frederik-andersen-8475883','31','1989-10-02',28,'Herning',NULL,'DNK','6\' 4\"','230','L','Goalie','G','Goalie','658:30',NULL,NULL,NULL,NULL,11,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'59:51',NULL,NULL,NULL,0,1,0,6,5,328,53,18,257,20,286,60,0.896,3.46,11,366,38,88.33,90.00,89.86,'2017-10-14 08:01:24','2017-10-31 15:20:56',NULL,NULL,NULL),(14,8476410,'Josh','Leivo','josh-leivo-8476410','32','1993-05-26',24,'Innisfil','ON','CAN','6\' 2\"','210','R','Left Wing','LW','Forward','14:56',1,0,0,4,1,0,0,0,'03:34','11:22','00:00',0.00,0.00,0,0,0,0,1,1,1,19,'14:56','11:22','00:00','03:34',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 08:01:24','2017-10-31 15:20:56',NULL,NULL,NULL),(15,8476853,'Morgan','Rielly','morgan-rielly-8476853','44','1994-03-09',23,'Vancouver','BC','CAN','6\' 1\"','217','L','Defenseman','D','Defenseman','250:24',9,1,4,32,12,5,1,4,'28:11','205:54','16:19',0.00,3.10,0,0,0,0,11,-3,10,368,'20:52','17:09','01:21','02:20',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 08:01:24','2017-10-31 15:20:56',NULL,NULL,NULL),(16,8476941,'Connor','Carrick','connor-carrick-8476941','8','1994-04-13',23,'Orland Park','IL','USA','5\' 10\"','192','R','Defenseman','D','Defenseman','84:31',1,0,2,6,6,2,0,0,'00:27','83:45','00:19',0.00,0.00,0,0,0,0,5,-1,1,121,'14:05','13:57','00:03','00:04',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 08:01:24','2017-10-31 15:20:56',NULL,NULL,NULL),(17,8477015,'Connor','Brown','connor-brown-8477015','28','1994-01-14',23,'Toronto','ON','CAN','6\' 0\"','185','R','Right Wing','RW','Forward','179:23',3,3,4,12,12,4,0,0,'19:27','129:36','30:20',50.00,25.00,2,0,0,0,6,-4,6,274,'14:56','10:48','02:31','01:37',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 08:01:24','2017-10-31 15:20:56',NULL,NULL,NULL),(18,8477939,'William','Nylander','william-nylander-8477939','29','1996-05-01',21,'Calgary','AB','CAN','6\' 0\"','191','R','Center','C','Forward','211:38',7,3,4,42,12,6,1,3,'29:28','182:07','00:03',53.06,7.10,1,0,0,0,1,10,10,298,'17:38','15:10','00:00','02:27',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 08:01:24','2017-10-31 15:20:56',NULL,NULL,NULL),(19,8478483,'Mitchell','Marner','mitchell-marner-8478483','16','1997-05-05',20,'Markham','ON','CAN','6\' 0\"','175','R','Center','C','Forward','180:08',5,1,0,21,12,8,1,3,'29:31','148:28','02:09',65.38,4.80,0,0,0,0,8,-9,6,256,'15:00','12:22','00:10','02:27',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 08:01:24','2017-10-31 15:20:56','Marner93','mitchymarner',NULL),(20,8479318,'Auston','Matthews','auston-matthews-8479318','34','1997-09-17',20,'San Ramon','CA','USA','6\' 3\"','216','L','Center','C','Forward','219:31',7,9,0,37,12,4,1,3,'29:48','189:08','00:35',51.66,24.30,2,2,0,0,11,12,16,303,'18:17','15:45','00:02','02:29',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 08:01:24','2017-10-31 15:20:56','AM34',NULL,NULL),(21,8479458,'Nikita','Zaitsev','nikita-zaitsev-8479458','22','1991-10-29',26,'Moscow',NULL,'RUS','6\' 2\"','195','R','Defenseman','D','Defenseman','274:23',4,2,8,7,12,22,0,0,'03:21','223:35','47:27',0.00,28.60,0,0,0,0,33,7,6,344,'22:51','18:37','03:57','00:16',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 08:01:24','2017-10-31 15:20:56',NULL,NULL,NULL),(22,8480157,'Calle','Rosen','calle-rosen-8480157','48','1994-02-02',23,'Vaxjo',NULL,'SWE','6\' 1\"','195','L','Defenseman','D','Defenseman','51:09',1,0,4,5,4,1,0,0,'01:42','49:26','00:01',0.00,0.00,0,0,0,0,0,-3,1,78,'12:47','12:21','00:00','00:25',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 08:01:24','2017-10-22 09:13:37',NULL,NULL,NULL),(23,8480158,'Andreas','Borgman','andreas-borgman-8480158','55','1995-06-18',22,'Stockholm',NULL,'SWE','6\' 0\"','212','L','Defenseman','D','Defenseman','150:32',2,1,4,13,10,25,0,0,'00:18','143:52','06:22',0.00,7.70,0,0,0,0,8,-3,3,218,'15:03','14:23','00:38','00:01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-14 08:01:24','2017-10-31 15:20:56',NULL,NULL,NULL),(24,8471392,'Roman','Polak','roman-polak-8471392','46','1986-04-28',31,'Ostrava',NULL,'CZE','6\' 2\"','235','R','Defenseman','D','Defenseman','60:31',1,0,2,3,4,5,0,0,'00:00','56:42','03:49',0.00,0.00,0,0,0,0,6,-2,1,87,'15:07','14:10','00:57','00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-25 10:01:02','2017-10-31 15:20:55',NULL,NULL,NULL),(25,8477953,'Kasperi','Kapanen','kasperi-kapanen-8477953','24','1996-07-23',21,'Kuopio',NULL,'FIN','6\' 1\"','187','R','Right Wing','RW','Forward','07:31',0,0,0,2,1,3,0,0,'00:00','07:31','00:00',0.00,0.00,0,0,0,0,0,0,0,11,'07:31','07:31','00:00','00:00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2017-10-29 11:05:04','2017-10-31 15:20:56',NULL,NULL,NULL);
/*!40000 ALTER TABLE `players` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,1,4,'27 DAYS TILL LEAFS HOCKEY',NULL,'The clock continues to expire as the Leafs only have 27 days until they drop the puck! We will get our first look at the Leafs this season on Sept 8th, 7PM EST.','<p>27 DAYS</p>\r\n<p>&nbsp;</p>\r\n<p>The clock continues to expire as the Leafs only have 27 days until they drop the puck! We will get our first look at the Leafs this season on Sept 8th, 7PM EST. The rookie tournament held at Ricoh Coliseum is beginning. Training camp is going to begin soon after that, only to have pre-season roll around the corner. Any game fanatics reading this right now, because the upcoming NHL 18 Xbox One release date is September 15, 2017 in the US and the UK. We are being spoiled in September! Grab Leafs TV from the local cable provider so you can shrink up that 27 days.</p>\r\n<p>&nbsp;</p>\r\n<p>NHL-1-700x394</p>\r\n<p>Auston Matthews on Tuukka Rask &ndash; NHL 18, EA Sports</p>\r\n<p>ROOKIE TOURNAMENT</p>\r\n<p>&nbsp;</p>\r\n<p>This is the very first time we will get to see Timothy Liljegren in a Leaf jersey. The Rookie Tournament will be held September 8-10th against the Ottawa Senators and the Montreal Canadiens. The players will compete in two games against rival teams, the roster includes a total of 15 forwards, nine defencemen and two goaltenders. Babcock will not be coaching as Toronto Marlies head coach, Sheldon Keefe will run the squad along with his bandits Rob Davison and A.J. MacLean, and Piero Greco. These are all members of the Marlies organization including the goalie coach Piero.</p>\r\n<p>&nbsp;</p>\r\n<p>21443203_10155815566163083_571444075_n</p>\r\n<p>Maple Leafs Rookie Tournament Roster</p>\r\n<p>TRAINING CAMP</p>\r\n<p>&nbsp;</p>\r\n<p>The Toronto Maple Leafs will hold their training camp on September 15-17th. The Leafs decided to switch it up this time a round after two extremely successful training camps held in Halifax, they will travel this time within Ontario to Niagara Falls. This is mainly a fan experience where they can gather up the opportunity to see their Leafs up close &amp; personal. This is one of my favorite things that the Leafs do as an organization, they bring the hockey to the fan and allow minor hockey, towns people, and kids to experience something that normally wouldn&rsquo;t occur in that city. As usual, there will be an Alumni game but the roster has yet to be released. I know I said this is mainly a fan experience but all eyes will be on the Leafs from the coaches, the spotlight will be on for all!</p>\r\n<p>&nbsp;</p>\r\n<p>PRE-SEASON</p>\r\n<p>&nbsp;</p>\r\n<p>This is sneaking up on us so fast that I don&rsquo;t even care that it means summer is over. Leafs Pre- season starts on September 18th and goes till the 30th, this will include match ups with the Ottawa Senators, Buffalo Sabres, Montreal Canadiens,&nbsp; and the Detroit Redwings.&nbsp; The Maple Leafs will play each of these teams twice. The games are as followed:</p>\r\n<p>&nbsp;</p>\r\n<p>Monday, September 18: Toronto at Ottawa, 7:30 PM</p>\r\n<p>Tuesday, September 19: Ottawa at Toronto, 7:30 PM</p>\r\n<p>Friday, September 22: Buffalo at Toronto (Ricoh Coliseum), 7:30 PM</p>\r\n<p>Saturday, September 23: Toronto at Buffalo, 7 PM</p>\r\n<p>Monday, September 25: Montreal at Toronto (Ricoh Coliseum), 7:30 PM</p>\r\n<p>Wednesday, September 27: Montreal v. Toronto, at Quebec City, Que (Centre Videotron), 7 PM</p>\r\n<p>Friday, September 29: Toronto at Detroit, 7:30 PM</p>\r\n<p>Saturday, September 30: Detroit at Toronto (Ricoh Coliseum), 7 PM</p>\r\n<p>source- https://editorinleaf.com/2017/08/08/toronto-maple-leafs-preseason-schedule/</p>\r\n<p>THE NUMBER 27</p>\r\n<p>&nbsp;</p>\r\n<p>This time, we are going to take a look at the number 27 and how it can be in relation to the Toronto Maple Leafs. I&rsquo;m going to start with who wore the jersey number because my favorite player of all time wore number 27 for the Maple Leafs. I know what a lot of you are thinking&hellip; its gotta be Sittler or Mahovlich. Well I will gladly disappoint most of you because my guy is Mike Peca. A draft pic by Vancouver, captain of the Islanders, 2002 Team Canada Gold Medalist, cup run with oil country and only to come to the Leafs to receive a season ending injury&hellip; Again, this is my favorite player of all time, not all time Leaf&hellip; anyway here is the list of lads who wore the 27 for the buds!</p>\r\n<p>&nbsp;</p>\r\n<p>Mike Nykoluk (1957)</p>\r\n<p>Kenny Girard (1957-60)</p>\r\n<p>Frank Mahovlich (1958-68)</p>\r\n<p>Gerry Meehan (1969)</p>\r\n<p>Gordie Nelson (1970)</p>\r\n<p>Darryl Sittler (1971-82)</p>\r\n<p>Miroslav Ihnacak (1986-87)</p>\r\n<p>Dave Semenko (1988)</p>\r\n<p>John Kordic (1989-91)</p>\r\n<p>Lucien Deblois (1991-92)</p>\r\n<p>Shayne Corson (2001-03)</p>\r\n<p>Bryan Marchment (2004)</p>\r\n<p>Michael Peca (2007)</p>\r\n<p>pecaleafs.jpeg.size.xxlarge.letterbox</p>\r\n<p>Mike Peca &ndash; Toronto Maple Leaf</p>\r\n<p>DARRYL SITTLER</p>\r\n<p>&nbsp;</p>\r\n<p>The man, the legend, the captain; this guy once scored 10 frigging points in a game! That&rsquo;s unthinkable, even in the time he played in. Such an accomplishment that we have held on too as Leaf fans because it is one of the bright spots on the shaky past where the cup drought is still outstanding. Sittler was one of those hard nosed, put your hard hat on and go to work type of players. He was drafted by Toronto 8th overall in the 1970 Entry Draft soon to become one of the most popular Leafs of all time! Some might say he was a Canadian hero.</p>\r\n<p>&nbsp;</p>\r\n<p>FUN FACTS WITH THE LEAFS AND 27</p>\r\n<p>&nbsp;</p>\r\n<p>Morgan Rielly had 27 points last season</p>\r\n<p>Mike Van Ryn played 27 games for the Maple Leafs</p>\r\n<p>Goaltender Mark Laforest played 27 games for the Maple Leafs in 1990</p>\r\n<p>Leafs drafted Tie Domi 27th overall in 1988</p>\r\n<p>Leafs drafted Randy osburn in 1972</p>\r\n<p>Nik Antropov had 27 power play career goals</p>\r\n<p>&nbsp;</p>\r\n<p>Video: Nik Antropov and Alexi Pocahontas</p>\r\n<p>&nbsp;</p>\r\n<p>IN CONCLUSION</p>\r\n<p>&nbsp;</p>\r\n<p>This is another short and sweet addition to the countdown till Maple Leaf Hockey! I hope everyone enjoys the bathroom reading. We only have 27 days till we can show the rest of this league that were ready to play with the best of them. Remember to mark the dates so you don&rsquo;t miss any preseason action!</p>','posts/October2017/sittler.jpg','27-days-till-leafs-hockey','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',1,'2017-10-06 17:48:21','2017-10-31 15:30:17'),(2,1,1,'30 DAYS, COUNTDOWN TO LEAFS HOCKEY',NULL,'Time continues to pass as the days continue to crumble! OHL pre-season has started so if you live in Ontario, take advantage and buy the cheap pre-season tickets to see some young talent in action.','<p>This is the body for the sample post, which includes the body.</p>\r\n<h2>We can use all kinds of format!</h2>\r\n<p>And include a bunch of other stuff.</p>','posts/October2017/30days.jpg','30-days-countdown-to-leafs-hockey','Meta Description for sample post','keyword1, keyword2, keyword3','PUBLISHED',1,'2017-09-06 17:48:21','2017-10-31 16:25:43'),(3,1,1,'32 DAYS, COUNTDOWN TO LEAF HOCKEY',NULL,'The Toronto Maple Leafs will begin their 2017/18 regular season against Laine’s Jets in the Peg.  The game will begin at 7pm EST on October 4th. Now, don’t be alarmed people!! We will get some boys in blue action during preseason and camps. Hockey is coming sooner than you think!','<p>This is the body for the latest post</p>','posts/October2017/tumblr_mvcu7sETQR1qj7wuoo1_1280.png','32-days-countdown-to-leaf-hockey','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2017-10-06 17:48:21','2017-10-08 13:36:49'),(4,1,1,'ROUND 1 – LEAFS – CAPITALS',NULL,'The Toronto Maple Leafs have done it, the playoffs have been made. The only catch.. is they have to face-off against the Washington Capitals in the first round. Washington had another dominant regular season. They are president trophy winners and look to finally capture Lord’s Stanley.','<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>','posts/October2017/leafsvscaps11.jpg','round-1-leafs-capitals','this be a meta descript','keyword1, keyword2, keyword3','PUBLISHED',0,'2017-10-06 17:48:21','2017-10-07 19:42:47');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Administrator','2017-10-06 17:48:21','2017-10-06 17:48:21'),(2,'user','Normal User','2017-10-06 17:48:21','2017-10-06 17:48:21');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedules`
--

DROP TABLE IF EXISTS `schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video` text COLLATE utf8mb4_unicode_ci,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `away` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `away_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `away_logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `homeId` int(11) NOT NULL,
  `awayId` int(11) NOT NULL,
  `home_score` int(11) NOT NULL,
  `away_score` int(11) NOT NULL,
  `final_period` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `winner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `winner_abbr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `game_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `recorded` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `schedules_game_id_unique` (`game_id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedules`
--

LOCK TABLES `schedules` WRITE;
/*!40000 ALTER TABLE `schedules` DISABLE KEYS */;
INSERT INTO `schedules` VALUES (165,'http://md-akc.med.nhl.com/mp4/nhl/2017/10/05/742e4190-3fff-45bf-8b32-33c7e315a55d/1507172892200/asset_1800k.mp4','Oct 04, 2017 7:00pm','WPG','TOR','Winnipeg Jets','Toronto Maple Leafs','winnipeg_jets.svg','toronto_maple_leafs.svg',52,10,2,7,'3rd','10','TOR','leaf_win','2017-10-10 06:44:13','2017-10-20 16:57:08','2017020001',1),(166,'http://md-akc.med.nhl.com/mp4/nhl/2017/10/08/45b5b6d3-7baf-464f-a0e3-8c60fb61967f/1507436737486/asset_1800k.mp4','Oct 07, 2017 7:00pm','TOR','NYR','Toronto Maple Leafs','New York Rangers','toronto_maple_leafs.svg','new_york_rangers.svg',10,3,8,5,'3rd','10','TOR','leaf_win','2017-10-10 06:44:13','2017-10-20 16:57:08','2017020016',1),(167,'http://md-akc.med.nhl.com/mp4/nhl/2017/10/10/3a18c188-6ec2-477c-b17e-29f0e8260778/1507604812132/asset_1800k.mp4','Oct 09, 2017 7:00pm','TOR','CHI','Toronto Maple Leafs','Chicago Blackhawks','toronto_maple_leafs.svg','chicago_blackhawks.svg',10,16,4,3,'OT','10','TOR','leaf_ot_win','2017-10-10 06:44:13','2017-10-20 16:57:08','2017020035',1),(168,'http://md-akc.med.nhl.com/mp4/nhl/2017/10/12/9b0694ed-15b4-4161-84a9-18d3844b29d8/1507785914219/asset_1800k.mp4','Oct 11, 2017 7:30pm','TOR','NJD','Toronto Maple Leafs','New Jersey Devils','toronto_maple_leafs.svg','new_jersey_devils.svg',10,1,3,6,'3rd','1','NJD','leaf_loss','2017-10-10 06:44:13','2017-10-20 16:57:08','2017020046',1),(169,'http://md-akc.med.nhl.com/mp4/nhl/2017/10/15/49bd45f3-4733-4085-aca6-c45cdf147dfc/1508040015945/asset_1800k.mp4','Oct 14, 2017 7:00pm','MTL','TOR','Montréal Canadiens','Toronto Maple Leafs','montreal_canadiens.svg','toronto_maple_leafs.svg',8,10,3,4,'OT','10','TOR','leaf_ot_win','2017-10-10 06:44:13','2017-10-20 16:57:08','2017020063',1),(170,'http://md-akc.med.nhl.com/mp4/nhl/2017/10/18/52d20cd5-c05b-4b4a-bef0-291cbfa2ca29/1508296838484/asset_1800k.mp4','Oct 17, 2017 7:00pm','WSH','TOR','Washington Capitals','Toronto Maple Leafs','washington_capitals.svg','toronto_maple_leafs.svg',15,10,0,2,'3rd','10','TOR','leaf_win','2017-10-10 06:44:13','2017-10-20 16:57:08','2017020084',1),(171,'http://md-akc.med.nhl.com/mp4/nhl/2017/10/19/94bd9696-bc30-4159-9707-3ca7f5112441/1508386550410/asset_1800k.mp4','Oct 18, 2017 7:30pm','TOR','DET','Toronto Maple Leafs','Detroit Red Wings','toronto_maple_leafs.svg','detroit_red_wings.svg',10,17,6,3,'3rd','10','TOR','leaf_win','2017-10-10 06:44:13','2017-10-20 16:57:08','2017020092',1),(172,'http://md-akc.med.nhl.com/mp4/nhl/2017/10/22/fd953626-02dc-43e6-b37d-ace18dc003c6/1508646754185/asset_1800k.mp4','Oct 21, 2017 7:00pm','OTT','TOR','Ottawa Senators','Toronto Maple Leafs','ottawa_senators.svg','toronto_maple_leafs.svg',9,10,6,3,'3rd','9','OTT','leaf_loss','2017-10-10 06:44:13','2017-10-22 09:13:37','2017020113',1),(173,'http://md-akc.med.nhl.com/mp4/nhl/2017/10/24/9ab9fb62-76af-469f-ac9d-9d0ce32247dd/1508813475788/asset_1800k.mp4','Oct 23, 2017 7:00pm','TOR','LAK','Toronto Maple Leafs','Los Angeles Kings','toronto_maple_leafs.svg','los_angeles_kings.svg',10,26,3,2,'3rd','10','TOR','leaf_win','2017-10-10 06:44:13','2017-10-25 10:01:02','2017020123',1),(174,'http://md-akc.med.nhl.com/mp4/nhl/2017/10/27/9c85d882-51e7-4cf9-97c7-d9c4c114e261/1509074478021/asset_1800k.mp4','Oct 26, 2017 7:00pm','TOR','CAR','Toronto Maple Leafs','Carolina Hurricanes','toronto_maple_leafs.svg','carolina_hurricanes.svg',10,12,3,6,'3rd','12','CAR','leaf_loss','2017-10-10 06:44:13','2017-10-27 08:07:27','2017020139',1),(175,'http://md-akc.med.nhl.com/mp4/nhl/2017/10/29/da2cb762-2560-405f-aad1-57f12cbe4c06/1509246563292/asset_1800k.mp4','Oct 28, 2017 7:00pm','TOR','PHI','Toronto Maple Leafs','Philadelphia Flyers','toronto_maple_leafs.svg','philadelphia_flyers.svg',10,4,2,4,'3rd','4','PHI','leaf_loss','2017-10-10 06:44:13','2017-10-29 11:05:04','2017020157',1),(176,'http://md-akc.med.nhl.com/mp4/nhl/2017/10/31/09e44671-fe6e-4c0b-91fc-9f6779b745f0/1509430081379/asset_1800k.mp4','Oct 30, 2017 10:30pm','SJS','TOR','San Jose Sharks','Toronto Maple Leafs','san_jose_sharks.svg','toronto_maple_leafs.svg',28,10,3,2,'3rd','28','SJS','leaf_loss','2017-10-10 06:44:13','2017-10-31 15:20:56','2017020177',1),(177,'','Nov 01, 2017 10:00pm','ANA','TOR','Anaheim Ducks','Toronto Maple Leafs','anaheim_ducks.svg','toronto_maple_leafs.svg',24,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:14:41','2017020184',0),(178,'','Nov 02, 2017 10:30pm','LAK','TOR','Los Angeles Kings','Toronto Maple Leafs','los_angeles_kings.svg','toronto_maple_leafs.svg',26,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:14:41','2017020197',0),(179,'','Nov 04, 2017 7:00pm','STL','TOR','St. Louis Blues','Toronto Maple Leafs','st_louis_blues.svg','toronto_maple_leafs.svg',19,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020205',0),(180,'','Nov 06, 2017 7:00pm','TOR','VGK','Toronto Maple Leafs','Vegas Golden Knights','toronto_maple_leafs.svg','vegas_golden_knights.svg',10,54,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020218',0),(181,'','Nov 08, 2017 7:30pm','TOR','MIN','Toronto Maple Leafs','Minnesota Wild','toronto_maple_leafs.svg','minnesota_wild.svg',10,30,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020232',0),(182,'','Nov 10, 2017 7:00pm','TOR','BOS','Toronto Maple Leafs','Boston Bruins','toronto_maple_leafs.svg','boston_bruins.svg',10,6,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020243',0),(183,'','Nov 11, 2017 7:00pm','BOS','TOR','Boston Bruins','Toronto Maple Leafs','boston_bruins.svg','toronto_maple_leafs.svg',6,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020250',0),(184,'','Nov 16, 2017 7:00pm','TOR','NJD','Toronto Maple Leafs','New Jersey Devils','toronto_maple_leafs.svg','new_jersey_devils.svg',10,1,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020278',0),(185,'','Nov 18, 2017 7:00pm','MTL','TOR','Montréal Canadiens','Toronto Maple Leafs','montreal_canadiens.svg','toronto_maple_leafs.svg',8,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020298',0),(186,'','Nov 20, 2017 7:00pm','TOR','ARI','Toronto Maple Leafs','Arizona Coyotes','toronto_maple_leafs.svg','arizona_coyotes.svg',10,53,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020311',0),(187,'','Nov 22, 2017 7:00pm','FLA','TOR','Florida Panthers','Toronto Maple Leafs','florida_panthers.svg','toronto_maple_leafs.svg',13,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020321',0),(188,'','Nov 24, 2017 7:30pm','CAR','TOR','Carolina Hurricanes','Toronto Maple Leafs','carolina_hurricanes.svg','toronto_maple_leafs.svg',12,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020343',0),(189,'','Nov 25, 2017 7:00pm','TOR','WSH','Toronto Maple Leafs','Washington Capitals','toronto_maple_leafs.svg','washington_capitals.svg',10,15,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020348',0),(190,'','Nov 28, 2017 9:00pm','CGY','TOR','Calgary Flames','Toronto Maple Leafs','calgary_flames.svg','toronto_maple_leafs.svg',20,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020374',0),(191,'','Nov 30, 2017 9:00pm','EDM','TOR','Edmonton Oilers','Toronto Maple Leafs','edmonton_oilers.svg','toronto_maple_leafs.svg',22,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020387',0),(192,'','Dec 02, 2017 7:00pm','VAN','TOR','Vancouver Canucks','Toronto Maple Leafs','vancouver_canucks.svg','toronto_maple_leafs.svg',23,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020402',0),(193,'','Dec 06, 2017 7:30pm','TOR','CGY','Toronto Maple Leafs','Calgary Flames','toronto_maple_leafs.svg','calgary_flames.svg',10,20,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020426',0),(194,'','Dec 09, 2017 7:00pm','PIT','TOR','Pittsburgh Penguins','Toronto Maple Leafs','pittsburgh_penguins.svg','toronto_maple_leafs.svg',5,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020450',0),(195,'','Dec 10, 2017 7:00pm','TOR','EDM','Toronto Maple Leafs','Edmonton Oilers','toronto_maple_leafs.svg','edmonton_oilers.svg',10,22,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020456',0),(196,'','Dec 12, 2017 7:00pm','PHI','TOR','Philadelphia Flyers','Toronto Maple Leafs','philadelphia_flyers.svg','toronto_maple_leafs.svg',4,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020468',0),(197,'','Dec 14, 2017 8:00pm','MIN','TOR','Minnesota Wild','Toronto Maple Leafs','minnesota_wild.svg','toronto_maple_leafs.svg',30,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020484',0),(198,'','Dec 15, 2017 7:30pm','DET','TOR','Detroit Red Wings','Toronto Maple Leafs','detroit_red_wings.svg','toronto_maple_leafs.svg',17,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020494',0),(199,'','Dec 19, 2017 2:00pm','TOR','CAR','Toronto Maple Leafs','Carolina Hurricanes','toronto_maple_leafs.svg','carolina_hurricanes.svg',10,12,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020516',0),(200,'','Dec 20, 2017 7:30pm','CBJ','TOR','Columbus Blue Jackets','Toronto Maple Leafs','columbus_blue_jackets.svg','toronto_maple_leafs.svg',29,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020526',0),(201,'','Dec 23, 2017 7:00pm','NYR','TOR','New York Rangers','Toronto Maple Leafs','new_york_rangers.svg','toronto_maple_leafs.svg',3,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020548',0),(202,'','Dec 28, 2017 9:00pm','ARI','TOR','Arizona Coyotes','Toronto Maple Leafs','arizona_coyotes.svg','toronto_maple_leafs.svg',53,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020572',0),(203,'','Dec 29, 2017 9:00pm','COL','TOR','Colorado Avalanche','Toronto Maple Leafs','colorado_avalanche.svg','toronto_maple_leafs.svg',21,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020584',0),(204,'','Dec 31, 2017 3:30pm','VGK','TOR','Vegas Golden Knights','Toronto Maple Leafs','vegas_golden_knights.svg','toronto_maple_leafs.svg',54,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020593',0),(205,'','Jan 02, 2018 7:00pm','TOR','TBL','Toronto Maple Leafs','Tampa Bay Lightning','toronto_maple_leafs.svg','tampa_bay_lightning.svg',10,14,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020602',0),(206,'','Jan 04, 2018 7:00pm','TOR','SJS','Toronto Maple Leafs','San Jose Sharks','toronto_maple_leafs.svg','san_jose_sharks.svg',10,28,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020617',0),(207,'','Jan 06, 2018 7:00pm','TOR','VAN','Toronto Maple Leafs','Vancouver Canucks','toronto_maple_leafs.svg','vancouver_canucks.svg',10,23,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020637',0),(208,'','Jan 08, 2018 7:00pm','TOR','CBJ','Toronto Maple Leafs','Columbus Blue Jackets','toronto_maple_leafs.svg','columbus_blue_jackets.svg',10,29,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020652',0),(209,'','Jan 10, 2018 7:30pm','TOR','OTT','Toronto Maple Leafs','Ottawa Senators','toronto_maple_leafs.svg','ottawa_senators.svg',10,9,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020660',0),(210,'','Jan 16, 2018 7:00pm','TOR','STL','Toronto Maple Leafs','St. Louis Blues','toronto_maple_leafs.svg','st_louis_blues.svg',10,19,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020687',0),(211,'','Jan 18, 2018 7:00pm','PHI','TOR','Philadelphia Flyers','Toronto Maple Leafs','philadelphia_flyers.svg','toronto_maple_leafs.svg',4,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020698',0),(212,'','Jan 20, 2018 7:00pm','OTT','TOR','Ottawa Senators','Toronto Maple Leafs','ottawa_senators.svg','toronto_maple_leafs.svg',9,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020713',0),(213,'','Jan 22, 2018 7:00pm','TOR','COL','Toronto Maple Leafs','Colorado Avalanche','toronto_maple_leafs.svg','colorado_avalanche.svg',10,21,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020726',0),(214,'','Jan 24, 2018 8:00pm','CHI','TOR','Chicago Blackhawks','Toronto Maple Leafs','chicago_blackhawks.svg','toronto_maple_leafs.svg',16,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020744',0),(215,'','Jan 25, 2018 8:30pm','DAL','TOR','Dallas Stars','Toronto Maple Leafs','dallas_stars.svg','toronto_maple_leafs.svg',25,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020754',0),(216,'','Jan 31, 2018 7:30pm','TOR','NYI','Toronto Maple Leafs','New York Islanders','toronto_maple_leafs.svg','new_york_islanders.svg',10,2,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020773',0),(217,'','Feb 01, 2018 7:00pm','NYR','TOR','New York Rangers','Toronto Maple Leafs','new_york_rangers.svg','toronto_maple_leafs.svg',3,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020779',0),(218,'','Feb 03, 2018 7:00pm','BOS','TOR','Boston Bruins','Toronto Maple Leafs','boston_bruins.svg','toronto_maple_leafs.svg',6,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020794',0),(219,'','Feb 05, 2018 7:00pm','TOR','ANA','Toronto Maple Leafs','Anaheim Ducks','toronto_maple_leafs.svg','anaheim_ducks.svg',10,24,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020808',0),(220,'','Feb 07, 2018 7:30pm','TOR','NSH','Toronto Maple Leafs','Nashville Predators','toronto_maple_leafs.svg','nashville_predators.svg',10,18,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020823',0),(221,'','Feb 10, 2018 7:00pm','TOR','OTT','Toronto Maple Leafs','Ottawa Senators','toronto_maple_leafs.svg','ottawa_senators.svg',10,9,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020844',0),(222,'','Feb 12, 2018 7:00pm','TOR','TBL','Toronto Maple Leafs','Tampa Bay Lightning','toronto_maple_leafs.svg','tampa_bay_lightning.svg',10,14,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020861',0),(223,'','Feb 14, 2018 7:00pm','TOR','CBJ','Toronto Maple Leafs','Columbus Blue Jackets','toronto_maple_leafs.svg','columbus_blue_jackets.svg',10,29,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020876',0),(224,'','Feb 17, 2018 7:00pm','PIT','TOR','Pittsburgh Penguins','Toronto Maple Leafs','pittsburgh_penguins.svg','toronto_maple_leafs.svg',5,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020899',0),(225,'','Feb 18, 2018 7:00pm','DET','TOR','Detroit Red Wings','Toronto Maple Leafs','detroit_red_wings.svg','toronto_maple_leafs.svg',17,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020909',0),(226,'','Feb 20, 2018 7:00pm','TOR','FLA','Toronto Maple Leafs','Florida Panthers','toronto_maple_leafs.svg','florida_panthers.svg',10,13,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020918',0),(227,'','Feb 22, 2018 7:00pm','TOR','NYI','Toronto Maple Leafs','New York Islanders','toronto_maple_leafs.svg','new_york_islanders.svg',10,2,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020930',0),(228,'','Feb 24, 2018 7:00pm','TOR','BOS','Toronto Maple Leafs','Boston Bruins','toronto_maple_leafs.svg','boston_bruins.svg',10,6,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020948',0),(229,'','Feb 26, 2018 7:30pm','TBL','TOR','Tampa Bay Lightning','Toronto Maple Leafs','tampa_bay_lightning.svg','toronto_maple_leafs.svg',14,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020966',0),(230,'','Feb 27, 2018 7:30pm','FLA','TOR','Florida Panthers','Toronto Maple Leafs','florida_panthers.svg','toronto_maple_leafs.svg',13,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017020972',0),(231,'','Mar 03, 2018 8:00pm','WSH','TOR','Washington Capitals','Toronto Maple Leafs','washington_capitals.svg','toronto_maple_leafs.svg',15,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017021005',0),(232,'','Mar 05, 2018 7:30pm','BUF','TOR','Buffalo Sabres','Toronto Maple Leafs','buffalo_sabres.svg','toronto_maple_leafs.svg',7,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017021015',0),(233,'','Mar 10, 2018 7:00pm','TOR','PIT','Toronto Maple Leafs','Pittsburgh Penguins','toronto_maple_leafs.svg','pittsburgh_penguins.svg',10,5,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017021055',0),(234,'','Mar 14, 2018 7:00pm','TOR','DAL','Toronto Maple Leafs','Dallas Stars','toronto_maple_leafs.svg','dallas_stars.svg',10,25,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017021078',0),(235,'','Mar 15, 2018 7:00pm','BUF','TOR','Buffalo Sabres','Toronto Maple Leafs','buffalo_sabres.svg','toronto_maple_leafs.svg',7,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017021083',0),(236,'','Mar 17, 2018 7:00pm','TOR','MTL','Toronto Maple Leafs','Montréal Canadiens','toronto_maple_leafs.svg','montreal_canadiens.svg',10,8,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017021101',0),(237,'','Mar 20, 2018 7:30pm','TBL','TOR','Tampa Bay Lightning','Toronto Maple Leafs','tampa_bay_lightning.svg','toronto_maple_leafs.svg',14,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017021127',0),(238,'','Mar 22, 2018 8:00pm','NSH','TOR','Nashville Predators','Toronto Maple Leafs','nashville_predators.svg','toronto_maple_leafs.svg',18,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017021142',0),(239,'','Mar 24, 2018 7:00pm','TOR','DET','Toronto Maple Leafs','Detroit Red Wings','toronto_maple_leafs.svg','detroit_red_wings.svg',10,17,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017021153',0),(240,'','Mar 26, 2018 7:00pm','TOR','BUF','Toronto Maple Leafs','Buffalo Sabres','toronto_maple_leafs.svg','buffalo_sabres.svg',10,7,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017021168',0),(241,'','Mar 28, 2018 7:30pm','TOR','FLA','Toronto Maple Leafs','Florida Panthers','toronto_maple_leafs.svg','florida_panthers.svg',10,13,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017021186',0),(242,'','Mar 30, 2018 7:00pm','NYI','TOR','New York Islanders','Toronto Maple Leafs','new_york_islanders.svg','toronto_maple_leafs.svg',2,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017021200',0),(243,'','Mar 31, 2018 7:00pm','TOR','WPG','Toronto Maple Leafs','Winnipeg Jets','toronto_maple_leafs.svg','winnipeg_jets.svg',10,52,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017021209',0),(244,'','Apr 02, 2018 7:00pm','TOR','BUF','Toronto Maple Leafs','Buffalo Sabres','toronto_maple_leafs.svg','buffalo_sabres.svg',10,7,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017021223',0),(245,'','Apr 05, 2018 7:00pm','NJD','TOR','New Jersey Devils','Toronto Maple Leafs','new_jersey_devils.svg','toronto_maple_leafs.svg',1,10,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017021241',0),(246,'','Apr 07, 2018 7:00pm','TOR','MTL','Toronto Maple Leafs','Montréal Canadiens','toronto_maple_leafs.svg','montreal_canadiens.svg',10,8,0,0,'Upcoming','','','','2017-10-10 06:44:13','2017-10-10 08:23:05','2017021259',0);
/*!40000 ALTER TABLE `schedules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'site.title','Site Title','Site Title','','text',1,'Site'),(2,'site.description','Site Description','Site Description','','text',2,'Site'),(3,'site.logo','Site Logo','settings/October2017/lnb-50-high-full.png','','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID','','','text',4,'Site'),(5,'admin.bg_image','Admin Background Image','','','image',5,'Admin'),(6,'admin.title','Admin Title','LNB','','text',1,'Admin'),(7,'admin.description','Admin Description','Welcome to Voyager. The Missing Admin for Laravel','','text',2,'Admin'),(8,'admin.loader','Admin Loader','','','image',3,'Admin'),(9,'admin.icon_image','Admin Icon Image','','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)','','','text',1,'Admin'),(11,'site.fb_page','Facebook Page','https://www.facebook.com/TheMapleLeafs/',NULL,'text',6,'Site'),(12,'site.fb_group','Facebook Group','https://www.facebook.com/groups/LeafsToday/',NULL,'text',7,'Site'),(13,'site.twitter_url','Twitter','https://twitter.com/leafsnb',NULL,'text',8,'Site'),(15,'site.instagram_url','Instagram','',NULL,'text',9,'Site');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `social_media`
--

DROP TABLE IF EXISTS `social_media`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `social_media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` enum('Management/Team','Leafs','Marlies') COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('Twitter','Facebook') COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `player_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `social_media`
--

LOCK TABLES `social_media` WRITE;
/*!40000 ALTER TABLE `social_media` DISABLE KEYS */;
INSERT INTO `social_media` VALUES (1,'Auston Matthews','Leafs','Twitter','https://twitter.com/AM34',8479318,'2017-10-26 11:55:47','2017-10-26 11:55:47'),(2,'James van Riemsdyk','Leafs','Twitter','https://twitter.com/JVReemer21',8474037,'2017-10-26 11:57:57','2017-10-26 11:57:57');
/*!40000 ALTER TABLE `social_media` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `team_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abbr` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `division` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conference` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `wins` int(11) NOT NULL,
  `losses` int(11) NOT NULL,
  `ot` int(11) NOT NULL,
  `ga` int(11) NOT NULL,
  `gf` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `dr` int(11) NOT NULL,
  `cr` int(11) NOT NULL,
  `lr` int(11) NOT NULL,
  `wr` int(11) NOT NULL,
  `row` int(11) NOT NULL,
  `gp` int(11) NOT NULL,
  `streak` int(11) NOT NULL,
  `streak_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `l_wins` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `l_losses` int(11) NOT NULL DEFAULT '0',
  `l_OT` int(11) NOT NULL DEFAULT '0',
  `l_SO` int(11) NOT NULL DEFAULT '0',
  `l_gf` int(11) NOT NULL DEFAULT '0',
  `l_ga` int(11) NOT NULL DEFAULT '0',
  `l_OTW` int(11) NOT NULL DEFAULT '0',
  `l_SOW` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `teams_team_id_unique` (`team_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (1,3,'New York Rangers','NYR','Metropolitan','Eastern',3,7,2,43,34,8,8,15,28,9,3,12,1,'L1',1,'2017-10-11 13:09:16','2017-10-31 15:20:55',0,0,0,5,8,0,0),(2,15,'Washington Capitals','WSH','Metropolitan','Eastern',5,6,1,41,36,11,6,10,22,4,4,12,1,'L1',1,'2017-10-11 13:14:41','2017-10-31 15:20:55',0,0,0,0,2,0,0),(3,1,'New Jersey Devils','NJD','Metropolitan','Eastern',8,2,0,31,40,16,1,2,5,0,6,10,2,'W2',0,'2017-10-11 13:14:41','2017-10-29 11:05:04',1,0,0,6,3,0,0),(4,29,'Columbus Blue Jackets','CBJ','Metropolitan','Eastern',8,4,0,30,37,16,2,3,6,0,7,12,1,'W1',0,'2017-10-11 13:14:41','2017-10-31 15:20:55',0,0,0,0,0,0,0),(5,4,'Philadelphia Flyers','PHI','Metropolitan','Eastern',6,5,1,35,41,13,5,8,14,2,6,12,1,'OT1',0,'2017-10-11 13:14:41','2017-10-31 15:20:55',1,0,0,4,2,0,0),(6,12,'Carolina Hurricanes','CAR','Metropolitan','Eastern',4,4,2,30,28,10,7,12,25,6,3,10,1,'OT1',0,'2017-10-11 13:14:41','2017-10-31 15:20:55',1,0,0,6,3,0,0),(7,2,'New York Islanders','NYI','Metropolitan','Eastern',7,4,1,38,45,15,3,4,7,0,6,12,2,'W2',0,'2017-10-11 13:14:41','2017-10-31 15:20:55',0,0,0,0,0,0,0),(8,5,'Pittsburgh Penguins','PIT','Metropolitan','Eastern',7,5,1,50,36,15,4,6,9,1,7,13,2,'L2',0,'2017-10-11 13:14:41','2017-10-31 15:20:55',0,0,0,0,0,0,0),(9,54,'Vegas Golden Knights','VGK','Pacific','Western',8,2,0,25,37,16,2,3,4,0,8,10,1,'L1',0,'2017-10-11 15:19:38','2017-10-31 15:20:55',0,0,0,0,0,0,0),(10,26,'Los Angeles Kings','LAK','Pacific','Western',9,2,1,24,40,19,1,2,3,0,8,12,1,'L1',1,'2017-10-11 15:19:38','2017-10-31 15:20:55',0,0,0,2,3,0,0),(11,20,'Calgary Flames','CGY','Pacific','Western',6,6,0,33,28,12,6,12,20,6,5,12,1,'W1',0,'2017-10-11 15:19:38','2017-10-31 15:20:55',0,0,0,0,0,0,0),(12,23,'Vancouver Canucks','VAN','Pacific','Western',6,3,2,27,31,14,3,4,10,0,6,11,1,'OT1',0,'2017-10-11 15:19:38','2017-10-31 15:20:55',0,0,0,0,0,0,0),(13,24,'Anaheim Ducks','ANA','Pacific','Western',6,4,1,33,35,13,4,6,13,1,5,11,2,'W2',0,'2017-10-11 15:19:38','2017-10-31 15:20:55',0,0,0,0,0,0,0),(14,22,'Edmonton Oilers','EDM','Pacific','Western',3,6,1,33,22,7,7,14,30,8,3,10,1,'L1',0,'2017-10-11 15:19:38','2017-10-31 15:20:55',0,0,0,0,0,0,0),(15,53,'Arizona Coyotes','ARI','Pacific','Western',1,10,1,51,30,3,8,15,31,9,1,12,1,'W1',0,'2017-10-11 15:19:38','2017-10-31 15:20:55',0,0,0,0,0,0,0),(16,28,'San Jose Sharks','SJS','Pacific','Western',6,5,0,28,30,12,5,8,16,2,6,11,2,'W2',0,'2017-10-11 15:19:38','2017-10-31 15:20:56',1,0,0,3,2,0,0),(17,10,'Toronto Maple Leafs','TOR','Atlantic','Eastern',7,5,0,43,47,14,3,7,11,0,7,12,3,'L3',0,'2017-10-11 15:20:59','2017-10-31 15:20:55',0,0,0,0,0,0,0),(18,14,'Tampa Bay Lightning','TBL','Atlantic','Eastern',10,2,1,36,53,21,1,1,1,0,10,13,1,'W1',0,'2017-10-11 15:20:59','2017-10-31 15:20:55',0,0,0,0,0,0,0),(19,17,'Detroit Red Wings','DET','Atlantic','Eastern',5,6,1,35,32,11,5,11,23,5,3,12,1,'W1',1,'2017-10-11 15:20:59','2017-10-31 15:20:55',0,0,0,3,6,0,0),(20,9,'Ottawa Senators','OTT','Atlantic','Eastern',5,2,5,41,44,15,2,5,8,0,4,12,1,'L1',0,'2017-10-11 15:20:59','2017-10-31 15:20:55',1,0,0,6,3,0,0),(21,13,'Florida Panthers','FLA','Atlantic','Eastern',4,6,1,44,40,9,6,13,26,7,4,11,1,'L1',0,'2017-10-11 15:20:59','2017-10-31 15:20:55',0,0,0,0,0,0,0),(22,6,'Boston Bruins','BOS','Atlantic','Eastern',4,3,3,33,30,11,4,9,21,3,4,10,2,'OT2',0,'2017-10-11 15:20:59','2017-10-31 15:20:55',0,0,0,0,0,0,0),(23,8,'Montréal Canadiens','MTL','Atlantic','Eastern',4,7,1,45,31,9,7,14,27,8,3,12,2,'W2',0,'2017-10-11 15:20:59','2017-10-31 15:20:55',0,0,0,3,4,1,0),(24,7,'Buffalo Sabres','BUF','Atlantic','Eastern',3,7,2,44,29,8,8,16,29,10,3,12,2,'L2',0,'2017-10-11 15:20:59','2017-10-31 15:20:55',0,0,0,0,0,0,0),(25,19,'St. Louis Blues','STL','Central','Western',10,2,1,30,44,21,1,1,2,0,9,13,4,'W4',0,'2017-10-11 15:20:59','2017-10-31 15:20:55',0,0,0,0,0,0,0),(26,16,'Chicago Blackhawks','CHI','Central','Western',5,5,2,34,38,12,6,11,19,5,5,12,3,'L3',0,'2017-10-11 15:20:59','2017-10-31 15:20:55',0,0,0,3,4,1,0),(27,21,'Colorado Avalanche','COL','Central','Western',6,5,0,34,34,12,4,9,17,3,6,11,1,'W1',0,'2017-10-11 15:20:59','2017-10-31 15:20:55',0,0,0,0,0,0,0),(28,25,'Dallas Stars','DAL','Central','Western',7,5,0,33,34,14,2,5,12,0,7,12,2,'W2',0,'2017-10-11 15:20:59','2017-10-31 15:20:55',0,0,0,0,0,0,0),(29,18,'Nashville Predators','NSH','Central','Western',5,4,2,31,27,12,5,10,18,4,5,11,1,'L1',0,'2017-10-11 15:20:59','2017-10-31 15:20:55',0,0,0,0,0,0,0),(30,52,'Winnipeg Jets','WPG','Central','Western',5,3,2,31,31,12,3,7,15,0,5,10,1,'W1',1,'2017-10-11 15:20:59','2017-10-31 15:20:55',0,0,0,2,7,0,0),(31,30,'Minnesota Wild','MIN','Central','Western',4,3,2,28,30,10,7,13,24,7,4,9,2,'W2',0,'2017-10-11 15:20:59','2017-10-31 15:20:55',0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` VALUES (1,'data_types','display_name_singular',1,'pt','Post','2017-10-06 17:48:21','2017-10-06 17:48:21'),(2,'data_types','display_name_singular',2,'pt','Página','2017-10-06 17:48:21','2017-10-06 17:48:21'),(3,'data_types','display_name_singular',3,'pt','Utilizador','2017-10-06 17:48:21','2017-10-06 17:48:21'),(4,'data_types','display_name_singular',4,'pt','Categoria','2017-10-06 17:48:21','2017-10-06 17:48:21'),(5,'data_types','display_name_singular',5,'pt','Menu','2017-10-06 17:48:21','2017-10-06 17:48:21'),(6,'data_types','display_name_singular',6,'pt','Função','2017-10-06 17:48:21','2017-10-06 17:48:21'),(7,'data_types','display_name_plural',1,'pt','Posts','2017-10-06 17:48:21','2017-10-06 17:48:21'),(8,'data_types','display_name_plural',2,'pt','Páginas','2017-10-06 17:48:21','2017-10-06 17:48:21'),(9,'data_types','display_name_plural',3,'pt','Utilizadores','2017-10-06 17:48:21','2017-10-06 17:48:21'),(10,'data_types','display_name_plural',4,'pt','Categorias','2017-10-06 17:48:21','2017-10-06 17:48:21'),(11,'data_types','display_name_plural',5,'pt','Menus','2017-10-06 17:48:21','2017-10-06 17:48:21'),(12,'data_types','display_name_plural',6,'pt','Funções','2017-10-06 17:48:21','2017-10-06 17:48:21'),(13,'categories','slug',1,'pt','categoria-1','2017-10-06 17:48:21','2017-10-06 17:48:21'),(14,'categories','name',1,'pt','Categoria 1','2017-10-06 17:48:21','2017-10-06 17:48:21'),(15,'categories','slug',2,'pt','categoria-2','2017-10-06 17:48:21','2017-10-06 17:48:21'),(16,'categories','name',2,'pt','Categoria 2','2017-10-06 17:48:21','2017-10-06 17:48:21'),(17,'pages','title',1,'pt','Olá Mundo','2017-10-06 17:48:21','2017-10-06 17:48:21'),(18,'pages','slug',1,'pt','ola-mundo','2017-10-06 17:48:21','2017-10-06 17:48:21'),(19,'pages','body',1,'pt','<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','2017-10-06 17:48:21','2017-10-06 17:48:21'),(20,'menu_items','title',1,'pt','Painel de Controle','2017-10-06 17:48:21','2017-10-06 17:48:21'),(21,'menu_items','title',2,'pt','Media','2017-10-06 17:48:21','2017-10-06 17:48:21'),(22,'menu_items','title',3,'pt','Publicações','2017-10-06 17:48:21','2017-10-06 17:48:21'),(23,'menu_items','title',4,'pt','Utilizadores','2017-10-06 17:48:21','2017-10-06 17:48:21'),(24,'menu_items','title',5,'pt','Categorias','2017-10-06 17:48:21','2017-10-06 17:48:21'),(25,'menu_items','title',6,'pt','Páginas','2017-10-06 17:48:21','2017-10-06 17:48:21'),(26,'menu_items','title',7,'pt','Funções','2017-10-06 17:48:21','2017-10-06 17:48:21'),(27,'menu_items','title',8,'pt','Ferramentas','2017-10-06 17:48:21','2017-10-06 17:48:21'),(28,'menu_items','title',9,'pt','Menus','2017-10-06 17:48:21','2017-10-06 17:48:21'),(29,'menu_items','title',10,'pt','Base de dados','2017-10-06 17:48:21','2017-10-06 17:48:21'),(30,'menu_items','title',13,'pt','Configurações','2017-10-06 17:48:21','2017-10-06 17:48:21');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Rick Calder','rick@calder.io','users/default.png','$2y$10$juyG63qld2/wl/cZ9u84ne/E7UY9rdgCFTNzkpHD5xmyopwHQiIbG','hbdosHE4T5RPZ3P5vCnN18S6b9AQX5IlxeSHieB88XJqyEUNspOvlb39Aeaj','2017-10-06 17:48:21','2017-10-08 13:55:51'),(2,2,'Test User','admin@admin.com','users/default.png','$2y$10$iGq3BxYRxo/m2qBXy9I9buu7DKvjiPYACGUGgu3pqGS8ojNPRrNfC',NULL,'2017-10-18 08:37:43','2017-10-18 08:37:43');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-05 14:35:30
