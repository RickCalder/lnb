-- MySQL dump 10.13  Distrib 5.7.19, for Linux (x86_64)
--
-- Host: localhost    Database: lnb
-- ------------------------------------------------------
-- Server version	5.7.19-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,NULL,1,'Category 1','category-1','2017-10-06 17:48:21','2017-10-06 17:48:21'),(2,NULL,1,'Category 2','category-2','2017-10-06 17:48:21','2017-10-06 17:48:21');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,'',1),(2,1,'author_id','text','Author',1,0,1,1,0,1,'',2),(3,1,'category_id','text','Category',1,0,1,1,1,0,'',3),(4,1,'title','text','Title',1,1,1,1,1,1,'',4),(5,1,'excerpt','text_area','excerpt',1,0,1,1,1,1,'',5),(6,1,'body','rich_text_box','Body',1,0,1,1,1,1,'',6),(7,1,'image','image','Post Image',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}',7),(8,1,'slug','text','slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}',8),(9,1,'meta_description','text_area','meta_description',1,0,1,1,1,1,'',9),(10,1,'meta_keywords','text_area','meta_keywords',1,0,1,1,1,1,'',10),(11,1,'status','select_dropdown','status',1,1,1,1,1,1,'{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}',11),(12,1,'created_at','timestamp','created_at',0,1,1,0,0,0,'',12),(13,1,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',13),(14,2,'id','number','id',1,0,0,0,0,0,'',1),(15,2,'author_id','text','author_id',1,0,0,0,0,0,'',2),(16,2,'title','text','title',1,1,1,1,1,1,'',3),(17,2,'excerpt','text_area','excerpt',1,0,1,1,1,1,'',4),(18,2,'body','rich_text_box','body',1,0,1,1,1,1,'',5),(19,2,'slug','text','slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\"}}',6),(20,2,'meta_description','text','meta_description',1,0,1,1,1,1,'',7),(21,2,'meta_keywords','text','meta_keywords',1,0,1,1,1,1,'',8),(22,2,'status','select_dropdown','status',1,1,1,1,1,1,'{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}',9),(23,2,'created_at','timestamp','created_at',1,1,1,0,0,0,'',10),(24,2,'updated_at','timestamp','updated_at',1,0,0,0,0,0,'',11),(25,2,'image','image','image',0,1,1,1,1,1,'',12),(26,3,'id','number','id',1,0,0,0,0,0,'',1),(27,3,'name','text','name',1,1,1,1,1,1,'',2),(28,3,'email','text','email',1,1,1,1,1,1,'',3),(29,3,'password','password','password',0,0,0,1,1,0,'',4),(30,3,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}',10),(31,3,'remember_token','text','remember_token',0,0,0,0,0,0,'',5),(32,3,'created_at','timestamp','created_at',0,1,1,0,0,0,'',6),(33,3,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',7),(34,3,'avatar','image','avatar',0,1,1,1,1,1,'',8),(35,5,'id','number','id',1,0,0,0,0,0,'',1),(36,5,'name','text','name',1,1,1,1,1,1,'',2),(37,5,'created_at','timestamp','created_at',0,0,0,0,0,0,'',3),(38,5,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',4),(39,4,'id','number','id',1,0,0,0,0,0,'',1),(40,4,'parent_id','select_dropdown','parent_id',0,0,1,1,1,1,'{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',2),(41,4,'order','text','order',1,1,1,1,1,1,'{\"default\":1}',3),(42,4,'name','text','name',1,1,1,1,1,1,'',4),(43,4,'slug','text','slug',1,1,1,1,1,1,'{\"slugify\":{\"origin\":\"name\"}}',5),(44,4,'created_at','timestamp','created_at',0,0,1,0,0,0,'',6),(45,4,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',7),(46,6,'id','number','id',1,0,0,0,0,0,'',1),(47,6,'name','text','Name',1,1,1,1,1,1,'',2),(48,6,'created_at','timestamp','created_at',0,0,0,0,0,0,'',3),(49,6,'updated_at','timestamp','updated_at',0,0,0,0,0,0,'',4),(50,6,'display_name','text','Display Name',1,1,1,1,1,1,'',5),(51,1,'seo_title','text','seo_title',0,1,1,1,1,1,'',14),(52,1,'featured','checkbox','featured',1,1,1,1,1,1,'',15),(53,3,'role_id','text','role_id',1,1,1,1,1,1,'',9);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'posts','posts','Post','Posts','voyager-news','TCG\\Voyager\\Models\\Post','TCG\\Voyager\\Policies\\PostPolicy','','',1,0,'2017-10-06 17:48:21','2017-10-06 17:48:21'),(2,'pages','pages','Page','Pages','voyager-file-text','TCG\\Voyager\\Models\\Page',NULL,'','',1,0,'2017-10-06 17:48:21','2017-10-06 17:48:21'),(3,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy','','',1,0,'2017-10-06 17:48:21','2017-10-06 17:48:21'),(4,'categories','categories','Category','Categories','voyager-categories','TCG\\Voyager\\Models\\Category',NULL,'','',1,0,'2017-10-06 17:48:21','2017-10-06 17:48:21'),(5,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,'2017-10-06 17:48:21','2017-10-06 17:48:21'),(6,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,'2017-10-06 17:48:21','2017-10-06 17:48:21');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2017-10-06 17:48:21','2017-10-06 17:48:21','voyager.dashboard',NULL),(2,1,'Media','','_self','voyager-images',NULL,NULL,5,'2017-10-06 17:48:21','2017-10-06 17:48:21','voyager.media.index',NULL),(3,1,'Posts','','_self','voyager-news',NULL,NULL,6,'2017-10-06 17:48:21','2017-10-06 17:48:21','voyager.posts.index',NULL),(4,1,'Users','','_self','voyager-person',NULL,NULL,3,'2017-10-06 17:48:21','2017-10-06 17:48:21','voyager.users.index',NULL),(5,1,'Categories','','_self','voyager-categories',NULL,NULL,8,'2017-10-06 17:48:21','2017-10-06 17:48:21','voyager.categories.index',NULL),(6,1,'Pages','','_self','voyager-file-text',NULL,NULL,7,'2017-10-06 17:48:21','2017-10-06 17:48:21','voyager.pages.index',NULL),(7,1,'Roles','','_self','voyager-lock',NULL,NULL,2,'2017-10-06 17:48:21','2017-10-06 17:48:21','voyager.roles.index',NULL),(8,1,'Tools','','_self','voyager-tools',NULL,NULL,9,'2017-10-06 17:48:21','2017-10-06 17:48:21',NULL,NULL),(9,1,'Menu Builder','','_self','voyager-list',NULL,8,10,'2017-10-06 17:48:21','2017-10-06 17:48:21','voyager.menus.index',NULL),(10,1,'Database','','_self','voyager-data',NULL,8,11,'2017-10-06 17:48:21','2017-10-06 17:48:21','voyager.database.index',NULL),(11,1,'Compass','/admin/compass','_self','voyager-compass',NULL,8,12,'2017-10-06 17:48:21','2017-10-06 17:48:21',NULL,NULL),(12,1,'Hooks','/admin/hooks','_self','voyager-hook',NULL,8,13,'2017-10-06 17:48:21','2017-10-06 17:48:21',NULL,NULL),(13,1,'Settings','','_self','voyager-settings',NULL,NULL,14,'2017-10-06 17:48:21','2017-10-06 17:48:21','voyager.settings.index',NULL),(14,2,'Home','/','_self',NULL,'#000000',NULL,15,'2017-10-09 11:55:45','2017-10-09 11:55:45',NULL,''),(15,2,'Articles','/articles','_self',NULL,'#000000',NULL,16,'2017-10-09 11:56:02','2017-10-09 11:56:02',NULL,''),(16,2,'Statistics','/stats','_self',NULL,'#000000',NULL,17,'2017-10-09 11:56:21','2017-10-09 11:56:21',NULL,''),(17,2,'Schedule','/schedule','_self',NULL,'#000000',NULL,18,'2017-10-09 11:56:41','2017-10-09 11:56:41',NULL,''),(18,2,'Contact','/contact','_self',NULL,'#000000',NULL,19,'2017-10-09 11:56:59','2017-10-09 11:56:59',NULL,''),(20,3,'Maple Leafs Website','https://www.nhl.com/mapleleafs','_blank',NULL,'#000000',NULL,1,'2017-10-09 12:05:45','2017-10-09 12:06:17',NULL,''),(21,3,'Maple Leafs Twitter','https://twitter.com/MapleLeafs','_blank',NULL,'#000000',NULL,2,'2017-10-09 12:06:47','2017-10-09 12:08:20',NULL,''),(22,3,'Maple Leafs Facebook','https://www.facebook.com/torontomapleleafs/','_blank',NULL,'#000000',NULL,3,'2017-10-09 12:07:05','2017-10-09 12:08:20',NULL,''),(23,3,'Maple Leafs Instagram','https://www.instagram.com/MapleLeafs/','_blank',NULL,'#000000',NULL,4,'2017-10-09 12:07:25','2017-10-09 12:08:20',NULL,''),(24,3,'Maple Leafs YouTube','https://www.youtube.com/torontomapleleafs','_blank',NULL,'#000000',NULL,5,'2017-10-09 12:07:47','2017-10-09 12:08:20',NULL,'');
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2017-10-06 17:48:21','2017-10-06 17:48:21'),(2,'Footer Site','2017-10-09 11:55:14','2017-10-09 11:55:14'),(3,'Footer Links','2017-10-09 12:04:36','2017-10-09 12:04:36');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_01_01_000000_create_pages_table',1),(6,'2016_01_01_000000_create_posts_table',1),(7,'2016_02_15_204651_create_categories_table',1),(8,'2016_05_19_173453_create_menu_table',1),(9,'2016_10_21_190000_create_roles_table',1),(10,'2016_10_21_190000_create_settings_table',1),(11,'2016_11_30_135954_create_permission_table',1),(12,'2016_11_30_141208_create_permission_role_table',1),(13,'2016_12_26_201236_data_types__add__server_side',1),(14,'2017_01_13_000000_add_route_to_menu_items_table',1),(15,'2017_01_14_005015_create_translations_table',1),(16,'2017_01_15_000000_add_permission_group_id_to_permissions_table',1),(17,'2017_01_15_000000_create_permission_groups_table',1),(18,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(19,'2017_03_06_000000_add_controller_to_data_types_table',1),(20,'2017_04_11_000000_alter_post_nullable_fields_table',1),(21,'2017_04_21_000000_add_order_to_data_rows_table',1),(22,'2017_07_05_210000_add_policyname_to_data_types_table',1),(23,'2017_08_05_000000_add_group_to_settings_table',1),(24,'2017_10_09_152413_create_schedules_table',2),(25,'2017_10_09_154515_add_timestamps_to_schedule',3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,1,'Welcome to Leafs Nation Blueprint!','Stay up to date daily with Pages From The Blueprint, a blog jammed packed with up to the minute statistics, extensive analysis, and breaking news!','<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack.&nbsp;</p>','pages/October2017/9782709-nhl-centennial-classic-detroit-red-wings-at-toronto-maple-leafs.jpeg','home','Yar Meta Description','Keyword1, Keyword2','ACTIVE','2017-10-06 17:48:21','2017-10-07 19:13:38'),(2,1,'Articles','This is the articles page!',NULL,NULL,'articles','Articles','articles','INACTIVE','2017-10-09 12:58:07','2017-10-09 12:58:07');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_groups`
--

DROP TABLE IF EXISTS `permission_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permission_groups_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_groups`
--

LOCK TABLES `permission_groups` WRITE;
/*!40000 ALTER TABLE `permission_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission_group_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(2,'browse_database',NULL,'2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(3,'browse_media',NULL,'2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(4,'browse_compass',NULL,'2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(5,'browse_menus','menus','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(6,'read_menus','menus','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(7,'edit_menus','menus','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(8,'add_menus','menus','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(9,'delete_menus','menus','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(10,'browse_pages','pages','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(11,'read_pages','pages','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(12,'edit_pages','pages','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(13,'add_pages','pages','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(14,'delete_pages','pages','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(15,'browse_roles','roles','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(16,'read_roles','roles','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(17,'edit_roles','roles','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(18,'add_roles','roles','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(19,'delete_roles','roles','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(20,'browse_users','users','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(21,'read_users','users','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(22,'edit_users','users','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(23,'add_users','users','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(24,'delete_users','users','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(25,'browse_posts','posts','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(26,'read_posts','posts','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(27,'edit_posts','posts','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(28,'add_posts','posts','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(29,'delete_posts','posts','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(30,'browse_categories','categories','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(31,'read_categories','categories','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(32,'edit_categories','categories','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(33,'add_categories','categories','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(34,'delete_categories','categories','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(35,'browse_settings','settings','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(36,'read_settings','settings','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(37,'edit_settings','settings','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(38,'add_settings','settings','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL),(39,'delete_settings','settings','2017-10-06 17:48:21','2017-10-06 17:48:21',NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,1,1,'27 DAYS TILL LEAFS HOCKEY',NULL,'The clock continues to expire as the Leafs only have 27 days until they drop the puck! We will get our first look at the Leafs this season on Sept 8th, 7PM EST.','<p>27 DAYS</p>\r\n<p>&nbsp;</p>\r\n<p>The clock continues to expire as the Leafs only have 27 days until they drop the puck! We will get our first look at the Leafs this season on Sept 8th, 7PM EST. The rookie tournament held at Ricoh Coliseum is beginning. Training camp is going to begin soon after that, only to have pre-season roll around the corner. Any game fanatics reading this right now, because the upcoming NHL 18 Xbox One release date is September 15, 2017 in the US and the UK. We are being spoiled in September! Grab Leafs TV from the local cable provider so you can shrink up that 27 days.</p>\r\n<p>&nbsp;</p>\r\n<p>NHL-1-700x394</p>\r\n<p>Auston Matthews on Tuukka Rask &ndash; NHL 18, EA Sports</p>\r\n<p>ROOKIE TOURNAMENT</p>\r\n<p>&nbsp;</p>\r\n<p>This is the very first time we will get to see Timothy Liljegren in a Leaf jersey. The Rookie Tournament will be held September 8-10th against the Ottawa Senators and the Montreal Canadiens. The players will compete in two games against rival teams, the roster includes a total of 15 forwards, nine defencemen and two goaltenders. Babcock will not be coaching as Toronto Marlies head coach, Sheldon Keefe will run the squad along with his bandits Rob Davison and A.J. MacLean, and Piero Greco. These are all members of the Marlies organization including the goalie coach Piero.</p>\r\n<p>&nbsp;</p>\r\n<p>21443203_10155815566163083_571444075_n</p>\r\n<p>Maple Leafs Rookie Tournament Roster</p>\r\n<p>TRAINING CAMP</p>\r\n<p>&nbsp;</p>\r\n<p>The Toronto Maple Leafs will hold their training camp on September 15-17th. The Leafs decided to switch it up this time a round after two extremely successful training camps held in Halifax, they will travel this time within Ontario to Niagara Falls. This is mainly a fan experience where they can gather up the opportunity to see their Leafs up close &amp; personal. This is one of my favorite things that the Leafs do as an organization, they bring the hockey to the fan and allow minor hockey, towns people, and kids to experience something that normally wouldn&rsquo;t occur in that city. As usual, there will be an Alumni game but the roster has yet to be released. I know I said this is mainly a fan experience but all eyes will be on the Leafs from the coaches, the spotlight will be on for all!</p>\r\n<p>&nbsp;</p>\r\n<p>PRE-SEASON</p>\r\n<p>&nbsp;</p>\r\n<p>This is sneaking up on us so fast that I don&rsquo;t even care that it means summer is over. Leafs Pre- season starts on September 18th and goes till the 30th, this will include match ups with the Ottawa Senators, Buffalo Sabres, Montreal Canadiens,&nbsp; and the Detroit Redwings.&nbsp; The Maple Leafs will play each of these teams twice. The games are as followed:</p>\r\n<p>&nbsp;</p>\r\n<p>Monday, September 18: Toronto at Ottawa, 7:30 PM</p>\r\n<p>Tuesday, September 19: Ottawa at Toronto, 7:30 PM</p>\r\n<p>Friday, September 22: Buffalo at Toronto (Ricoh Coliseum), 7:30 PM</p>\r\n<p>Saturday, September 23: Toronto at Buffalo, 7 PM</p>\r\n<p>Monday, September 25: Montreal at Toronto (Ricoh Coliseum), 7:30 PM</p>\r\n<p>Wednesday, September 27: Montreal v. Toronto, at Quebec City, Que (Centre Videotron), 7 PM</p>\r\n<p>Friday, September 29: Toronto at Detroit, 7:30 PM</p>\r\n<p>Saturday, September 30: Detroit at Toronto (Ricoh Coliseum), 7 PM</p>\r\n<p>source- https://editorinleaf.com/2017/08/08/toronto-maple-leafs-preseason-schedule/</p>\r\n<p>THE NUMBER 27</p>\r\n<p>&nbsp;</p>\r\n<p>This time, we are going to take a look at the number 27 and how it can be in relation to the Toronto Maple Leafs. I&rsquo;m going to start with who wore the jersey number because my favorite player of all time wore number 27 for the Maple Leafs. I know what a lot of you are thinking&hellip; its gotta be Sittler or Mahovlich. Well I will gladly disappoint most of you because my guy is Mike Peca. A draft pic by Vancouver, captain of the Islanders, 2002 Team Canada Gold Medalist, cup run with oil country and only to come to the Leafs to receive a season ending injury&hellip; Again, this is my favorite player of all time, not all time Leaf&hellip; anyway here is the list of lads who wore the 27 for the buds!</p>\r\n<p>&nbsp;</p>\r\n<p>Mike Nykoluk (1957)</p>\r\n<p>Kenny Girard (1957-60)</p>\r\n<p>Frank Mahovlich (1958-68)</p>\r\n<p>Gerry Meehan (1969)</p>\r\n<p>Gordie Nelson (1970)</p>\r\n<p>Darryl Sittler (1971-82)</p>\r\n<p>Miroslav Ihnacak (1986-87)</p>\r\n<p>Dave Semenko (1988)</p>\r\n<p>John Kordic (1989-91)</p>\r\n<p>Lucien Deblois (1991-92)</p>\r\n<p>Shayne Corson (2001-03)</p>\r\n<p>Bryan Marchment (2004)</p>\r\n<p>Michael Peca (2007)</p>\r\n<p>pecaleafs.jpeg.size.xxlarge.letterbox</p>\r\n<p>Mike Peca &ndash; Toronto Maple Leaf</p>\r\n<p>DARRYL SITTLER</p>\r\n<p>&nbsp;</p>\r\n<p>The man, the legend, the captain; this guy once scored 10 frigging points in a game! That&rsquo;s unthinkable, even in the time he played in. Such an accomplishment that we have held on too as Leaf fans because it is one of the bright spots on the shaky past where the cup drought is still outstanding. Sittler was one of those hard nosed, put your hard hat on and go to work type of players. He was drafted by Toronto 8th overall in the 1970 Entry Draft soon to become one of the most popular Leafs of all time! Some might say he was a Canadian hero.</p>\r\n<p>&nbsp;</p>\r\n<p>FUN FACTS WITH THE LEAFS AND 27</p>\r\n<p>&nbsp;</p>\r\n<p>Morgan Rielly had 27 points last season</p>\r\n<p>Mike Van Ryn played 27 games for the Maple Leafs</p>\r\n<p>Goaltender Mark Laforest played 27 games for the Maple Leafs in 1990</p>\r\n<p>Leafs drafted Tie Domi 27th overall in 1988</p>\r\n<p>Leafs drafted Randy osburn in 1972</p>\r\n<p>Nik Antropov had 27 power play career goals</p>\r\n<p>&nbsp;</p>\r\n<p>Video: Nik Antropov and Alexi Pocahontas</p>\r\n<p>&nbsp;</p>\r\n<p>IN CONCLUSION</p>\r\n<p>&nbsp;</p>\r\n<p>This is another short and sweet addition to the countdown till Maple Leaf Hockey! I hope everyone enjoys the bathroom reading. We only have 27 days till we can show the rest of this league that were ready to play with the best of them. Remember to mark the dates so you don&rsquo;t miss any preseason action!</p>','posts/October2017/sittler.jpg','27-days-till-leafs-hockey','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2017-10-06 17:48:21','2017-10-07 19:38:30'),(2,1,1,'30 DAYS, COUNTDOWN TO LEAFS HOCKEY',NULL,'Time continues to pass as the days continue to crumble! OHL pre-season has started so if you live in Ontario, take advantage and buy the cheap pre-season tickets to see some young talent in action.','<p>This is the body for the sample post, which includes the body.</p>\r\n<h2>We can use all kinds of format!</h2>\r\n<p>And include a bunch of other stuff.</p>','posts/October2017/30days.jpg','30-days-countdown-to-leafs-hockey','Meta Description for sample post','keyword1, keyword2, keyword3','PUBLISHED',0,'2017-10-06 17:48:21','2017-10-07 19:41:12'),(3,1,1,'32 DAYS, COUNTDOWN TO LEAF HOCKEY',NULL,'The Toronto Maple Leafs will begin their 2017/18 regular season against Laine’s Jets in the Peg.  The game will begin at 7pm EST on October 4th. Now, don’t be alarmed people!! We will get some boys in blue action during preseason and camps. Hockey is coming sooner than you think!','<p>This is the body for the latest post</p>','posts/October2017/tumblr_mvcu7sETQR1qj7wuoo1_1280.png','32-days-countdown-to-leaf-hockey','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2017-10-06 17:48:21','2017-10-08 13:36:49'),(4,1,1,'ROUND 1 – LEAFS – CAPITALS',NULL,'The Toronto Maple Leafs have done it, the playoffs have been made. The only catch.. is they have to face-off against the Washington Capitals in the first round. Washington had another dominant regular season. They are president trophy winners and look to finally capture Lord’s Stanley.','<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>','posts/October2017/leafsvscaps11.jpg','round-1-leafs-capitals','this be a meta descript','keyword1, keyword2, keyword3','PUBLISHED',0,'2017-10-06 17:48:21','2017-10-07 19:42:47');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Administrator','2017-10-06 17:48:21','2017-10-06 17:48:21'),(2,'user','Normal User','2017-10-06 17:48:21','2017-10-06 17:48:21');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedules`
--

DROP TABLE IF EXISTS `schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `video` text COLLATE utf8mb4_unicode_ci,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `away` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `away_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `away_logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `homeId` int(11) NOT NULL,
  `awayId` int(11) NOT NULL,
  `home_score` int(11) NOT NULL,
  `away_score` int(11) NOT NULL,
  `final_period` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `winner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `winner_abbr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedules`
--

LOCK TABLES `schedules` WRITE;
/*!40000 ALTER TABLE `schedules` DISABLE KEYS */;
INSERT INTO `schedules` VALUES (1,'http://md-akc.med.nhl.com/mp4/nhl/2017/10/05/742e4190-3fff-45bf-8b32-33c7e315a55d/1507172892200/asset_1800k.mp4','Oct 04, 2017','WPG','TOR','Winnipeg Jets','Toronto Maple Leafs','winnipeg_jets.svg','toronto_maple_leafs.svg',52,10,2,7,'3rd','10','TOR','leaf_win','2017-10-09 16:16:02','2017-10-09 16:16:02'),(2,'http://md-akc.med.nhl.com/mp4/nhl/2017/10/08/45b5b6d3-7baf-464f-a0e3-8c60fb61967f/1507436737486/asset_1800k.mp4','Oct 07, 2017','TOR','NYR','Toronto Maple Leafs','New York Rangers','toronto_maple_leafs.svg','new_york_rangers.svg',10,3,8,5,'3rd','10','TOR','leaf_win','2017-10-09 16:16:02','2017-10-09 16:16:02'),(3,'','Oct 09, 2017','TOR','CHI','Toronto Maple Leafs','Chicago Blackhawks','toronto_maple_leafs.svg','chicago_blackhawks.svg',10,16,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(4,'','Oct 11, 2017','TOR','NJD','Toronto Maple Leafs','New Jersey Devils','toronto_maple_leafs.svg','new_jersey_devils.svg',10,1,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(5,'','Oct 14, 2017','MTL','TOR','Montréal Canadiens','Toronto Maple Leafs','montreal_canadiens.svg','toronto_maple_leafs.svg',8,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(6,'','Oct 17, 2017','WSH','TOR','Washington Capitals','Toronto Maple Leafs','washington_capitals.svg','toronto_maple_leafs.svg',15,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(7,'','Oct 18, 2017','TOR','DET','Toronto Maple Leafs','Detroit Red Wings','toronto_maple_leafs.svg','detroit_red_wings.svg',10,17,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(8,'','Oct 21, 2017','OTT','TOR','Ottawa Senators','Toronto Maple Leafs','ottawa_senators.svg','toronto_maple_leafs.svg',9,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(9,'','Oct 23, 2017','TOR','LAK','Toronto Maple Leafs','Los Angeles Kings','toronto_maple_leafs.svg','los_angeles_kings.svg',10,26,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(10,'','Oct 26, 2017','TOR','CAR','Toronto Maple Leafs','Carolina Hurricanes','toronto_maple_leafs.svg','carolina_hurricanes.svg',10,12,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(11,'','Oct 28, 2017','TOR','PHI','Toronto Maple Leafs','Philadelphia Flyers','toronto_maple_leafs.svg','philadelphia_flyers.svg',10,4,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(12,'','Oct 30, 2017','SJS','TOR','San Jose Sharks','Toronto Maple Leafs','san_jose_sharks.svg','toronto_maple_leafs.svg',28,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(13,'','Nov 01, 2017','ANA','TOR','Anaheim Ducks','Toronto Maple Leafs','anaheim_ducks.svg','toronto_maple_leafs.svg',24,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(14,'','Nov 02, 2017','LAK','TOR','Los Angeles Kings','Toronto Maple Leafs','los_angeles_kings.svg','toronto_maple_leafs.svg',26,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(15,'','Nov 04, 2017','STL','TOR','St. Louis Blues','Toronto Maple Leafs','st_louis_blues.svg','toronto_maple_leafs.svg',19,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(16,'','Nov 06, 2017','TOR','VGK','Toronto Maple Leafs','Vegas Golden Knights','toronto_maple_leafs.svg','vegas_golden_knights.svg',10,54,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(17,'','Nov 08, 2017','TOR','MIN','Toronto Maple Leafs','Minnesota Wild','toronto_maple_leafs.svg','minnesota_wild.svg',10,30,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(18,'','Nov 10, 2017','TOR','BOS','Toronto Maple Leafs','Boston Bruins','toronto_maple_leafs.svg','boston_bruins.svg',10,6,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(19,'','Nov 11, 2017','BOS','TOR','Boston Bruins','Toronto Maple Leafs','boston_bruins.svg','toronto_maple_leafs.svg',6,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(20,'','Nov 16, 2017','TOR','NJD','Toronto Maple Leafs','New Jersey Devils','toronto_maple_leafs.svg','new_jersey_devils.svg',10,1,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(21,'','Nov 18, 2017','MTL','TOR','Montréal Canadiens','Toronto Maple Leafs','montreal_canadiens.svg','toronto_maple_leafs.svg',8,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(22,'','Nov 20, 2017','TOR','ARI','Toronto Maple Leafs','Arizona Coyotes','toronto_maple_leafs.svg','arizona_coyotes.svg',10,53,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(23,'','Nov 22, 2017','FLA','TOR','Florida Panthers','Toronto Maple Leafs','florida_panthers.svg','toronto_maple_leafs.svg',13,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(24,'','Nov 24, 2017','CAR','TOR','Carolina Hurricanes','Toronto Maple Leafs','carolina_hurricanes.svg','toronto_maple_leafs.svg',12,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(25,'','Nov 25, 2017','TOR','WSH','Toronto Maple Leafs','Washington Capitals','toronto_maple_leafs.svg','washington_capitals.svg',10,15,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(26,'','Nov 28, 2017','CGY','TOR','Calgary Flames','Toronto Maple Leafs','calgary_flames.svg','toronto_maple_leafs.svg',20,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(27,'','Nov 30, 2017','EDM','TOR','Edmonton Oilers','Toronto Maple Leafs','edmonton_oilers.svg','toronto_maple_leafs.svg',22,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(28,'','Dec 02, 2017','VAN','TOR','Vancouver Canucks','Toronto Maple Leafs','vancouver_canucks.svg','toronto_maple_leafs.svg',23,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(29,'','Dec 06, 2017','TOR','CGY','Toronto Maple Leafs','Calgary Flames','toronto_maple_leafs.svg','calgary_flames.svg',10,20,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(30,'','Dec 09, 2017','PIT','TOR','Pittsburgh Penguins','Toronto Maple Leafs','pittsburgh_penguins.svg','toronto_maple_leafs.svg',5,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(31,'','Dec 10, 2017','TOR','EDM','Toronto Maple Leafs','Edmonton Oilers','toronto_maple_leafs.svg','edmonton_oilers.svg',10,22,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(32,'','Dec 12, 2017','PHI','TOR','Philadelphia Flyers','Toronto Maple Leafs','philadelphia_flyers.svg','toronto_maple_leafs.svg',4,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(33,'','Dec 14, 2017','MIN','TOR','Minnesota Wild','Toronto Maple Leafs','minnesota_wild.svg','toronto_maple_leafs.svg',30,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(34,'','Dec 15, 2017','DET','TOR','Detroit Red Wings','Toronto Maple Leafs','detroit_red_wings.svg','toronto_maple_leafs.svg',17,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(35,'','Dec 19, 2017','TOR','CAR','Toronto Maple Leafs','Carolina Hurricanes','toronto_maple_leafs.svg','carolina_hurricanes.svg',10,12,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(36,'','Dec 20, 2017','CBJ','TOR','Columbus Blue Jackets','Toronto Maple Leafs','columbus_blue_jackets.svg','toronto_maple_leafs.svg',29,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(37,'','Dec 23, 2017','NYR','TOR','New York Rangers','Toronto Maple Leafs','new_york_rangers.svg','toronto_maple_leafs.svg',3,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(38,'','Dec 28, 2017','ARI','TOR','Arizona Coyotes','Toronto Maple Leafs','arizona_coyotes.svg','toronto_maple_leafs.svg',53,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(39,'','Dec 29, 2017','COL','TOR','Colorado Avalanche','Toronto Maple Leafs','colorado_avalanche.svg','toronto_maple_leafs.svg',21,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(40,'','Dec 31, 2017','VGK','TOR','Vegas Golden Knights','Toronto Maple Leafs','vegas_golden_knights.svg','toronto_maple_leafs.svg',54,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(41,'','Jan 02, 2018','TOR','TBL','Toronto Maple Leafs','Tampa Bay Lightning','toronto_maple_leafs.svg','tampa_bay_lightning.svg',10,14,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(42,'','Jan 04, 2018','TOR','SJS','Toronto Maple Leafs','San Jose Sharks','toronto_maple_leafs.svg','san_jose_sharks.svg',10,28,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(43,'','Jan 06, 2018','TOR','VAN','Toronto Maple Leafs','Vancouver Canucks','toronto_maple_leafs.svg','vancouver_canucks.svg',10,23,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(44,'','Jan 08, 2018','TOR','CBJ','Toronto Maple Leafs','Columbus Blue Jackets','toronto_maple_leafs.svg','columbus_blue_jackets.svg',10,29,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(45,'','Jan 10, 2018','TOR','OTT','Toronto Maple Leafs','Ottawa Senators','toronto_maple_leafs.svg','ottawa_senators.svg',10,9,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(46,'','Jan 16, 2018','TOR','STL','Toronto Maple Leafs','St. Louis Blues','toronto_maple_leafs.svg','st_louis_blues.svg',10,19,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(47,'','Jan 18, 2018','PHI','TOR','Philadelphia Flyers','Toronto Maple Leafs','philadelphia_flyers.svg','toronto_maple_leafs.svg',4,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(48,'','Jan 20, 2018','OTT','TOR','Ottawa Senators','Toronto Maple Leafs','ottawa_senators.svg','toronto_maple_leafs.svg',9,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(49,'','Jan 22, 2018','TOR','COL','Toronto Maple Leafs','Colorado Avalanche','toronto_maple_leafs.svg','colorado_avalanche.svg',10,21,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(50,'','Jan 24, 2018','CHI','TOR','Chicago Blackhawks','Toronto Maple Leafs','chicago_blackhawks.svg','toronto_maple_leafs.svg',16,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(51,'','Jan 25, 2018','DAL','TOR','Dallas Stars','Toronto Maple Leafs','dallas_stars.svg','toronto_maple_leafs.svg',25,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(52,'','Jan 31, 2018','TOR','NYI','Toronto Maple Leafs','New York Islanders','toronto_maple_leafs.svg','new_york_islanders.svg',10,2,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(53,'','Feb 01, 2018','NYR','TOR','New York Rangers','Toronto Maple Leafs','new_york_rangers.svg','toronto_maple_leafs.svg',3,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(54,'','Feb 03, 2018','BOS','TOR','Boston Bruins','Toronto Maple Leafs','boston_bruins.svg','toronto_maple_leafs.svg',6,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(55,'','Feb 05, 2018','TOR','ANA','Toronto Maple Leafs','Anaheim Ducks','toronto_maple_leafs.svg','anaheim_ducks.svg',10,24,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(56,'','Feb 07, 2018','TOR','NSH','Toronto Maple Leafs','Nashville Predators','toronto_maple_leafs.svg','nashville_predators.svg',10,18,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(57,'','Feb 10, 2018','TOR','OTT','Toronto Maple Leafs','Ottawa Senators','toronto_maple_leafs.svg','ottawa_senators.svg',10,9,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(58,'','Feb 12, 2018','TOR','TBL','Toronto Maple Leafs','Tampa Bay Lightning','toronto_maple_leafs.svg','tampa_bay_lightning.svg',10,14,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(59,'','Feb 14, 2018','TOR','CBJ','Toronto Maple Leafs','Columbus Blue Jackets','toronto_maple_leafs.svg','columbus_blue_jackets.svg',10,29,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(60,'','Feb 17, 2018','PIT','TOR','Pittsburgh Penguins','Toronto Maple Leafs','pittsburgh_penguins.svg','toronto_maple_leafs.svg',5,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(61,'','Feb 18, 2018','DET','TOR','Detroit Red Wings','Toronto Maple Leafs','detroit_red_wings.svg','toronto_maple_leafs.svg',17,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(62,'','Feb 20, 2018','TOR','FLA','Toronto Maple Leafs','Florida Panthers','toronto_maple_leafs.svg','florida_panthers.svg',10,13,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(63,'','Feb 22, 2018','TOR','NYI','Toronto Maple Leafs','New York Islanders','toronto_maple_leafs.svg','new_york_islanders.svg',10,2,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(64,'','Feb 24, 2018','TOR','BOS','Toronto Maple Leafs','Boston Bruins','toronto_maple_leafs.svg','boston_bruins.svg',10,6,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(65,'','Feb 26, 2018','TBL','TOR','Tampa Bay Lightning','Toronto Maple Leafs','tampa_bay_lightning.svg','toronto_maple_leafs.svg',14,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(66,'','Feb 27, 2018','FLA','TOR','Florida Panthers','Toronto Maple Leafs','florida_panthers.svg','toronto_maple_leafs.svg',13,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(67,'','Mar 03, 2018','WSH','TOR','Washington Capitals','Toronto Maple Leafs','washington_capitals.svg','toronto_maple_leafs.svg',15,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(68,'','Mar 05, 2018','BUF','TOR','Buffalo Sabres','Toronto Maple Leafs','buffalo_sabres.svg','toronto_maple_leafs.svg',7,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(69,'','Mar 10, 2018','TOR','PIT','Toronto Maple Leafs','Pittsburgh Penguins','toronto_maple_leafs.svg','pittsburgh_penguins.svg',10,5,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(70,'','Mar 14, 2018','TOR','DAL','Toronto Maple Leafs','Dallas Stars','toronto_maple_leafs.svg','dallas_stars.svg',10,25,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(71,'','Mar 15, 2018','BUF','TOR','Buffalo Sabres','Toronto Maple Leafs','buffalo_sabres.svg','toronto_maple_leafs.svg',7,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(72,'','Mar 17, 2018','TOR','MTL','Toronto Maple Leafs','Montréal Canadiens','toronto_maple_leafs.svg','montreal_canadiens.svg',10,8,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(73,'','Mar 20, 2018','TBL','TOR','Tampa Bay Lightning','Toronto Maple Leafs','tampa_bay_lightning.svg','toronto_maple_leafs.svg',14,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(74,'','Mar 22, 2018','NSH','TOR','Nashville Predators','Toronto Maple Leafs','nashville_predators.svg','toronto_maple_leafs.svg',18,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(75,'','Mar 24, 2018','TOR','DET','Toronto Maple Leafs','Detroit Red Wings','toronto_maple_leafs.svg','detroit_red_wings.svg',10,17,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(76,'','Mar 26, 2018','TOR','BUF','Toronto Maple Leafs','Buffalo Sabres','toronto_maple_leafs.svg','buffalo_sabres.svg',10,7,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(77,'','Mar 28, 2018','TOR','FLA','Toronto Maple Leafs','Florida Panthers','toronto_maple_leafs.svg','florida_panthers.svg',10,13,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(78,'','Mar 30, 2018','NYI','TOR','New York Islanders','Toronto Maple Leafs','new_york_islanders.svg','toronto_maple_leafs.svg',2,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(79,'','Mar 31, 2018','TOR','WPG','Toronto Maple Leafs','Winnipeg Jets','toronto_maple_leafs.svg','winnipeg_jets.svg',10,52,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(80,'','Apr 02, 2018','TOR','BUF','Toronto Maple Leafs','Buffalo Sabres','toronto_maple_leafs.svg','buffalo_sabres.svg',10,7,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(81,'','Apr 05, 2018','NJD','TOR','New Jersey Devils','Toronto Maple Leafs','new_jersey_devils.svg','toronto_maple_leafs.svg',1,10,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02'),(82,'','Apr 07, 2018','TOR','MTL','Toronto Maple Leafs','Montréal Canadiens','toronto_maple_leafs.svg','montreal_canadiens.svg',10,8,0,0,'Upcoming','','','','2017-10-09 16:16:02','2017-10-09 16:16:02');
/*!40000 ALTER TABLE `schedules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'site.title','Site Title','Site Title','','text',1,'Site'),(2,'site.description','Site Description','Site Description','','text',2,'Site'),(3,'site.logo','Site Logo','settings/October2017/lnb-50-high-full.png','','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID','','','text',4,'Site'),(5,'admin.bg_image','Admin Background Image','','','image',5,'Admin'),(6,'admin.title','Admin Title','LNB','','text',1,'Admin'),(7,'admin.description','Admin Description','Welcome to Voyager. The Missing Admin for Laravel','','text',2,'Admin'),(8,'admin.loader','Admin Loader','','','image',3,'Admin'),(9,'admin.icon_image','Admin Icon Image','','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)','','','text',1,'Admin'),(11,'site.fb_page','Facebook Page','',NULL,'text',6,'Site'),(12,'site.fb_group','Facebook Group','https://www.facebook.com/groups/LeafsToday/',NULL,'text',7,'Site'),(13,'site.twitter_url','Twitter','https://twitter.com/leafsnb',NULL,'text',8,'Site');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` VALUES (1,'data_types','display_name_singular',1,'pt','Post','2017-10-06 17:48:21','2017-10-06 17:48:21'),(2,'data_types','display_name_singular',2,'pt','Página','2017-10-06 17:48:21','2017-10-06 17:48:21'),(3,'data_types','display_name_singular',3,'pt','Utilizador','2017-10-06 17:48:21','2017-10-06 17:48:21'),(4,'data_types','display_name_singular',4,'pt','Categoria','2017-10-06 17:48:21','2017-10-06 17:48:21'),(5,'data_types','display_name_singular',5,'pt','Menu','2017-10-06 17:48:21','2017-10-06 17:48:21'),(6,'data_types','display_name_singular',6,'pt','Função','2017-10-06 17:48:21','2017-10-06 17:48:21'),(7,'data_types','display_name_plural',1,'pt','Posts','2017-10-06 17:48:21','2017-10-06 17:48:21'),(8,'data_types','display_name_plural',2,'pt','Páginas','2017-10-06 17:48:21','2017-10-06 17:48:21'),(9,'data_types','display_name_plural',3,'pt','Utilizadores','2017-10-06 17:48:21','2017-10-06 17:48:21'),(10,'data_types','display_name_plural',4,'pt','Categorias','2017-10-06 17:48:21','2017-10-06 17:48:21'),(11,'data_types','display_name_plural',5,'pt','Menus','2017-10-06 17:48:21','2017-10-06 17:48:21'),(12,'data_types','display_name_plural',6,'pt','Funções','2017-10-06 17:48:21','2017-10-06 17:48:21'),(13,'categories','slug',1,'pt','categoria-1','2017-10-06 17:48:21','2017-10-06 17:48:21'),(14,'categories','name',1,'pt','Categoria 1','2017-10-06 17:48:21','2017-10-06 17:48:21'),(15,'categories','slug',2,'pt','categoria-2','2017-10-06 17:48:21','2017-10-06 17:48:21'),(16,'categories','name',2,'pt','Categoria 2','2017-10-06 17:48:21','2017-10-06 17:48:21'),(17,'pages','title',1,'pt','Olá Mundo','2017-10-06 17:48:21','2017-10-06 17:48:21'),(18,'pages','slug',1,'pt','ola-mundo','2017-10-06 17:48:21','2017-10-06 17:48:21'),(19,'pages','body',1,'pt','<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','2017-10-06 17:48:21','2017-10-06 17:48:21'),(20,'menu_items','title',1,'pt','Painel de Controle','2017-10-06 17:48:21','2017-10-06 17:48:21'),(21,'menu_items','title',2,'pt','Media','2017-10-06 17:48:21','2017-10-06 17:48:21'),(22,'menu_items','title',3,'pt','Publicações','2017-10-06 17:48:21','2017-10-06 17:48:21'),(23,'menu_items','title',4,'pt','Utilizadores','2017-10-06 17:48:21','2017-10-06 17:48:21'),(24,'menu_items','title',5,'pt','Categorias','2017-10-06 17:48:21','2017-10-06 17:48:21'),(25,'menu_items','title',6,'pt','Páginas','2017-10-06 17:48:21','2017-10-06 17:48:21'),(26,'menu_items','title',7,'pt','Funções','2017-10-06 17:48:21','2017-10-06 17:48:21'),(27,'menu_items','title',8,'pt','Ferramentas','2017-10-06 17:48:21','2017-10-06 17:48:21'),(28,'menu_items','title',9,'pt','Menus','2017-10-06 17:48:21','2017-10-06 17:48:21'),(29,'menu_items','title',10,'pt','Base de dados','2017-10-06 17:48:21','2017-10-06 17:48:21'),(30,'menu_items','title',13,'pt','Configurações','2017-10-06 17:48:21','2017-10-06 17:48:21');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Rick Calder','rick@calder.io','users/default.png','$2y$10$juyG63qld2/wl/cZ9u84ne/E7UY9rdgCFTNzkpHD5xmyopwHQiIbG','hbdosHE4T5RPZ3P5vCnN18S6b9AQX5IlxeSHieB88XJqyEUNspOvlb39Aeaj','2017-10-06 17:48:21','2017-10-08 13:55:51');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-09 20:32:40
