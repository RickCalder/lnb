$(document).ready(function(){
  if( $(window).width() >= 990 ) {
    var view = 'month';
  } else {
    var view = "listMonth";
  }
  console.log(games)
  $('#schedule').fullCalendar({
      defaultView: view,
      aspectRatio: 1,
      events: games,
      eventRender: function(event, element) {
        var where = event.game_class === 'away_game' ? ' @ ' : ' vs ';
        var ended = event.final_period === '3rd' ? 'Regulation' : event.final_period;
        if( event.link != "") {
          var gm6 ='<div class="center"><a class="gamelink" href="" data-gm6="' + event.link + '"><img src="/img/tv.png" alt="image of a TV for the game in six link"></a>'
        } else {
          var gm6 = ""
        }
        console.log(event.link + "hi")
        if(  typeof event.opponent_score !== 'undefined' ) {
          var score = '<div><p class="center"><span class="' + event.leafs_win + '">TOR ' +  event.leafs_score + '</span>' + where + '<span class="' + event.opponent_win + '">' + event.opponent_abbr + ' ' + event.opponent_score + '</span></p><p class="center">' + ended + '</p></div>'; 
          element.html('<div class="center" style="padding-top:0.5rem">' + '<img src="/img/logos/' + event.opponent_logo + '"></div>' + score + gm6)
        } else {
          element.html('<div class="center" style="padding-top:0.5rem">' + '<img src="/img/logos/' + event.opponent_logo + '">' + event.time + '<br>' + where + event.opponent_abbr + '</div>')
        }
      }
  });

  $(document).on("click", ".gamelink", function(e){
    e.preventDefault();
    var videoUrl = $(this).data("gm6")
    $(".game-video-modal").html( '<video controls id="game-video"><source src="' + videoUrl + '" type="video/mp4">Your browser does not support HTML5 video.</video>' )
    $("#videoModal").modal("show")
  });

  $(document).on("click", ".close-video", function(e){
    e.preventDefault();
    $("video")[0].pause()
  });

});