$(document).ready(function () {
  var skaters = $('#skatersTable').DataTable({
    'order': [[5, 'desc']],
    'paging': false,
    'responsive': true,
    'info': false
  });
  $(".forwards").on("click", function(e) {
    e.preventDefault();
    skaters
    .column(0)
    .search('Forward')
    skaters.draw()
  });

  $(".defense").on("click", function(e) {
    e.preventDefault();
    skaters
    .column(0)
    .search('Defenseman')
    skaters.draw()
  });

  $(".all").on("click", function(e) {
    e.preventDefault();
    skaters
    .column(0)
    .search('')
    skaters.draw()
  });

  $('#goaliesTable').DataTable({
    'order': [[9, 'desc']],
    'paging': false,
    'responsive': true,
    'info': false
  });
});