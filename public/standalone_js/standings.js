$(document).ready(function () {
  var standings = $('#standingsTable').DataTable({
    'paging': false,
    'responsive': true,
    'info': false,
    'order':[[5, 'desc'],[1, 'asc'], [6, 'desc'], [9, 'desc']],
    columnDefs: [
      {responsivePriority: 0, targets: 5},
      {width: '25%', targets: 0}
    ]
  });

  var wildcard = $('.wildcard-table').DataTable({
    'paging': false,
    'responsive': true,
    'info': false,
    'bFilter': false,
    'order':[[5, 'desc'],[1, 'asc'], [6, 'desc'], [9, 'desc']],
    columnDefs: [
      {responsivePriority: 0, targets: 5},
      {width: '25%', targets: 0}
    ]
  });

  $("#standings-select").on("change", function() {
    var whichType = $(this).find(":selected").data("stype")
    var whichOne = $(this).val()
    console.log(whichOne)
    if( whichType === "wildcard" ) {
      $("#full-standings").hide()
      $("#wildcard").show()
      $(".standings-type").html(whichOne)
    } else {
      $("#wildcard").hide()
      $("#wildcard").hide()
      $("#full-standings").show(function(){
        standings
        .responsive.recalc()
      })

    }
    if( whichType == "division") {
      $(".standings-type").html(whichOne)
      standings
        .columns([12, 11])
        .search("")
        .column(11)
        .search(whichOne)
      standings
        .order([[5, 'desc'],[1, 'asc'], [6, 'desc'], [9, 'desc']])
        .draw();
    }
    if( whichType == "conference") {
      $(".standings-type").html(whichOne)
      standings
        .columns([12, 11])
        .search("")
        .column(12)
        .search(whichOne);
      standings
        .order([[5, 'desc'],[1, 'asc'], [6, 'desc'], [9, 'desc']])
        .draw();
    }
    if( whichType == "league") {
      $(".standings-type").html(whichOne)
      standings
        .columns([12, 11])
        .search("")
      standings
        .order([[5, 'desc'],[1, 'asc'], [6, 'desc'], [9, 'desc']])
        .draw();
    }
    console.log(whichType)
  })
});