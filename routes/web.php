<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
 
Route::get('/', 'HomeController@index');
Route::get('/articles', 'ArticleController@index')->name('articles');
Route::get('/articles/{slug}', 'ArticleController@show');
Route::get('/update', 'HomeController@update');
Route::get('/gameday', 'HomeController@gameday');
Route::get('/update_full', 'HomeController@update_schedule_all');
Route::get('/schedule', 'ScheduleController@index')->name('schedule');
Route::get('/segments', 'ScheduleController@segments')->name('segments');
Route::get('/versus', 'ScheduleController@versus')->name('versus');
Route::get('/standings', 'StatController@standings')->name('standings');
Route::get('/stats', 'StatController@stats')->name('stats');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/media', 'HomeController@media')->name('media');
Route::get('/player/{slug}', 'StatController@show')->name('player');
Route::get('logout', function (){
Auth::logout();
return redirect('/');
});

Auth::routes();
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
