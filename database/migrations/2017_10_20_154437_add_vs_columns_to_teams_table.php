<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVsColumnsToTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teams', function (Blueprint $table) {
            $table->integer('l_wins')->default(0)->after('streak_code');
            $table->integer('l_losses')->default(0);
            $table->integer('l_OT')->default(0);
            $table->integer('l_SO')->default(0);
            $table->integer('l_gf')->default(0);
            $table->integer('l_ga')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
