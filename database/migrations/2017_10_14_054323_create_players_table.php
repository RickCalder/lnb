<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('league_id')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('nhl_slug');
            $table->integer('jersey_number');
            $table->integer('birth_date');
            $table->integer('age');
            $table->string('birth_city');
            $table->string('birth_state_province');
            $table->string('birth_country');
            $table->string('height');
            $table->string('weight');
            $table->string('shoots_catches');
            $table->string('position');
            $table->string('position_abbr');
            $table->string('position_type');
            $table->string('toi');
            $table->integer('assists')->nullable();
            $table->integer('goals')->nullable();
            $table->integer('pim')->nullable();
            $table->integer('shots')->nullable();
            $table->integer('games')->nullable();
            $table->integer('hits')->nullable();
            $table->integer('pp_goals')->nullable();
            $table->integer('pp_points')->nullable();
            $table->string('pp_toi')->nullable();
            $table->string('even_toi')->nullable();
            $table->string('sh_toi')->nullable();
            $table->decimal('face_off_pct', 2, 2)->nullable();
            $table->decimal('shot_pct', 2, 2)->nullable();
            $table->integer('gwg')->nullable();
            $table->integer('otg')->nullable();
            $table->integer('shg')->nullable();
            $table->integer('shp')->nullable();
            $table->integer('blocked')->nullable();
            $table->integer('plus_minus')->nullable();
            $table->integer('points')->nullable();
            $table->integer('shifts')->nullable();
            $table->string('toi_game');
            $table->string('even_toi_game')->nullable();
            $table->string('sh_toi_game')->nullable();
            $table->string('pp_toi_game')->nullable();
            $table->integer('ot')->nullable();
            $table->integer('shutouts')->nullable();
            $table->integer('ties')->nullable();
            $table->integer('wins')->nullable();
            $table->integer('losses')->nullable();
            $table->integer('saves')->nullable();
            $table->integer('pp_saves')->nullable();
            $table->integer('sh_saves')->nullable();
            $table->integer('even_saves')->nullable();
            $table->integer('sh_shots')->nullable();
            $table->integer('even_shots')->nullable();
            $table->integer('pp_shots')->nullable();
            $table->decimal('save_pct',3, 3)->nullable();
            $table->decimal('goals_against_average', 2, 2)->nullable();
            $table->integer('games_started')->nullable();
            $table->integer('shots_against')->nullable();
            $table->integer('goals_against')->nullable();
            $table->decimal('pp_save_pct', 2, 2)->nullable();
            $table->decimal('sh_save_pct', 2, 2)->nullable();
            $table->decimal('even_save_pct', 2, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players');
    }
}
