<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePlayersChangeDecimalParams extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('players', function (Blueprint $table) {
            $table->decimal('face_off_pct', 8, 2)->nullable()->change();
            $table->decimal('shot_pct', 8, 2)->nullable()->change();
            $table->decimal('pp_save_pct', 8, 2)->nullable()->change();
            $table->decimal('sh_save_pct', 8, 2)->nullable()->change();
            $table->decimal('even_save_pct', 8, 2)->nullable()->change();
            $table->decimal('save_pct',8, 3)->nullable()->change();
            $table->decimal('goals_against_average', 8, 2)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
