<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('team_id')->unique();
            $table->string('name');
            $table->string('abbr');
            $table->string('division');
            $table->string('conference');
            $table->integer('wins');
            $table->integer('losses');
            $table->integer('ot');
            $table->integer('ga');
            $table->integer('gf');
            $table->integer('points');
            $table->integer('dr');
            $table->integer('cr');
            $table->integer('lr');
            $table->integer('wr');
            $table->integer('row');
            $table->integer('gp');
            $table->integer('streak');
            $table->string('streak_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
