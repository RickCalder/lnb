<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->text('video');
            $table->string('date');
            $table->string('home');
            $table->string('away');
            $table->string('home_name');
            $table->string('away_name');
            $table->string('home_logo');
            $table->string('away_logo');
            $table->integer('homeId');
            $table->integer('awayId');
            $table->integer('home_score');
            $table->integer('away_score');
            $table->string('final_period');
            $table->string('winner');
            $table->string('winner_abbr');
            $table->string('class');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
